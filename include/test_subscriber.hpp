#include <string>
#include <fstream>
#include <vector>
#include <thread>

#include <rclcpp/rclcpp.hpp>
#include <message_filters/subscriber.h>  
#include <message_filters/synchronizer.h>  
#include <message_filters/sync_policies/approximate_time.h>

#include <sensor_msgs/msg/joint_state.hpp>

#include "signal.h"

#include <unistd.h>

using namespace message_filters;

namespace test_synchronizer
{

// using Msg = sensor_msgs::msg::JointState;
// typedef std::shared_ptr<Msg> MsgPtr;
// typedef std::shared_ptr<Msg const> MsgConstPtr;

// class ApproximateTimeSynchronizerTestThree
// {
// public:

//   ApproximateTimeSynchronizerTestThree(uint32_t queue_size) : sync_(queue_size)
//   {
//     sync_.registerCallback(std::bind(&ApproximateTimeSynchronizerTestThree::callback, this, 
//                         std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
//   }

//     void callback(const MsgConstPtr& msg1, const MsgConstPtr& msg2, const MsgConstPtr& msg3)
//   {
//     printf("Call_back called\n");
//   }


// private:
//   typedef Synchronizer<ApproximateTime<Msg, Msg, Msg> > Sync3;
// public:
//   Sync3 sync_;
// };

class TestSubscriber
    : public rclcpp::Node
{
    public:
    TestSubscriber(): Node("subscriber"), m_synchronizer(10)
    {
        callback_group_subscriber_ = this->create_callback_group(
            rclcpp::CallbackGroupType::MutuallyExclusive); // MutuallyExclusive, Reentrant

        rclcpp::SubscriptionOptions sub_opts;
        sub_opts.callback_group = callback_group_subscriber_;

        // for (size_t i = 0; i < 3; i++)
        // {
            // m_subscribers[i] = std::make_unique<message_filters::Subscriber<JointStateMsgT>>(
            //     this, "topic" + std::to_string(i + 1), rmw_qos_profile_sensor_data);
        m_subscribers[0] = this->create_subscription<JointStateMsgT>("topic" + std::to_string(1), 300,
                                        std::bind(&TestSubscriber::callback1, this, std::placeholders::_1));
                                        
        m_subscribers[1] = this->create_subscription<JointStateMsgT>("topic" + std::to_string(2), 300,
                                        std::bind(&TestSubscriber::callback2, this, std::placeholders::_1));

        m_subscribers[2] = this->create_subscription<JointStateMsgT>("topic" + std::to_string(3), 300,
                                        std::bind(&TestSubscriber::callback3, this, std::placeholders::_1));



        //     }
        // }

        // m_synchronizer = std::make_unique<message_filters::Synchronizer<SyncPolicyT>>(
        //     SyncPolicyT(10), *m_subscribers[0], *m_subscribers[1], *m_subscribers[2]
        // );

        m_synchronizer.registerCallback(
            std::bind(
                &TestSubscriber::sync_callback, this, std::placeholders::_1,
                std::placeholders::_2, std::placeholders::_3
            )
        );
    }

    ~TestSubscriber()
    {
    }

    private:
    using JointStateMsgT = sensor_msgs::msg::JointState;
    using SyncPolicyT = message_filters::sync_policies::ApproximateTime<JointStateMsgT, JointStateMsgT, JointStateMsgT>;

    int count = 0;

    double long_delay_upper1 = 0.;
    double long_delay_upper2 = 0.;
    double long_delay_upper3 = 0.;

    double previous_delay_upper1 = 0.;
    double previous_delay_upper2 = 0.;
    double previous_delay_upper3 = 0.;

    double previous_timestamp1 = 0.;
    double previous_timestamp2 = 0.;
    double previous_timestamp3 = 0.;

    double observed_period_upper1 = 0.;
    double observed_period_upper2 = 0.;
    double observed_period_upper3 = 0.;
    

    void sync_callback(const JointStateMsgT::ConstSharedPtr& msg1, const JointStateMsgT::ConstSharedPtr& msg2, const JointStateMsgT::ConstSharedPtr& msg3)
    {
        rclcpp::Time now = this->now();

        rclcpp::Time start_time1 = msg1->header.stamp;

        double secs_start_time1 = start_time1.seconds();

        // printf("----------------------Temp time 3: %f.\n", secs_start_time1);

        rclcpp::Time start_time2 = msg2->header.stamp;

        double secs_start_time2 = start_time2.seconds();

        // printf("----------------------Temp time 3: %f.\n", secs_start_time2);

        rclcpp::Time start_time3 = msg3->header.stamp;

        double secs_start_time3 = start_time3.seconds();

        // printf("----------------------Temp time 3: %f.\n", secs_start_time3);

        // rclcpp::Duration delay_duration = now - start_time;
        // double delay = (double)delay_duration.sec + 1e-9*(double)delay_duration.nanosec;
        
        double delay1 = (now - start_time1).seconds();
        printf("----------------------Delay time of msg 1: %f.\n", delay1);

        double delay2 = (now - start_time2).seconds();
        printf("----------------------Delay time of msg 2: %f.\n", delay2);

        double delay3 = (now - start_time3).seconds();
        printf("----------------------Delay time of msg 3: %f.\n", delay3);

        if (delay1 > long_delay_upper1)
        {
            long_delay_upper1 = delay1;
        }

        if (delay2 > long_delay_upper2)
        {
            long_delay_upper2 = delay2;
        }

        if (delay3 > long_delay_upper3)
        {
            long_delay_upper3 = delay3;
        }

        if (count >= 200)
        {
            std::cout << endl;

            printf("----------------------Long delay upper bound of channel 1: %f.\n", long_delay_upper1);

            printf("----------------------Long delay upper bound of channel 2: %f.\n", long_delay_upper2);

            printf("----------------------Long delay upper bound of channel 3: %f.\n", long_delay_upper3);

            printf("----------------------Observed maximum long delay time: %f.\n", maximum_delay(long_delay_upper1, long_delay_upper2, long_delay_upper3));

            printf("----------------------Previous delay upper bound of channel 1: %f.\n", previous_delay_upper1);

            printf("----------------------Previous delay upper bound of channel 2: %f.\n", previous_delay_upper2);

            printf("----------------------Previous delay upper bound of channel 3: %f.\n", previous_delay_upper3);

            printf("----------------------Observed maximum previous delay time: %f.\n", maximum_delay(previous_delay_upper1, previous_delay_upper2, previous_delay_upper3));

            printf("----------------------Observed maximum period of channel 1: %f.\n", observed_period_upper1);

            printf("----------------------Observed maximum period of channel 2: %f.\n", observed_period_upper2);

            printf("----------------------Observed maximum period of channel 3: %f.\n", observed_period_upper3);

            kill(getpid(),SIGINT);
        }

        count++;

        std::thread::id threadID = std::this_thread::get_id();

// std::cout << "*7* Msg::filter::Subscriber cb is called (inside thread with ID :: " << threadID << ".)" << std::endl;

        std::cout << endl;
    }

    void callback1(JointStateMsgT::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();

        m_synchronizer.add<0>(msg);

        rclcpp::Time time1 = msg->header.temp_stamp;
        rclcpp::Time time2 = msg->header.stamp;

        double delay = (time1 - time2).seconds();

        if (delay > previous_delay_upper1)
        {
            previous_delay_upper1 = delay;
        }

        if (previous_timestamp1 == 0.)
        {
            previous_timestamp1 = time2.seconds();
        }
        else
        {
            double new_period_value = time2.seconds() - previous_timestamp1;
            if(new_period_value > observed_period_upper1)
            {
                observed_period_upper1 = new_period_value;
            }

            previous_timestamp1 = time2.seconds();
        }
    }

    void callback2(JointStateMsgT::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();

        m_synchronizer.add<1>(msg);

        rclcpp::Time time1 = msg->header.temp_stamp;
        rclcpp::Time time2 = msg->header.stamp;

        double delay = (time1 - time2).seconds();

        if (delay > previous_delay_upper2)
        {
            previous_delay_upper2 = delay;
        }

        if (previous_timestamp2 == 0.)
        {
            previous_timestamp2 = time2.seconds();
        }
        else
        {
            double new_period_value = time2.seconds() - previous_timestamp2;
            if(new_period_value > observed_period_upper2)
            {
                observed_period_upper2 = new_period_value;
            }

            previous_timestamp2 = time2.seconds();
        }
    }

    void callback3(JointStateMsgT::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();

        m_synchronizer.add<2>(msg);

        rclcpp::Time time1 = msg->header.temp_stamp;
        rclcpp::Time time2 = msg->header.stamp;

        double delay = (time1 - time2).seconds();

        if (delay > previous_delay_upper3)
        {
            previous_delay_upper3 = delay;
        }

        if (previous_timestamp3 == 0.)
        {
            previous_timestamp3 = time2.seconds();
        }
        else
        {
            double new_period_value = time2.seconds() - previous_timestamp3;
            if(new_period_value > observed_period_upper3)
            {
                observed_period_upper3 = new_period_value;
            }

            previous_timestamp3 = time2.seconds();
        }  
    }

    double maximum_delay(double v1, double v2, double v3)
    {
        if (v1 > v2)
        {
            if (v1 > v3)
            {
                return v1;
            }

            return v3;
        }
        else 
        {
            if (v2 > v3)
            {
                return v2;
            }

            return v3;
        }
    }

    // std::unique_ptr<message_filters::Subscriber<JointStateMsgT>> m_subscribers[3];
    typedef Synchronizer<SyncPolicyT> ApproSync;
    
    // std::unique_ptr<message_filters::Synchronizer<SyncPolicyT>> m_synchronizer;

    rclcpp::Subscription<JointStateMsgT>::SharedPtr m_subscribers[3];

    // Callback group settings
    rclcpp::CallbackGroup::SharedPtr callback_group_subscriber_;

    public:
    ApproSync m_synchronizer;
};

}