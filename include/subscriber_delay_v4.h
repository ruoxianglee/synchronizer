/*
Delay operation executes in publisher.
Add more timing tracing points.
*/

#include <string>
#include <fstream>
#include <vector>
#include <cfloat>

#include <message_filters/subscriber.h>  
#include <message_filters/synchronizer.h>  
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/sync_policies/approximate_time_model.h>

#include <sensor_msgs/msg/joint_state.hpp>

#include "signal.h"
#include "helper.h"

using namespace message_filters;

using std::placeholders::_1;
using std::placeholders::_2;
using std::placeholders::_3;
using std::placeholders::_4;
using std::placeholders::_5;
using std::placeholders::_6;
using std::placeholders::_7;
using std::placeholders::_8;
using std::placeholders::_9;

namespace synchronizer
{

#define buffer_size 10000000

class SubscriberTopic2
    : public rclcpp::Node
{
    ofstream& outfile_alg_;
    ofstream& outfile_mdl_;

    public:
    SubscriberTopic2(int period1, int period2, std::ofstream& outfile_alg, std::ofstream& outfile_mdl) :
        Node("subscriber_topic2"), outfile_alg_(outfile_alg), outfile_mdl_(outfile_mdl),
        alg_sync_(buffer_size), mdl_sync_(), period1_(period1), period2_(period2),
        count_alg_(0), count_mdl_(0), delay_previous1_(0), delay_previous2_(0),
        alg_observerd_wctd_(0), mdl_observed_wctd_(0), previous_timestamp1_(0), previous_timestamp2_(0), observed_wcp1_(0), observed_wcp2_(0)
    {
        this->declare_parameter("num_counted_events", 100);
        this->declare_parameter("num_published_set", 1000);

        this->get_parameter("num_counted_events", num_counted_events_);
        this->get_parameter("num_published_set", num_published_set_);

        RCLCPP_INFO(this->get_logger(), "required number of published sets: %d.", num_published_set_);
        RCLCPP_INFO(this->get_logger(), "required number of counted events: %d.", num_counted_events_);

        // callback_group_subscriber_ = this->create_callback_group(
        // rclcpp::CallbackGroupType::Reentrant);

        // Each of these callback groups is basically a thread
        // Everything assigned to one of them gets bundled into the same thread
        // auto sub_opt = rclcpp::SubscriptionOptions();
        // sub_opt.callback_group = callback_group_subscriber_;

        topic1_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic1", 300, 
                                                                        std::bind(&SubscriberTopic2::callback1, this, _1));
        topic2_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic2", 300, 
                                                                        std::bind(&SubscriberTopic2::callback2, this, _1));

        // alg_sync_.registerCallback(std::bind(&SubscriberTopic2::alg_callback, this, _1, _2));
        mdl_sync_.registerCallback(std::bind(&SubscriberTopic2::mdl_callback, this, _1, _2));

        RCLCPP_INFO(this->get_logger(), "Period 1: %d, Period 2: %d", period1, period2);

        // alg_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        // alg_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);

        mdl_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        mdl_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);

        // alg_sync_.setAgePenalty(0);
        mdl_sync_.setAgePenalty(0);
    }

    ~SubscriberTopic2()
    {
    }

    private:
    void alg_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2)
    {
        if(count_alg_ > num_published_set_)
        {
            return;    
        }
        if(count_alg_ == num_published_set_)
        {
            cout << "Approximate Time Algorithm has got enough published sets !" << endl;
            if(count_alg_ == num_published_set_ && count_mdl_ == num_published_set_)
            {
                count_alg_++;
                kill(getpid(),SIGINT);
            }
            return;
        }

        count_alg_++;

        double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        outfile_alg_ << topic1_timestamp << " " << topic2_timestamp << endl;
        double time_disparity = cal_time_disparity(2, topic1_timestamp, topic2_timestamp);

        if(time_disparity > alg_observerd_wctd_)
        {
            alg_observerd_wctd_ = time_disparity;
        }
    }

    void mdl_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2)
    {
        rclcpp::Time output_time = this->now();

        if(count_mdl_ >= num_published_set_ && count_event1_ >= num_counted_events_ && count_event2_ >= num_counted_events_)
        {
            cout << "Approximate Time Model has got enough published sets !" << endl;

            outfile_mdl_ << observed_bcp1_ << " " << observed_bcp2_ << " ";
            outfile_mdl_ << observed_wcp1_ << " " << observed_wcp2_ << " ";
            outfile_mdl_ << min_trans_delay1_ /  Mstos << " " << min_trans_delay2_ /  Mstos << " ";
            outfile_mdl_ << max_trans_delay1_ /  Mstos << " " << max_trans_delay2_ /  Mstos << " ";
            outfile_mdl_ << max_queuing_delay1_ << " " << max_queuing_delay2_ << " ";
            outfile_mdl_ << max_blocked_delay1_ << " " << max_blocked_delay2_ << " ";
            outfile_mdl_ << max_sync_latency1_ << " " << max_sync_latency2_ << endl;

            kill(getpid(),SIGINT);
            return;
        }
        count_mdl_++;

        if (wait_earliest_selected_msg1_ && waiting_event1_ == msg1->timing.event_id)
        {
            if (msg1->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time1 = msg1->timing.arrival_time;
                double queuing_latency1 = (arrival_time1 - earliest_arrival_time1_).seconds();
                
                if (queuing_latency1 > max_queuing_delay1_)
                {
                    max_queuing_delay1_ = queuing_latency1;
                }
                
                // Sync latency logging
                double sync_latency1 = (output_time - earliest_arrival_time1_).seconds();

                if (sync_latency1 > max_sync_latency1_)
                {
                    max_sync_latency1_ = sync_latency1;
                }
                
                count_event1_++;
            }

            wait_earliest_selected_msg1_ = false;
        }

        if (wait_earliest_selected_msg2_ && waiting_event2_ == msg2->timing.event_id)
        {   
            if (msg2->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time2 = msg2->timing.arrival_time;
                double queuing_latency2 = (arrival_time2 - earliest_arrival_time2_).seconds();
                
                if (queuing_latency2 > max_queuing_delay2_)
                {
                    max_queuing_delay2_ = queuing_latency2;
                }
                
                // Sync latency logging
                double sync_latency2 = (output_time - earliest_arrival_time2_).seconds();

                if (sync_latency2 > max_sync_latency2_)
                {
                    max_sync_latency2_ = sync_latency2;
                }

                count_event2_++;
            }

            wait_earliest_selected_msg2_ = false;
        }

        // Blocked delay logging
        rclcpp::Time start_time1 = msg1->header.temp_stamp;
        rclcpp::Time start_time2 = msg2->header.temp_stamp;

        double delay1 = (output_time - start_time1).seconds();
        double delay2 = (output_time - start_time2).seconds();
        // printf("----------------------Blocked delay time of msg 2: %f.\n", delay2);

        if (delay1 > max_blocked_delay1_)
        {
            max_blocked_delay1_ = delay1;
        }

        if (delay2 > max_blocked_delay2_)
        {
            max_blocked_delay2_ = delay2;
        }
    }

    void callback1(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time1_ = msg->timing.arrival_time;
            wait_earliest_selected_msg1_ = true;
            waiting_event1_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous1_ = absolute_delay.seconds() * Mstos;

        // alg_sync_.add<0>(msg);
        mdl_sync_.add<0>(msg);

        if (delay_previous1_ > max_trans_delay1_)
        {
            max_trans_delay1_ = delay_previous1_;
        }

        if (delay_previous1_ < min_trans_delay1_)
        {
            min_trans_delay1_ = delay_previous1_;
        }
        
        double topic1_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp1_ > 0)
            {
                double observed_now = topic1_timestamp - previous_timestamp1_;
                if(observed_now > observed_wcp1_)
                {
                    observed_wcp1_ = observed_now;
                }

                if(observed_now < observed_bcp1_)
                {
                    observed_bcp1_ = observed_now;
                }
            }
            previous_timestamp1_ = topic1_timestamp;
        }
    }

    void callback2(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time2_ = msg->timing.arrival_time;
            wait_earliest_selected_msg2_ = true;
            waiting_event2_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous2_ = absolute_delay.seconds() * Mstos;

        // alg_sync_.add<1>(msg);
        mdl_sync_.add<1>(msg);

        if (delay_previous2_ > max_trans_delay2_)
        {
            max_trans_delay2_ = delay_previous2_;
        }

        if (delay_previous2_ < min_trans_delay2_)
        {
            min_trans_delay2_ = delay_previous2_;
        }

        double topic2_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp2_ > 0)
            {
                double observed_now = topic2_timestamp - previous_timestamp2_;
                if(observed_now > observed_wcp2_)
                {
                    observed_wcp2_ = observed_now;
                }

                if(observed_now < observed_bcp2_)
                {
                    observed_bcp2_ = observed_now;
                }
            }
            previous_timestamp2_ = topic2_timestamp;
        }
    }

    rclcpp::CallbackGroup::SharedPtr callback_group_subscriber_;

    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr topic1_sub_;
    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr topic2_sub_;

    typedef Synchronizer<sync_policies::ApproximateTime<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > AlgSync;
    typedef Synchronizer<sync_policies::ApproximateTimeModel<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > MdlSync;

    AlgSync alg_sync_;
    MdlSync mdl_sync_;

    int period1_, period2_;

    int msg_delay_opt_; // Message delay options. 0: no delay, 2: delay 0-upper;
    int delay_upper_;   // Message delay upper limit (0, 10, 20, 30, 40)
    int num_published_set_; // Required number of published sets

    int count_alg_, count_mdl_;
    double delay_previous1_, delay_previous2_;

    double alg_observerd_wctd_, mdl_observed_wctd_;
    double previous_timestamp1_, previous_timestamp2_;
    double observed_wcp1_, observed_wcp2_;

    double observed_bcp1_ = FLT_MAX, observed_bcp2_ = FLT_MAX;

    double max_trans_delay1_ = 0., max_trans_delay2_ = 0.;
    double min_trans_delay1_ = FLT_MAX, min_trans_delay2_ = FLT_MAX;
    double max_blocked_delay1_ = 0., max_blocked_delay2_ = 0.;
    
    double max_queuing_delay1_ = 0., max_queuing_delay2_ = 0.;
    double max_sync_latency1_ = 0., max_sync_latency2_ = 0.;
    rclcpp::Time earliest_arrival_time1_, earliest_arrival_time2_;
    bool wait_earliest_selected_msg1_ = false, wait_earliest_selected_msg2_ = false;
    unsigned int waiting_event1_, waiting_event2_;

    int count_event1_ = 0, count_event2_ = 0;
    int num_counted_events_;
};

class SubscriberTopic3
    : public rclcpp::Node
{
    ofstream& outfile_alg_;
    ofstream& outfile_mdl_;

    public:
    SubscriberTopic3(int period1, int period2, int period3, std::ofstream& outfile_alg, std::ofstream& outfile_mdl) :
        Node("subscriber_topic3"), outfile_alg_(outfile_alg), outfile_mdl_(outfile_mdl),
        alg_sync_(buffer_size), mdl_sync_(), period1_(period1), period2_(period2), period3_(period3),
        count_alg_(0), count_mdl_(0), delay_previous1_(0.), delay_previous2_(0), delay_previous3_(0),
        alg_observerd_wctd_(0), mdl_observed_wctd_(0), previous_timestamp1_(0), previous_timestamp2_(0), previous_timestamp3_(0), observed_wcp1_(0), observed_wcp2_(0), observed_wcp3_(0)
    {
        this->declare_parameter("num_counted_events", 100);
        this->declare_parameter("num_published_set", 1000);

        this->get_parameter("num_counted_events", num_counted_events_);
        this->get_parameter("num_published_set", num_published_set_);

        RCLCPP_INFO(this->get_logger(), "required number of published sets: %d.", num_published_set_);
        RCLCPP_INFO(this->get_logger(), "required number of counted events: %d.", num_counted_events_);

        callback_group_subscriber_ = this->create_callback_group(
        rclcpp::CallbackGroupType::Reentrant);

        // Each of these callback groups is basically a thread
        // Everything assigned to one of them gets bundled into the same thread
        auto sub_opt = rclcpp::SubscriptionOptions();
        sub_opt.callback_group = callback_group_subscriber_;

        topic1_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic1", 300, 
                                                                        std::bind(&SubscriberTopic3::callback1, this, _1));

        topic2_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic2", 300, 
                                                                        std::bind(&SubscriberTopic3::callback2, this, _1));

        topic3_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic3", 300, 
                                                                        std::bind(&SubscriberTopic3::callback3, this, _1));

        // alg_sync_.registerCallback(std::bind(&SubscriberTopic3::alg_callback, this, _1, _2, _3));
        mdl_sync_.registerCallback(std::bind(&SubscriberTopic3::mdl_callback, this, _1, _2, _3));

        RCLCPP_INFO(this->get_logger(), "Period 1: %d, Period 2: %d, Period 3: %d", period1, period2, period3);

        // alg_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        // alg_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);
        // alg_sync_.setInterMessageLowerBound(2, PeriodBase * 0.001 * period3);

        mdl_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        mdl_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);
        mdl_sync_.setInterMessageLowerBound(2, PeriodBase * 0.001 * period3);
        
        // alg_sync_.setAgePenalty(0);
        mdl_sync_.setAgePenalty(0);
    }

    ~SubscriberTopic3()
    {
    }

    private:
    void alg_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg3)
    {
        if(count_alg_ > num_published_set_)
        {
            return;
        }

        if(count_alg_ == num_published_set_)
        {
            cout << "Approximate Time Algorithm has got enough published sets !" << endl;
            if(count_alg_ == num_published_set_ && count_mdl_ == num_published_set_)
            {
                count_alg_++;

                kill(getpid(),SIGINT);
            }
            return;
        }

        count_alg_++;

        double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        double topic3_timestamp = (double)msg3->header.stamp.sec + 1e-9*(double)msg3->header.stamp.nanosec;
        outfile_alg_ << topic1_timestamp << " " << topic2_timestamp << " " << topic3_timestamp << endl;
        
        double time_disparity = cal_time_disparity(3, topic1_timestamp, topic2_timestamp, topic3_timestamp);

        if(time_disparity > alg_observerd_wctd_)
        {
            alg_observerd_wctd_ = time_disparity;
        }
    }

    void mdl_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg3)
    {
        rclcpp::Time output_time = this->now();

        if(count_mdl_ >= num_published_set_ && count_event1_ >= num_counted_events_ && count_event2_ >= num_counted_events_ && count_event3_ >= num_counted_events_)
        {
            cout << "Approximate Time Model has got enough published sets !" << endl;

            outfile_mdl_ << observed_bcp1_ << " " << observed_bcp2_ << " " << observed_bcp3_ << " ";
            outfile_mdl_ << observed_wcp1_ << " " << observed_wcp2_ << " " << observed_wcp3_ << " ";
            outfile_mdl_ << min_trans_delay1_ /  Mstos << " " << min_trans_delay2_ /  Mstos << " " << min_trans_delay3_ /  Mstos << " ";
            outfile_mdl_ << max_trans_delay1_ /  Mstos << " " << max_trans_delay2_ /  Mstos << " " << max_trans_delay3_ /  Mstos << " ";
            outfile_mdl_ << max_queuing_delay1_ << " " << max_queuing_delay2_ << " " << max_queuing_delay3_<< " ";
            outfile_mdl_ << max_blocked_delay1_ << " " << max_blocked_delay2_ << " " << max_blocked_delay3_ << " ";
            outfile_mdl_ << max_sync_latency1_ << " " << max_sync_latency2_ << " " << max_sync_latency3_ << endl;

            kill(getpid(),SIGINT);
            return;     
        }

        count_mdl_++;

        cout << "Entering fusion point .... with obtained event id: " << msg1->timing.event_id << endl;

        if (wait_earliest_selected_msg1_ && waiting_event1_ == msg1->timing.event_id)
        {
            if (msg1->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time1 = msg1->timing.arrival_time;
                double queuing_latency1 = (arrival_time1 - earliest_arrival_time1_).seconds();
                
                cout << "Fused set: Msg 1 event ID: " << msg1->timing.event_id << endl;
                cout << "   Earliest arrival time: " << earliest_arrival_time1_.seconds() << "." << earliest_arrival_time1_.nanoseconds() << " Earliest selected time: " << arrival_time1.seconds() << "." << arrival_time1.nanoseconds() << " Output time: " << output_time.seconds() << "." << output_time.nanoseconds() << endl;
        
                if (queuing_latency1 > max_queuing_delay1_)
                {
                    max_queuing_delay1_ = queuing_latency1;
                }
                
                // Sync latency logging
                double sync_latency1 = (output_time - earliest_arrival_time1_).seconds();

                if (sync_latency1 > max_sync_latency1_)
                {
                    max_sync_latency1_ = sync_latency1;
                }

                count_event1_++;
            }

            wait_earliest_selected_msg1_ = false;
        }

        if (wait_earliest_selected_msg2_ && waiting_event2_ == msg2->timing.event_id)
        {   
            if (msg2->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time2 = msg2->timing.arrival_time;
                double queuing_latency2 = (arrival_time2 - earliest_arrival_time2_).seconds();
                
                if (queuing_latency2 > max_queuing_delay2_)
                {
                    max_queuing_delay2_ = queuing_latency2;
                }
                
                // Sync latency logging
                double sync_latency2 = (output_time - earliest_arrival_time2_).seconds();

                if (sync_latency2 > max_sync_latency2_)
                {
                    max_sync_latency2_ = sync_latency2;
                }

                count_event2_++;
            }

            wait_earliest_selected_msg2_ = false;
        }

        if (wait_earliest_selected_msg3_ && waiting_event3_ == msg3->timing.event_id)
        {   
            if (msg3->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time3 = msg3->timing.arrival_time;
                double queuing_latency3 = (arrival_time3 - earliest_arrival_time3_).seconds();
                
                if (queuing_latency3 > max_queuing_delay3_)
                {
                    max_queuing_delay3_ = queuing_latency3;
                }
                
                // Sync latency logging
                double sync_latency3 = (output_time - earliest_arrival_time3_).seconds();

                if (sync_latency3 > max_sync_latency3_)
                {
                    max_sync_latency3_ = sync_latency3;
                }
                
                count_event3_++;
            }

            wait_earliest_selected_msg3_ = false;
        }

        // Blocked delay logging
        rclcpp::Time start_time1 = msg1->header.temp_stamp;
        rclcpp::Time start_time2 = msg2->header.temp_stamp;
        rclcpp::Time start_time3 = msg3->header.temp_stamp;

        double delay1 = (output_time - start_time1).seconds();
        double delay2 = (output_time - start_time2).seconds();
        double delay3 = (output_time - start_time3).seconds();

        if (delay1 > max_blocked_delay1_)
        {
            max_blocked_delay1_ = delay1;
        }

        if (delay2 > max_blocked_delay2_)
        {
            max_blocked_delay2_ = delay2;
        }

        if (delay3 > max_blocked_delay3_)
        {
            max_blocked_delay3_ = delay3;
        }

        // double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        // double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        // double topic3_timestamp = (double)msg3->header.stamp.sec + 1e-9*(double)msg3->header.stamp.nanosec;
                    
        // outfile_mdl_ << topic1_timestamp << " " << topic2_timestamp << " " << topic3_timestamp << endl;
        // double time_disparity = cal_time_disparity(3, topic1_timestamp, topic2_timestamp, topic3_timestamp);

        // if(time_disparity > mdl_observed_wctd_)
        // {
        //     mdl_observed_wctd_ = time_disparity;
        // }
    }

    void callback1(sensor_msgs::msg::JointState::SharedPtr msg)
    {   
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time1_ = msg->timing.arrival_time;
            wait_earliest_selected_msg1_ = true;
            waiting_event1_ = msg->timing.event_id;
            cout << "Channel 1: Msg Event ID: " << msg->timing.event_id << endl;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous1_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<0>(msg);
        mdl_sync_.add<0>(msg);

        if (delay_previous1_ > max_trans_delay1_)
        {
            max_trans_delay1_ = delay_previous1_;
        }

        if (delay_previous1_ < min_trans_delay1_)
        {
            min_trans_delay1_ = delay_previous1_;
        }

        double topic1_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp1_ > 0)
            {
                double observed_now = topic1_timestamp - previous_timestamp1_;
                if(observed_now > observed_wcp1_)
                {
                    observed_wcp1_ = observed_now;
                }

                if(observed_now < observed_bcp1_)
                {
                    observed_bcp1_ = observed_now;
                }
            }
            previous_timestamp1_ = topic1_timestamp;
        }
    }

    void callback2(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time2_ = msg->timing.arrival_time;
            wait_earliest_selected_msg2_ = true;
            waiting_event2_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous2_ = absolute_delay.seconds() * Mstos;   

        // // alg_sync_.add<1>(msg);
        mdl_sync_.add<1>(msg);

        if (delay_previous2_ > max_trans_delay2_)
        {
            max_trans_delay2_ = delay_previous2_;
        }

        if (delay_previous2_ < min_trans_delay2_)
        {
            min_trans_delay2_ = delay_previous2_;
        }

        double topic2_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp2_ > 0)
            {
                double observed_now = topic2_timestamp - previous_timestamp2_;
                if(observed_now > observed_wcp2_)
                {
                    observed_wcp2_ = observed_now;
                }

                if(observed_now < observed_bcp2_)
                {
                    observed_bcp2_ = observed_now;
                }
            }
            previous_timestamp2_ = topic2_timestamp;
        }
    }

    void callback3(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time3_ = msg->timing.arrival_time;
            wait_earliest_selected_msg3_ = true;
            waiting_event3_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;

        delay_previous3_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<2>(msg);
        mdl_sync_.add<2>(msg);

        if (delay_previous3_ > max_trans_delay3_)
        {
            max_trans_delay3_ = delay_previous3_;
        }

        if (delay_previous3_ < min_trans_delay3_)
        {
            min_trans_delay3_ = delay_previous3_;
        }

        double topic3_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp3_ > 0)
            {
                double observed_now = topic3_timestamp - previous_timestamp3_;
                if(observed_now > observed_wcp3_)
                {
                    observed_wcp3_ = observed_now;
                }

                if(observed_now < observed_bcp3_)
                {
                    observed_bcp3_ = observed_now;
                }
            }
            previous_timestamp3_ = topic3_timestamp;
        }
    }

    rclcpp::CallbackGroup::SharedPtr callback_group_subscriber_;

    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr topic1_sub_;
    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr topic2_sub_;
    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr topic3_sub_;

    typedef Synchronizer<sync_policies::ApproximateTime<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > AlgSync;
    typedef Synchronizer<sync_policies::ApproximateTimeModel<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > MdlSync;

    AlgSync alg_sync_;
    MdlSync mdl_sync_;

    int period1_, period2_, period3_;

    int msg_delay_opt_; // Message delay options. 0: no delay, 2: delay 0-upper;
    int delay_upper_;   // Message delay upper limit (0, 10, 20, 30, 40)
    int num_published_set_; // Required number of published sets    
    
    int count_alg_, count_mdl_;
    double delay_previous1_, delay_previous2_, delay_previous3_;

    double alg_observerd_wctd_, mdl_observed_wctd_;
    double previous_timestamp1_, previous_timestamp2_, previous_timestamp3_;
    double observed_wcp1_, observed_wcp2_, observed_wcp3_;

    double observed_bcp1_ = FLT_MAX, observed_bcp2_ = FLT_MAX, observed_bcp3_ = FLT_MAX;

    double max_trans_delay1_ = 0., max_trans_delay2_ = 0., max_trans_delay3_ = 0.;
    double min_trans_delay1_ = FLT_MAX, min_trans_delay2_ = FLT_MAX, min_trans_delay3_ = FLT_MAX;
    double max_blocked_delay1_ = 0., max_blocked_delay2_ = 0., max_blocked_delay3_ = 0.;

    double max_queuing_delay1_ = 0., max_queuing_delay2_ = 0., max_queuing_delay3_ = 0.;
    double max_sync_latency1_ = 0., max_sync_latency2_ = 0., max_sync_latency3_ = 0.;
    rclcpp::Time earliest_arrival_time1_, earliest_arrival_time2_, earliest_arrival_time3_;
    bool wait_earliest_selected_msg1_ = false, wait_earliest_selected_msg2_ = false, wait_earliest_selected_msg3_ = false;
    unsigned int waiting_event1_, waiting_event2_, waiting_event3_;

    int count_event1_ = 0, count_event2_ = 0, count_event3_ = 0;
    int num_counted_events_;
};

class SubscriberTopic4
    : public rclcpp::Node
{
    ofstream& outfile_alg_;
    ofstream& outfile_mdl_;

    public:
    SubscriberTopic4(int period1, int period2, int period3, int period4, std::ofstream& outfile_alg, std::ofstream& outfile_mdl) :
        Node("subscriber_topic4"), outfile_alg_(outfile_alg), outfile_mdl_(outfile_mdl), 
        alg_sync_(buffer_size), mdl_sync_(), 
        period1_(period1), period2_(period2), period3_(period3), period4_(period4),
        count_alg_(0), count_mdl_(0), 
        delay_previous1_(0), delay_previous2_(0), delay_previous3_(0), delay_previous4_(0),
        alg_observerd_wctd_(0), mdl_observed_wctd_(0), 
        previous_timestamp1_(0), previous_timestamp2_(0), previous_timestamp3_(0), previous_timestamp4_(0), 
        observed_wcp1_(0), observed_wcp2_(0), observed_wcp3_(0), observed_wcp4_(0)
    {
        this->declare_parameter("num_counted_events", 100);
        this->declare_parameter("num_published_set", 1000);

        this->get_parameter("num_counted_events", num_counted_events_);
        this->get_parameter("num_published_set", num_published_set_);

        RCLCPP_INFO(this->get_logger(), "required number of published sets: %d.", num_published_set_);
        RCLCPP_INFO(this->get_logger(), "required number of counted events: %d.", num_counted_events_);

        callback_group_subscriber_ = this->create_callback_group(
        rclcpp::CallbackGroupType::Reentrant);

        // Each of these callback groups is basically a thread
        // Everything assigned to one of them gets bundled into the same thread
        auto sub_opt = rclcpp::SubscriptionOptions();
        sub_opt.callback_group = callback_group_subscriber_;

        topic1_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic1", 300, 
                                                                        std::bind(&SubscriberTopic4::callback1, this, _1));

        topic2_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic2", 300, 
                                                                        std::bind(&SubscriberTopic4::callback2, this, _1));

        topic3_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic3", 300, 
                                                                        std::bind(&SubscriberTopic4::callback3, this, _1));

        topic4_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic4", 300, 
                                                                        std::bind(&SubscriberTopic4::callback4, this, _1));

        // alg_sync_.registerCallback(std::bind(&SubscriberTopic4::alg_callback, this, _1, _2, _3, _4));
        mdl_sync_.registerCallback(std::bind(&SubscriberTopic4::mdl_callback, this, _1, _2, _3, _4));

        RCLCPP_INFO(this->get_logger(), "Period 1: %d, Period 2: %d, Period 3: %d, Period 4: %d",
                    period1, period2, period3, period4);

        // alg_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        // alg_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);
        // alg_sync_.setInterMessageLowerBound(2, PeriodBase * 0.001 * period3);
        // alg_sync_.setInterMessageLowerBound(3, PeriodBase * 0.001 * period4);

        mdl_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        mdl_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);
        mdl_sync_.setInterMessageLowerBound(2, PeriodBase * 0.001 * period3);
        mdl_sync_.setInterMessageLowerBound(3, PeriodBase * 0.001 * period4);

        // alg_sync_.setAgePenalty(0);
        mdl_sync_.setAgePenalty(0);
    }

    ~SubscriberTopic4()
    {
    }

    private:
    void alg_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg3, const sensor_msgs::msg::JointState::ConstSharedPtr& msg4)
    {
        if(count_alg_ > num_published_set_)
        {
            return;
        }

        if(count_alg_ == num_published_set_)
        {
            cout << "Approximate Time Algorithm has got enough published sets !" << endl;
            if(count_alg_ == num_published_set_ && count_mdl_ == num_published_set_)
            {
                count_alg_++;
                kill(getpid(),SIGINT);
            }
            return;
        }        
        count_alg_++;

        double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        double topic3_timestamp = (double)msg3->header.stamp.sec + 1e-9*(double)msg3->header.stamp.nanosec;
        double topic4_timestamp = (double)msg4->header.stamp.sec + 1e-9*(double)msg4->header.stamp.nanosec;
        outfile_alg_ << topic1_timestamp << " " << topic2_timestamp << " " << topic3_timestamp << " " << topic4_timestamp << endl;
        
        double time_disparity = cal_time_disparity(4, topic1_timestamp, topic2_timestamp, topic3_timestamp, topic4_timestamp);
        if(time_disparity > alg_observerd_wctd_)
        {
            alg_observerd_wctd_ = time_disparity;
        }

    }

    void mdl_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg3, const sensor_msgs::msg::JointState::ConstSharedPtr& msg4)
    {
        rclcpp::Time output_time = this->now();

        if(count_mdl_ >= num_published_set_ && count_event1_ >= num_counted_events_ && 
           count_event2_ >= num_counted_events_ && count_event3_ >= num_counted_events_ && count_event4_ >= num_counted_events_)
        {
            cout << "Approximate Time Model has got enough published sets !" << endl;

            outfile_mdl_ << observed_bcp1_ << " " << observed_bcp2_ << " " << observed_bcp3_ << " " << observed_bcp4_ << " ";
            outfile_mdl_ << observed_wcp1_ << " " << observed_wcp2_ << " " << observed_wcp3_ << " " << observed_wcp4_ << " ";
            outfile_mdl_ << min_trans_delay1_ /  Mstos << " " << min_trans_delay2_ /  Mstos << " " << min_trans_delay3_ /  Mstos << " " << min_trans_delay4_ /  Mstos << " ";
            outfile_mdl_ << max_trans_delay1_ /  Mstos << " " << max_trans_delay2_ /  Mstos << " " << max_trans_delay3_ /  Mstos << " " << max_trans_delay4_ /  Mstos << " ";
            outfile_mdl_ << max_queuing_delay1_ << " " << max_queuing_delay2_ << " " << max_queuing_delay3_<< " " << max_queuing_delay4_<< " ";
            outfile_mdl_ << max_blocked_delay1_ << " " << max_blocked_delay2_ << " " << max_blocked_delay3_ << " " << max_blocked_delay4_ << " ";
            outfile_mdl_ << max_sync_latency1_ << " " << max_sync_latency2_ << " " << max_sync_latency3_ << " " << max_sync_latency4_ << endl;

            kill(getpid(),SIGINT);
            return;    
        }
        count_mdl_++;

        if (wait_earliest_selected_msg1_ && waiting_event1_ == msg1->timing.event_id)
        {
            if (msg1->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time1 = msg1->timing.arrival_time;
                double queuing_latency1 = (arrival_time1 - earliest_arrival_time1_).seconds();
                
                if (queuing_latency1 > max_queuing_delay1_)
                {
                    max_queuing_delay1_ = queuing_latency1;
                }
                
                // Sync latency logging
                double sync_latency1 = (output_time - earliest_arrival_time1_).seconds();

                if (sync_latency1 > max_sync_latency1_)
                {
                    max_sync_latency1_ = sync_latency1;
                }

                count_event1_++;
            }

            wait_earliest_selected_msg1_ = false;
        }

        if (wait_earliest_selected_msg2_ && waiting_event2_ == msg2->timing.event_id)
        {   
            if (msg2->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time2 = msg2->timing.arrival_time;
                double queuing_latency2 = (arrival_time2 - earliest_arrival_time2_).seconds();
                
                if (queuing_latency2 > max_queuing_delay2_)
                {
                    max_queuing_delay2_ = queuing_latency2;
                }
                
                // Sync latency logging
                double sync_latency2 = (output_time - earliest_arrival_time2_).seconds();

                if (sync_latency2 > max_sync_latency2_)
                {
                    max_sync_latency2_ = sync_latency2;
                }
                
                count_event2_++;
            }

            wait_earliest_selected_msg2_ = false;
        }

        if (wait_earliest_selected_msg3_ && waiting_event3_ == msg3->timing.event_id)
        {   
            if (msg3->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time3 = msg3->timing.arrival_time;
                double queuing_latency3 = (arrival_time3 - earliest_arrival_time3_).seconds();
                
                if (queuing_latency3 > max_queuing_delay3_)
                {
                    max_queuing_delay3_ = queuing_latency3;
                }
                
                // Sync latency logging
                double sync_latency3 = (output_time - earliest_arrival_time3_).seconds();

                if (sync_latency3 > max_sync_latency3_)
                {
                    max_sync_latency3_ = sync_latency3;
                }

                count_event3_++;
            }
            
            wait_earliest_selected_msg3_ = false;
        }

        if (wait_earliest_selected_msg4_ && waiting_event4_ == msg4->timing.event_id)
        {   
            if (msg4->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time4 = msg4->timing.arrival_time;
                double queuing_latency4 = (arrival_time4 - earliest_arrival_time4_).seconds();
                
                if (queuing_latency4 > max_queuing_delay4_)
                {
                    max_queuing_delay4_ = queuing_latency4;
                }
                
                // Sync latency logging
                double sync_latency4 = (output_time - earliest_arrival_time4_).seconds();

                if (sync_latency4 > max_sync_latency4_)
                {
                    max_sync_latency4_ = sync_latency4;
                }

                count_event4_++;
            }

            wait_earliest_selected_msg4_ = false;
        }

        // Blocked delay logging
        rclcpp::Time start_time1 = msg1->header.temp_stamp;
        rclcpp::Time start_time2 = msg2->header.temp_stamp;
        rclcpp::Time start_time3 = msg3->header.temp_stamp;
        rclcpp::Time start_time4 = msg4->header.temp_stamp;

        double delay1 = (output_time - start_time1).seconds();
        double delay2 = (output_time - start_time2).seconds();
        double delay3 = (output_time - start_time3).seconds();
        double delay4 = (output_time - start_time4).seconds();

        if (delay1 > max_blocked_delay1_)
        {
            max_blocked_delay1_ = delay1;
        }

        if (delay2 > max_blocked_delay2_)
        {
            max_blocked_delay2_ = delay2;
        }

        if (delay3 > max_blocked_delay3_)
        {
            max_blocked_delay3_ = delay3;
        }

        if (delay4 > max_blocked_delay4_)
        {
            max_blocked_delay4_ = delay4;
        }

        // double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        // double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        // double topic3_timestamp = (double)msg3->header.stamp.sec + 1e-9*(double)msg3->header.stamp.nanosec;
        // double topic4_timestamp = (double)msg4->header.stamp.sec + 1e-9*(double)msg4->header.stamp.nanosec;
        
        // outfile_mdl_ << topic1_timestamp << " " << topic2_timestamp << " " << topic3_timestamp << " " << topic4_timestamp << endl;
        // double time_disparity = cal_time_disparity(4, topic1_timestamp, topic2_timestamp, topic3_timestamp, topic4_timestamp);
        // if(time_disparity > mdl_observed_wctd_)
        // {
        //     mdl_observed_wctd_ = time_disparity;
        // }
    }

    void callback1(sensor_msgs::msg::JointState::SharedPtr msg)
    {   
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time1_ = msg->timing.arrival_time;
            wait_earliest_selected_msg1_ = true;
            waiting_event1_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous1_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<0>(msg);
        mdl_sync_.add<0>(msg);

        if (delay_previous1_ > max_trans_delay1_)
        {
            max_trans_delay1_ = delay_previous1_;
        }

        if (delay_previous1_ < min_trans_delay1_)
        {
            min_trans_delay1_ = delay_previous1_;
        }

        double topic1_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp1_ > 0)
            {
                double observed_now = topic1_timestamp - previous_timestamp1_;
                if(observed_now > observed_wcp1_)
                {
                    observed_wcp1_ = observed_now;
                }

                if(observed_now < observed_bcp1_)
                {
                    observed_bcp1_ = observed_now;
                }
            }
            previous_timestamp1_ = topic1_timestamp;
        }
    }

    void callback2(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time2_ = msg->timing.arrival_time;
            wait_earliest_selected_msg2_ = true;
            waiting_event2_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous2_ = absolute_delay.seconds() * Mstos;   

        // // alg_sync_.add<1>(msg);
        mdl_sync_.add<1>(msg);

        if (delay_previous2_ > max_trans_delay2_)
        {
            max_trans_delay2_ = delay_previous2_;
        }

        if (delay_previous2_ < min_trans_delay2_)
        {
            min_trans_delay2_ = delay_previous2_;
        }

        double topic2_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp2_ > 0)
            {
                double observed_now = topic2_timestamp - previous_timestamp2_;
                if(observed_now > observed_wcp2_)
                {
                    observed_wcp2_ = observed_now;
                }

                if(observed_now < observed_bcp2_)
                {
                    observed_bcp2_ = observed_now;
                }
            }
            previous_timestamp2_ = topic2_timestamp;
        }
    }

    void callback3(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time3_ = msg->timing.arrival_time;
            wait_earliest_selected_msg3_ = true;
            waiting_event3_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous3_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<2>(msg);
        mdl_sync_.add<2>(msg);

        if (delay_previous3_ > max_trans_delay3_)
        {
            max_trans_delay3_ = delay_previous3_;
        }

        if (delay_previous3_ < min_trans_delay3_)
        {
            min_trans_delay3_ = delay_previous3_;
        }

        double topic3_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp3_ > 0)
            {
                double observed_now = topic3_timestamp - previous_timestamp3_;
                if(observed_now > observed_wcp3_)
                {
                    observed_wcp3_ = observed_now;
                }

                if(observed_now < observed_bcp3_)
                {
                    observed_bcp3_ = observed_now;
                }
            }
            previous_timestamp3_ = topic3_timestamp;
        }
    }

    void callback4(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time4_ = msg->timing.arrival_time;
            wait_earliest_selected_msg4_ = true;
            waiting_event4_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous4_ = absolute_delay.seconds() * Mstos; 

        // // alg_sync_.add<3>(msg);
        mdl_sync_.add<3>(msg);

        if (delay_previous4_ > max_trans_delay4_)
        {
            max_trans_delay4_ = delay_previous4_;
        }

        if (delay_previous4_ < min_trans_delay4_)
        {
            min_trans_delay4_ = delay_previous4_;
        }

        double topic4_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp4_ > 0)
            {
                double observed_now = topic4_timestamp - previous_timestamp4_;
                if(observed_now > observed_wcp4_)
                {
                    observed_wcp4_ = observed_now;
                }

                if(observed_now < observed_bcp4_)
                {
                    observed_bcp4_ = observed_now;
                }
            }
            previous_timestamp4_ = topic4_timestamp;
        }
    }

    rclcpp::CallbackGroup::SharedPtr callback_group_subscriber_;

    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr topic1_sub_;
    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr topic2_sub_;
    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr topic3_sub_;
    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr topic4_sub_;

    typedef Synchronizer<sync_policies::ApproximateTime<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > AlgSync;
    typedef Synchronizer<sync_policies::ApproximateTimeModel<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > MdlSync;

    AlgSync alg_sync_;
    MdlSync mdl_sync_;

    int period1_, period2_, period3_, period4_;

    int msg_delay_opt_; // Message delay options. 0: no delay,2: delay 0-upper;
    int delay_upper_;   // Message delay upper limit (0, 10, 20, 30, 40)
    int num_published_set_; // Required number of published sets
    
    int count_alg_, count_mdl_;
    double delay_previous1_, delay_previous2_, delay_previous3_, delay_previous4_;

    double alg_observerd_wctd_, mdl_observed_wctd_;
    double previous_timestamp1_, previous_timestamp2_, previous_timestamp3_, previous_timestamp4_;
    double observed_wcp1_, observed_wcp2_, observed_wcp3_, observed_wcp4_;

    double observed_bcp1_ = FLT_MAX, observed_bcp2_ = FLT_MAX, observed_bcp3_ = FLT_MAX, observed_bcp4_ = FLT_MAX;

    double max_trans_delay1_ = 0., max_trans_delay2_ = 0., max_trans_delay3_ = 0., max_trans_delay4_ = 0.;
    double min_trans_delay1_ = FLT_MAX, min_trans_delay2_ = FLT_MAX, min_trans_delay3_ = FLT_MAX, min_trans_delay4_ = FLT_MAX;
    double max_blocked_delay1_ = 0., max_blocked_delay2_ = 0., max_blocked_delay3_ = 0., max_blocked_delay4_ = 0.;

    double max_queuing_delay1_ = 0., max_queuing_delay2_ = 0., max_queuing_delay3_ = 0., max_queuing_delay4_ = 0.;
    double max_sync_latency1_ = 0., max_sync_latency2_ = 0., max_sync_latency3_ = 0., max_sync_latency4_ = 0.;
    rclcpp::Time earliest_arrival_time1_, earliest_arrival_time2_, earliest_arrival_time3_, earliest_arrival_time4_;
    bool wait_earliest_selected_msg1_ = false, wait_earliest_selected_msg2_ = false, wait_earliest_selected_msg3_ = false, wait_earliest_selected_msg4_ = false;
    unsigned int waiting_event1_, waiting_event2_, waiting_event3_, waiting_event4_;

    int count_event1_ = 0, count_event2_ = 0, count_event3_ = 0, count_event4_ = 0;
    int num_counted_events_;
};

class SubscriberTopic5
    : public rclcpp::Node
{
    ofstream& outfile_alg_;
    ofstream& outfile_mdl_;

    public:
    SubscriberTopic5(int period1, int period2, int period3, int period4, int period5, std::ofstream& outfile_alg, std::ofstream& outfile_mdl) :
        Node("subscriber_topic5"), outfile_alg_(outfile_alg), outfile_mdl_(outfile_mdl),
        alg_sync_(buffer_size), mdl_sync_(), 
        period1_(period1), period2_(period2), period3_(period3), period4_(period4), period5_(period5),
        count_alg_(0), count_mdl_(0), 
        delay_previous1_(0), delay_previous2_(0), delay_previous3_(0), delay_previous4_(0), delay_previous5_(0),
        alg_observerd_wctd_(0), mdl_observed_wctd_(0), 
        previous_timestamp1_(0), previous_timestamp2_(0), previous_timestamp3_(0), previous_timestamp4_(0), previous_timestamp5_(0),
        observed_wcp1_(0), observed_wcp2_(0), observed_wcp3_(0), observed_wcp4_(0), observed_wcp5_(0)
    {
        this->declare_parameter("num_counted_events", 100);
        this->declare_parameter("num_published_set", 1000);

        this->get_parameter("num_counted_events", num_counted_events_);
        this->get_parameter("num_published_set", num_published_set_);

        RCLCPP_INFO(this->get_logger(), "required number of published sets: %d.", num_published_set_);
        RCLCPP_INFO(this->get_logger(), "required number of counted events: %d.", num_counted_events_);

        callback_group_subscriber_ = this->create_callback_group(
        rclcpp::CallbackGroupType::Reentrant);

        // Each of these callback groups is basically a thread
        // Everything assigned to one of them gets bundled into the same thread
        auto sub_opt = rclcpp::SubscriptionOptions();
        sub_opt.callback_group = callback_group_subscriber_;
        
        topic1_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic1", 300, 
                                                                        std::bind(&SubscriberTopic5::callback1, this, _1));

        topic2_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic2", 300, 
                                                                        std::bind(&SubscriberTopic5::callback2, this, _1));

        topic3_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic3", 300, 
                                                                        std::bind(&SubscriberTopic5::callback3, this, _1));

        topic4_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic4", 300, 
                                                                        std::bind(&SubscriberTopic5::callback4, this, _1));

        topic5_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic5", 300, 
                                                                        std::bind(&SubscriberTopic5::callback5, this, _1));

        // alg_sync_.registerCallback(std::bind(&SubscriberTopic5::alg_callback, this, _1, _2, _3, _4, _5));
        mdl_sync_.registerCallback(std::bind(&SubscriberTopic5::mdl_callback, this, _1, _2, _3, _4, _5));

        RCLCPP_INFO(this->get_logger(), "Period 1: %d, Period 2: %d, Period 3: %d, Period 4: %d, Period 5: %d",
                    period1, period2, period3, period4, period5);

        // alg_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        // alg_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);
        // alg_sync_.setInterMessageLowerBound(2, PeriodBase * 0.001 * period3);
        // alg_sync_.setInterMessageLowerBound(3, PeriodBase * 0.001 * period4);
        // alg_sync_.setInterMessageLowerBound(4, PeriodBase * 0.001 * period5);

        mdl_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        mdl_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);
        mdl_sync_.setInterMessageLowerBound(2, PeriodBase * 0.001 * period3);
        mdl_sync_.setInterMessageLowerBound(3, PeriodBase * 0.001 * period4);
        mdl_sync_.setInterMessageLowerBound(4, PeriodBase * 0.001 * period5);
        
        // alg_sync_.setAgePenalty(0);
        mdl_sync_.setAgePenalty(0);
    }

    ~SubscriberTopic5()
    {
    }

    private:
    void alg_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg3, const sensor_msgs::msg::JointState::ConstSharedPtr& msg4, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg5)
    {
        if(count_alg_ > num_published_set_)
        {
            return;
        }

        if(count_alg_ == num_published_set_)
        {
            cout << "Approximate Time Algorithm has got enough published sets !" << endl;
            if(count_alg_ == num_published_set_ && count_mdl_ == num_published_set_)
            {
                count_alg_++;
                kill(getpid(),SIGINT);
            }
            return;
        }        
        count_alg_++;

        double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        double topic3_timestamp = (double)msg3->header.stamp.sec + 1e-9*(double)msg3->header.stamp.nanosec;
        double topic4_timestamp = (double)msg4->header.stamp.sec + 1e-9*(double)msg4->header.stamp.nanosec;
        double topic5_timestamp = (double)msg5->header.stamp.sec + 1e-9*(double)msg5->header.stamp.nanosec;

        outfile_alg_ << topic1_timestamp << " " << topic2_timestamp << " " << topic3_timestamp << " " << topic4_timestamp << " " << topic5_timestamp << endl;        
        double time_disparity = cal_time_disparity(5, topic1_timestamp, topic2_timestamp, topic3_timestamp, topic4_timestamp, topic5_timestamp);
        if(time_disparity > alg_observerd_wctd_)
        {
            alg_observerd_wctd_ = time_disparity;
        }
    }

    void mdl_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg3, const sensor_msgs::msg::JointState::ConstSharedPtr& msg4,
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg5)
    {
        rclcpp::Time output_time = this->now();

        if(count_mdl_ >= num_published_set_ && count_event1_ >= num_counted_events_ && 
           count_event2_ >= num_counted_events_ && count_event3_ >= num_counted_events_ && 
           count_event4_ >= num_counted_events_ && count_event5_ >= num_counted_events_)
        {
            cout << "Approximate Time Model has got enough published sets !" << endl;

            outfile_mdl_ << observed_bcp1_ << " " << observed_bcp2_ << " " << observed_bcp3_ << " " << observed_bcp4_ << " " << observed_bcp5_ << " ";
            outfile_mdl_ << observed_wcp1_ << " " << observed_wcp2_ << " " << observed_wcp3_ << " " << observed_wcp4_ << " " << observed_wcp5_ << " ";
            outfile_mdl_ << min_trans_delay1_ /  Mstos << " " << min_trans_delay2_ /  Mstos << " " << min_trans_delay3_ /  Mstos << " " << min_trans_delay4_ /  Mstos << " " << min_trans_delay5_ /  Mstos << " ";
            outfile_mdl_ << max_trans_delay1_ /  Mstos << " " << max_trans_delay2_ /  Mstos << " " << max_trans_delay3_ /  Mstos << " " << max_trans_delay4_ /  Mstos << " " << max_trans_delay5_ /  Mstos << " ";
            outfile_mdl_ << max_queuing_delay1_ << " " << max_queuing_delay2_ << " " << max_queuing_delay3_<< " " << max_queuing_delay4_<< " " << max_queuing_delay5_<< " ";
            outfile_mdl_ << max_blocked_delay1_ << " " << max_blocked_delay2_ << " " << max_blocked_delay3_ << " " << max_blocked_delay4_ << " " << max_blocked_delay5_ << " ";
            outfile_mdl_ << max_sync_latency1_ << " " << max_sync_latency2_ << " " << max_sync_latency3_ << " " << max_sync_latency4_ << " " << max_sync_latency5_ << endl;

            kill(getpid(),SIGINT);
            return;    
        }
        count_mdl_++;

        if (wait_earliest_selected_msg1_ && waiting_event1_ == msg1->timing.event_id)
        {
            if (msg1->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time1 = msg1->timing.arrival_time;
                double queuing_latency1 = (arrival_time1 - earliest_arrival_time1_).seconds();
                
                if (queuing_latency1 > max_queuing_delay1_)
                {
                    max_queuing_delay1_ = queuing_latency1;
                }
                
                // Sync latency logging
                double sync_latency1 = (output_time - earliest_arrival_time1_).seconds();

                if (sync_latency1 > max_sync_latency1_)
                {
                    max_sync_latency1_ = sync_latency1;
                }

                count_event1_++;
            }

            wait_earliest_selected_msg1_ = false;
        }

        if (wait_earliest_selected_msg2_ && waiting_event2_ == msg2->timing.event_id)
        {   
            if (msg2->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time2 = msg2->timing.arrival_time;
                double queuing_latency2 = (arrival_time2 - earliest_arrival_time2_).seconds();
                
                if (queuing_latency2 > max_queuing_delay2_)
                {
                    max_queuing_delay2_ = queuing_latency2;
                }
                
                // Sync latency logging
                double sync_latency2 = (output_time - earliest_arrival_time2_).seconds();

                if (sync_latency2 > max_sync_latency2_)
                {
                    max_sync_latency2_ = sync_latency2;
                }

                count_event2_++;
            }

            wait_earliest_selected_msg2_ = false;
        }

        if (wait_earliest_selected_msg3_ && waiting_event3_ == msg3->timing.event_id)
        {   
            if (msg3->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time3 = msg3->timing.arrival_time;
                double queuing_latency3 = (arrival_time3 - earliest_arrival_time3_).seconds();
                
                if (queuing_latency3 > max_queuing_delay3_)
                {
                    max_queuing_delay3_ = queuing_latency3;
                }
                
                // Sync latency logging
                double sync_latency3 = (output_time - earliest_arrival_time3_).seconds();

                if (sync_latency3 > max_sync_latency3_)
                {
                    max_sync_latency3_ = sync_latency3;
                }

                count_event3_++;
            }

            wait_earliest_selected_msg3_ = false;
        }

        if (wait_earliest_selected_msg4_ && waiting_event4_ == msg4->timing.event_id)
        {   
            if (msg4->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time4 = msg4->timing.arrival_time;
                double queuing_latency4 = (arrival_time4 - earliest_arrival_time4_).seconds();
                
                if (queuing_latency4 > max_queuing_delay4_)
                {
                    max_queuing_delay4_ = queuing_latency4;
                }
                
                // Sync latency logging
                double sync_latency4 = (output_time - earliest_arrival_time4_).seconds();

                if (sync_latency4 > max_sync_latency4_)
                {
                    max_sync_latency4_ = sync_latency4;
                }

                count_event4_++;
            }

            wait_earliest_selected_msg4_ = false;
        }

        if (wait_earliest_selected_msg5_ && waiting_event5_ == msg5->timing.event_id)
        {   
            if (msg5->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time5 = msg5->timing.arrival_time;
                double queuing_latency5 = (arrival_time5 - earliest_arrival_time5_).seconds();
                
                if (queuing_latency5 > max_queuing_delay5_)
                {
                    max_queuing_delay5_ = queuing_latency5;
                }
                
                // Sync latency logging
                double sync_latency5 = (output_time - earliest_arrival_time5_).seconds();

                if (sync_latency5 > max_sync_latency5_)
                {
                    max_sync_latency5_ = sync_latency5;
                }

                count_event5_++;
            }

            wait_earliest_selected_msg5_ = false;
        }

        // Blocked delay logging
        rclcpp::Time start_time1 = msg1->header.temp_stamp;
        rclcpp::Time start_time2 = msg2->header.temp_stamp;
        rclcpp::Time start_time3 = msg3->header.temp_stamp;
        rclcpp::Time start_time4 = msg4->header.temp_stamp;
        rclcpp::Time start_time5 = msg5->header.temp_stamp;

        double delay1 = (output_time - start_time1).seconds();
        double delay2 = (output_time - start_time2).seconds();
        double delay3 = (output_time - start_time3).seconds();
        double delay4 = (output_time - start_time4).seconds();
        double delay5 = (output_time - start_time5).seconds();

        if (delay1 > max_blocked_delay1_)
        {
            max_blocked_delay1_ = delay1;
        }

        if (delay2 > max_blocked_delay2_)
        {
            max_blocked_delay2_ = delay2;
        }

        if (delay3 > max_blocked_delay3_)
        {
            max_blocked_delay3_ = delay3;
        }

        if (delay4 > max_blocked_delay4_)
        {
            max_blocked_delay4_ = delay4;
        }

        if (delay5 > max_blocked_delay5_)
        {
            max_blocked_delay5_ = delay5;
        }

        // double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        // double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        // double topic3_timestamp = (double)msg3->header.stamp.sec + 1e-9*(double)msg3->header.stamp.nanosec;
        // double topic4_timestamp = (double)msg4->header.stamp.sec + 1e-9*(double)msg4->header.stamp.nanosec;
        // double topic5_timestamp = (double)msg5->header.stamp.sec + 1e-9*(double)msg5->header.stamp.nanosec;

        // outfile_mdl_ << topic1_timestamp << " " << topic2_timestamp << " " << topic3_timestamp << " " << topic4_timestamp << " " << topic5_timestamp << endl;
        // double time_disparity = cal_time_disparity(5, topic1_timestamp, topic2_timestamp, topic3_timestamp, topic4_timestamp, topic5_timestamp);
        // if(time_disparity > mdl_observed_wctd_)
        // {
        //     mdl_observed_wctd_ = time_disparity;
        // }
    }

    void callback1(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time1_ = msg->timing.arrival_time;
            wait_earliest_selected_msg1_ = true;
            waiting_event1_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous1_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<0>(msg);
        mdl_sync_.add<0>(msg);

        if (delay_previous1_ > max_trans_delay1_)
        {
            max_trans_delay1_ = delay_previous1_;
        }

        if (delay_previous1_ < min_trans_delay1_)
        {
            min_trans_delay1_ = delay_previous1_;
        }

        double topic1_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp1_ > 0)
            {
                double observed_now = topic1_timestamp - previous_timestamp1_;
                if(observed_now > observed_wcp1_)
                {
                    observed_wcp1_ = observed_now;
                }

                if(observed_now < observed_bcp1_)
                {
                    observed_bcp1_ = observed_now;
                }
            }
            previous_timestamp1_ = topic1_timestamp;
        }
    }

    void callback2(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time2_ = msg->timing.arrival_time;
            wait_earliest_selected_msg2_ = true;
            waiting_event2_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous2_ = absolute_delay.seconds() * Mstos;   

        // // alg_sync_.add<1>(msg);
        mdl_sync_.add<1>(msg);

        if (delay_previous2_ > max_trans_delay2_)
        {
            max_trans_delay2_ = delay_previous2_;
        }

        if (delay_previous2_ < min_trans_delay2_)
        {
            min_trans_delay2_ = delay_previous2_;
        }

        double topic2_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp2_ > 0)
            {
                double observed_now = topic2_timestamp - previous_timestamp2_;
                if(observed_now > observed_wcp2_)
                {
                    observed_wcp2_ = observed_now;
                }

                if(observed_now < observed_bcp2_)
                {
                    observed_bcp2_ = observed_now;
                }
            }
            previous_timestamp2_ = topic2_timestamp;
        }
    }

    void callback3(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time3_ = msg->timing.arrival_time;
            wait_earliest_selected_msg3_ = true;
            waiting_event3_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous3_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<2>(msg);
        mdl_sync_.add<2>(msg);

        if (delay_previous3_ > max_trans_delay3_)
        {
            max_trans_delay3_ = delay_previous3_;
        }

        if (delay_previous3_ < min_trans_delay3_)
        {
            min_trans_delay3_ = delay_previous3_;
        }

        double topic3_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp3_ > 0)
            {
                double observed_now = topic3_timestamp - previous_timestamp3_;
                if(observed_now > observed_wcp3_)
                {
                    observed_wcp3_ = observed_now;
                }

                if(observed_now < observed_bcp3_)
                {
                    observed_bcp3_ = observed_now;
                }
            }
            previous_timestamp3_ = topic3_timestamp;
        }
    }

    void callback4(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time4_ = msg->timing.arrival_time;
            wait_earliest_selected_msg4_ = true;
            waiting_event4_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous4_ = absolute_delay.seconds() * Mstos; 

        // // alg_sync_.add<3>(msg);
        mdl_sync_.add<3>(msg);

        if (delay_previous4_ > max_trans_delay4_)
        {
            max_trans_delay4_ = delay_previous4_;
        }

        if (delay_previous4_ < min_trans_delay4_)
        {
            min_trans_delay4_ = delay_previous4_;
        }

        double topic4_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp4_ > 0)
            {
                double observed_now = topic4_timestamp - previous_timestamp4_;
                if(observed_now > observed_wcp4_)
                {
                    observed_wcp4_ = observed_now;
                }

                if(observed_now < observed_bcp4_)
                {
                    observed_bcp4_ = observed_now;
                }
            }
            previous_timestamp4_ = topic4_timestamp;
        }
    }

    void callback5(sensor_msgs::msg::JointState::SharedPtr msg)
    {       
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time5_ = msg->timing.arrival_time;
            wait_earliest_selected_msg5_ = true;
            waiting_event5_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous5_ = absolute_delay.seconds() * Mstos;
            
        // // alg_sync_.add<4>(msg);
        mdl_sync_.add<4>(msg);

        if (delay_previous5_ > max_trans_delay5_)
        {
            max_trans_delay5_ = delay_previous5_;
        }

        if (delay_previous5_ < min_trans_delay5_)
        {
            min_trans_delay5_ = delay_previous5_;
        }

        double topic5_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp5_ > 0)
            {
                double observed_now = topic5_timestamp - previous_timestamp5_;
                if(observed_now > observed_wcp5_)
                {
                    observed_wcp5_ = observed_now;
                }

                if(observed_now < observed_bcp5_)
                {
                    observed_bcp5_ = observed_now;
                }
            }
            previous_timestamp5_ = topic5_timestamp;
        }
    }

    rclcpp::CallbackGroup::SharedPtr callback_group_subscriber_;
    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr topic1_sub_, topic2_sub_, topic3_sub_, topic4_sub_, topic5_sub_;

    typedef Synchronizer<sync_policies::ApproximateTime<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > AlgSync;
    typedef Synchronizer<sync_policies::ApproximateTimeModel<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > MdlSync;

    AlgSync alg_sync_;
    MdlSync mdl_sync_;

    int period1_, period2_, period3_, period4_, period5_;

    int msg_delay_opt_; // Message delay options. 0: no delay, 2: delay 0-upper;
    int delay_upper_;   // Message delay upper limit (0, 10, 20, 30, 40)
    int num_published_set_; // Required number of published sets

    int count_alg_, count_mdl_;
    double delay_previous1_, delay_previous2_, delay_previous3_, delay_previous4_, delay_previous5_;

    double alg_observerd_wctd_, mdl_observed_wctd_;
    double previous_timestamp1_, previous_timestamp2_, previous_timestamp3_, previous_timestamp4_, previous_timestamp5_;
    double observed_wcp1_, observed_wcp2_, observed_wcp3_, observed_wcp4_, observed_wcp5_;

    double observed_bcp1_ = FLT_MAX, observed_bcp2_ = FLT_MAX, observed_bcp3_ = FLT_MAX, observed_bcp4_ = FLT_MAX, observed_bcp5_ = FLT_MAX;

    double max_trans_delay1_ = 0., max_trans_delay2_ = 0., max_trans_delay3_ = 0., max_trans_delay4_ = 0., max_trans_delay5_ = 0.;
    double min_trans_delay1_ = FLT_MAX, min_trans_delay2_ = FLT_MAX, min_trans_delay3_ = FLT_MAX, min_trans_delay4_ = FLT_MAX, min_trans_delay5_ = FLT_MAX;
    double max_blocked_delay1_ = 0., max_blocked_delay2_ = 0., max_blocked_delay3_ = 0., max_blocked_delay4_ = 0., max_blocked_delay5_ = 0.;

    double max_queuing_delay1_ = 0., max_queuing_delay2_ = 0., max_queuing_delay3_ = 0., max_queuing_delay4_ = 0., max_queuing_delay5_ = 0.;
    double max_sync_latency1_ = 0., max_sync_latency2_ = 0., max_sync_latency3_ = 0., max_sync_latency4_ = 0., max_sync_latency5_ = 0.;
    rclcpp::Time earliest_arrival_time1_, earliest_arrival_time2_, earliest_arrival_time3_, earliest_arrival_time4_, earliest_arrival_time5_;
    bool wait_earliest_selected_msg1_ = false, wait_earliest_selected_msg2_ = false, wait_earliest_selected_msg3_ = false, wait_earliest_selected_msg4_ = false, wait_earliest_selected_msg5_ = false;
    unsigned int waiting_event1_, waiting_event2_, waiting_event3_, waiting_event4_, waiting_event5_;

    int count_event1_ = 0, count_event2_ = 0, count_event3_ = 0, count_event4_ = 0, count_event5_ = 0;
    int num_counted_events_;
};

class SubscriberTopic6
    : public rclcpp::Node
{
    ofstream& outfile_alg_;
    ofstream& outfile_mdl_;

    public:
    SubscriberTopic6(int period1, int period2, int period3, int period4, int period5, int period6, std::ofstream& outfile_alg, std::ofstream& outfile_mdl) :
        Node("subscriber_topic6"), outfile_alg_(outfile_alg), outfile_mdl_(outfile_mdl),
        alg_sync_(buffer_size), mdl_sync_(), 
        period1_(period1), period2_(period2), period3_(period3), period4_(period4), period5_(period5), period6_(period6),
        count_alg_(0), count_mdl_(0), 
        delay_previous1_(0), delay_previous2_(0), delay_previous3_(0), delay_previous4_(0), delay_previous5_(0), delay_previous6_(0),
        alg_observerd_wctd_(0), mdl_observed_wctd_(0), 
        previous_timestamp1_(0), previous_timestamp2_(0), previous_timestamp3_(0), previous_timestamp4_(0), previous_timestamp5_(0), previous_timestamp6_(0), 
        observed_wcp1_(0), observed_wcp2_(0), observed_wcp3_(0), observed_wcp4_(0), observed_wcp5_(0), observed_wcp6_(0)
    {
        this->declare_parameter("num_counted_events", 100);
        this->declare_parameter("num_published_set", 1000);

        this->get_parameter("num_counted_events", num_counted_events_);
        this->get_parameter("num_published_set", num_published_set_);

        RCLCPP_INFO(this->get_logger(), "required number of published sets: %d.", num_published_set_);
        RCLCPP_INFO(this->get_logger(), "required number of counted events: %d.", num_counted_events_);

        callback_group_subscriber_ = this->create_callback_group(
        rclcpp::CallbackGroupType::Reentrant);

        // Each of these callback groups is basically a thread
        // Everything assigned to one of them gets bundled into the same thread
        auto sub_opt = rclcpp::SubscriptionOptions();
        sub_opt.callback_group = callback_group_subscriber_;

        topic1_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic1", 300, 
                                                                        std::bind(&SubscriberTopic6::callback1, this, _1));

        topic2_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic2", 300, 
                                                                        std::bind(&SubscriberTopic6::callback2, this, _1));

        topic3_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic3", 300, 
                                                                        std::bind(&SubscriberTopic6::callback3, this, _1));

        topic4_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic4", 300, 
                                                                        std::bind(&SubscriberTopic6::callback4, this, _1));

        topic5_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic5", 300, 
                                                                        std::bind(&SubscriberTopic6::callback5, this, _1));

        topic6_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic6", 300, 
                                                                        std::bind(&SubscriberTopic6::callback6, this, _1));

        // alg_sync_.registerCallback(std::bind(&SubscriberTopic6::alg_callback, this, _1, _2, _3, _4, _5, _6));
        mdl_sync_.registerCallback(std::bind(&SubscriberTopic6::mdl_callback, this, _1, _2, _3, _4, _5, _6));

        RCLCPP_INFO(this->get_logger(), "Period 1: %d, Period 2: %d, Period 3: %d, Period 4: %d, Period 5: %d, Period 6: %d",
                    period1, period2, period3, period4, period5, period6);

        // alg_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        // alg_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);
        // alg_sync_.setInterMessageLowerBound(2, PeriodBase * 0.001 * period3);
        // alg_sync_.setInterMessageLowerBound(3, PeriodBase * 0.001 * period4);
        // alg_sync_.setInterMessageLowerBound(4, PeriodBase * 0.001 * period5);
        // alg_sync_.setInterMessageLowerBound(5, PeriodBase * 0.001 * period6);
        
        mdl_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        mdl_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);
        mdl_sync_.setInterMessageLowerBound(2, PeriodBase * 0.001 * period3);
        mdl_sync_.setInterMessageLowerBound(3, PeriodBase * 0.001 * period4);
        mdl_sync_.setInterMessageLowerBound(4, PeriodBase * 0.001 * period5);
        mdl_sync_.setInterMessageLowerBound(5, PeriodBase * 0.001 * period6);
        
        // alg_sync_.setAgePenalty(0);
        mdl_sync_.setAgePenalty(0);
    }

    ~SubscriberTopic6()
    {
    }

    private:
    void alg_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg3, const sensor_msgs::msg::JointState::ConstSharedPtr& msg4, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg5, const sensor_msgs::msg::JointState::ConstSharedPtr& msg6)
    {
        if(count_alg_ > num_published_set_)
        {
            return;
        }

        if(count_alg_ == num_published_set_)
        {
            cout << "Approximate Time Algorithm has got enough published sets !" << endl;
            if(count_alg_ == num_published_set_ && count_mdl_ == num_published_set_)
            {
                count_alg_++;
                kill(getpid(),SIGINT);
            }
            return;
        }        
        count_alg_++;

        double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        double topic3_timestamp = (double)msg3->header.stamp.sec + 1e-9*(double)msg3->header.stamp.nanosec;
        double topic4_timestamp = (double)msg4->header.stamp.sec + 1e-9*(double)msg4->header.stamp.nanosec;
        double topic5_timestamp = (double)msg5->header.stamp.sec + 1e-9*(double)msg5->header.stamp.nanosec;
        double topic6_timestamp = (double)msg6->header.stamp.sec + 1e-9*(double)msg6->header.stamp.nanosec;

        outfile_alg_ << topic1_timestamp << " " << topic2_timestamp << " " << topic3_timestamp << " " << topic4_timestamp << " " << topic5_timestamp << " " << topic6_timestamp << endl;
        double time_disparity = cal_time_disparity(6, topic1_timestamp, topic2_timestamp, topic3_timestamp, topic4_timestamp, topic5_timestamp, topic6_timestamp);

        if(time_disparity > alg_observerd_wctd_)
        {
            alg_observerd_wctd_ = time_disparity;
        }
    }

    void mdl_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg3, const sensor_msgs::msg::JointState::ConstSharedPtr& msg4,
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg5, const sensor_msgs::msg::JointState::ConstSharedPtr& msg6)
    {
        rclcpp::Time output_time = this->now();

        if(count_mdl_ >= num_published_set_ && count_event1_ >= num_counted_events_ && 
           count_event2_ >= num_counted_events_ && count_event3_ >= num_counted_events_ && 
           count_event4_ >= num_counted_events_ && count_event5_ >= num_counted_events_ &&
           count_event6_ >= num_counted_events_)
        // if(count_mdl_ >= num_published_set_ * 10)
        {
            cout << "Approximate Time Model has got enough published sets !" << endl;

            outfile_mdl_ << observed_bcp1_ << " " << observed_bcp2_ << " " << observed_bcp3_ << " " << observed_bcp4_ << " " << observed_bcp5_ << " " << observed_bcp6_ << " ";
            outfile_mdl_ << observed_wcp1_ << " " << observed_wcp2_ << " " << observed_wcp3_ << " " << observed_wcp4_ << " " << observed_wcp5_ << " " << observed_wcp6_ << " ";
            outfile_mdl_ << min_trans_delay1_ /  Mstos << " " << min_trans_delay2_ /  Mstos << " " << min_trans_delay3_ /  Mstos << " " << min_trans_delay4_ /  Mstos << " " << min_trans_delay5_ /  Mstos << " " << min_trans_delay6_ /  Mstos << " ";
            outfile_mdl_ << max_trans_delay1_ /  Mstos << " " << max_trans_delay2_ /  Mstos << " " << max_trans_delay3_ /  Mstos << " " << max_trans_delay4_ /  Mstos << " " << max_trans_delay5_ /  Mstos << " " << max_trans_delay6_ /  Mstos << " ";
            outfile_mdl_ << max_queuing_delay1_ << " " << max_queuing_delay2_ << " " << max_queuing_delay3_<< " " << max_queuing_delay4_<< " " << max_queuing_delay5_ << " " << max_queuing_delay6_<< " ";
            outfile_mdl_ << max_blocked_delay1_ << " " << max_blocked_delay2_ << " " << max_blocked_delay3_ << " " << max_blocked_delay4_ << " " << max_blocked_delay5_ << " " << max_blocked_delay6_ << " ";
            outfile_mdl_ << max_sync_latency1_ << " " << max_sync_latency2_ << " " << max_sync_latency3_ << " " << max_sync_latency4_ << " " << max_sync_latency5_ << " " << max_sync_latency6_ << endl;
            // outfile_mdl_ << max_timestamp_differ1_ << " " << max_timestamp_differ2_ << " " << max_timestamp_differ3_ << " " << max_timestamp_differ4_ << " " << max_timestamp_differ5_ << " " << max_timestamp_differ6_ << endl;

            kill(getpid(),SIGINT);
            return;    
        }
        count_mdl_++;

        // if (count_mdl_ > 1)
        // {
        //     rclcpp::Time timestamp_now1 = msg1->header.temp_stamp;
        //     rclcpp::Time timestamp_now2 = msg2->header.temp_stamp;
        //     rclcpp::Time timestamp_now3 = msg3->header.temp_stamp;
        //     rclcpp::Time timestamp_now4 = msg4->header.temp_stamp;
        //     rclcpp::Time timestamp_now5 = msg5->header.temp_stamp;
        //     rclcpp::Time timestamp_now6 = msg6->header.temp_stamp;

        //     double timestamp_differ1 = (timestamp_now1 - timestamp_previous1_).seconds();
        //     double timestamp_differ2 = (timestamp_now2 - timestamp_previous2_).seconds();
        //     double timestamp_differ3 = (timestamp_now3 - timestamp_previous3_).seconds();
        //     double timestamp_differ4 = (timestamp_now4 - timestamp_previous4_).seconds();
        //     double timestamp_differ5 = (timestamp_now5 - timestamp_previous5_).seconds();
        //     double timestamp_differ6 = (timestamp_now6 - timestamp_previous6_).seconds();

        //     if (timestamp_differ1 > max_timestamp_differ1_)
        //     {
        //         max_timestamp_differ1_ = timestamp_differ1;
        //     }

        //     if (timestamp_differ2 > max_timestamp_differ2_)
        //     {
        //         max_timestamp_differ2_ = timestamp_differ2;
        //     }

        //     if (timestamp_differ3 > max_timestamp_differ3_)
        //     {
        //         max_timestamp_differ3_ = timestamp_differ3;
        //     }

        //     if (timestamp_differ4 > max_timestamp_differ4_)
        //     {
        //         max_timestamp_differ4_ = timestamp_differ4;
        //     }

        //     if (timestamp_differ5 > max_timestamp_differ5_)
        //     {
        //         max_timestamp_differ5_ = timestamp_differ5;
        //     }

        //     if (timestamp_differ6 > max_timestamp_differ6_)
        //     {
        //         max_timestamp_differ6_ = timestamp_differ6;
        //     }
        // }

        // timestamp_previous1_ = msg1->header.temp_stamp;
        // timestamp_previous2_ = msg2->header.temp_stamp;
        // timestamp_previous3_ = msg3->header.temp_stamp;
        // timestamp_previous4_ = msg4->header.temp_stamp;
        // timestamp_previous5_ = msg5->header.temp_stamp;
        // timestamp_previous6_ = msg6->header.temp_stamp;

        if (wait_earliest_selected_msg1_ && waiting_event1_ == msg1->timing.event_id)
        {
            if (msg1->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time1 = msg1->timing.arrival_time;
                double queuing_latency1 = (arrival_time1 - earliest_arrival_time1_).seconds();
        
                cout << "Fused set: Msg 1 event ID: " << msg1->timing.event_id << endl;
                cout << "   Earliest arrival time: " << earliest_arrival_time1_.seconds() << "." << earliest_arrival_time1_.nanoseconds() << " Earliest selected time: " << arrival_time1.seconds() << "." << arrival_time1.nanoseconds() << " Output time: " << output_time.seconds() << "." << output_time.nanoseconds() << endl;
        
                if (queuing_latency1 > max_queuing_delay1_)
                {
                    max_queuing_delay1_ = queuing_latency1;
                }
                
                // Sync latency logging
                double sync_latency1 = (output_time - earliest_arrival_time1_).seconds();

                if (sync_latency1 > max_sync_latency1_)
                {
                    max_sync_latency1_ = sync_latency1;
                }

                count_event1_++;
            }

            wait_earliest_selected_msg1_ = false;
        }

        if (wait_earliest_selected_msg2_ && waiting_event2_ == msg2->timing.event_id)
        {   
            if (msg2->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time2 = msg2->timing.arrival_time;
                double queuing_latency2 = (arrival_time2 - earliest_arrival_time2_).seconds();
                
                if (queuing_latency2 > max_queuing_delay2_)
                {
                    max_queuing_delay2_ = queuing_latency2;
                }
                
                // Sync latency logging
                double sync_latency2 = (output_time - earliest_arrival_time2_).seconds();

                if (sync_latency2 > max_sync_latency2_)
                {
                    max_sync_latency2_ = sync_latency2;
                }

                count_event2_++;
            }

            wait_earliest_selected_msg2_ = false;
        }

        if (wait_earliest_selected_msg3_ && waiting_event3_ == msg3->timing.event_id)
        {   
            if (msg3->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time3 = msg3->timing.arrival_time;
                double queuing_latency3 = (arrival_time3 - earliest_arrival_time3_).seconds();
                
                if (queuing_latency3 > max_queuing_delay3_)
                {
                    max_queuing_delay3_ = queuing_latency3;
                }
                
                // Sync latency logging
                double sync_latency3 = (output_time - earliest_arrival_time3_).seconds();

                if (sync_latency3 > max_sync_latency3_)
                {
                    max_sync_latency3_ = sync_latency3;
                }

                count_event3_++;
            }

            wait_earliest_selected_msg3_ = false;
        }

        if (wait_earliest_selected_msg4_ && waiting_event4_ == msg4->timing.event_id)
        {   
            if (msg4->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time4 = msg4->timing.arrival_time;
                double queuing_latency4 = (arrival_time4 - earliest_arrival_time4_).seconds();
                
                if (queuing_latency4 > max_queuing_delay4_)
                {
                    max_queuing_delay4_ = queuing_latency4;
                }
                
                // Sync latency logging
                double sync_latency4 = (output_time - earliest_arrival_time4_).seconds();

                if (sync_latency4 > max_sync_latency4_)
                {
                    max_sync_latency4_ = sync_latency4;
                }

                count_event4_++;
            }

            wait_earliest_selected_msg4_ = false;
        }

        if (wait_earliest_selected_msg5_ && waiting_event5_ == msg5->timing.event_id)
        {   
            if (msg5->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time5 = msg5->timing.arrival_time;
                double queuing_latency5 = (arrival_time5 - earliest_arrival_time5_).seconds();
                
                if (queuing_latency5 > max_queuing_delay5_)
                {
                    max_queuing_delay5_ = queuing_latency5;
                }
                
                // Sync latency logging
                double sync_latency5 = (output_time - earliest_arrival_time5_).seconds();

                if (sync_latency5 > max_sync_latency5_)
                {
                    max_sync_latency5_ = sync_latency5;
                }

                count_event5_++;
            }

            wait_earliest_selected_msg5_ = false;
        }

        if (wait_earliest_selected_msg6_ && waiting_event6_ == msg6->timing.event_id)
        {   
            if (msg6->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time6 = msg6->timing.arrival_time;
                double queuing_latency6 = (arrival_time6 - earliest_arrival_time6_).seconds();
                
                if (queuing_latency6 > max_queuing_delay6_)
                {
                    max_queuing_delay6_ = queuing_latency6;
                }
                
                // Sync latency logging
                double sync_latency6 = (output_time - earliest_arrival_time6_).seconds();

                if (sync_latency6 > max_sync_latency6_)
                {
                    max_sync_latency6_ = sync_latency6;
                }

                count_event6_++;
            }

            wait_earliest_selected_msg6_ = false;
        }

        // Blocked delay logging
        rclcpp::Time start_time1 = msg1->header.temp_stamp;
        rclcpp::Time start_time2 = msg2->header.temp_stamp;
        rclcpp::Time start_time3 = msg3->header.temp_stamp;
        rclcpp::Time start_time4 = msg4->header.temp_stamp;
        rclcpp::Time start_time5 = msg5->header.temp_stamp;
        rclcpp::Time start_time6 = msg6->header.temp_stamp;

        double delay1 = (output_time - start_time1).seconds();
        double delay2 = (output_time - start_time2).seconds();
        double delay3 = (output_time - start_time3).seconds();
        double delay4 = (output_time - start_time4).seconds();
        double delay5 = (output_time - start_time5).seconds();
        double delay6 = (output_time - start_time6).seconds();

        if (delay1 > max_blocked_delay1_)
        {
            max_blocked_delay1_ = delay1;
        }

        if (delay2 > max_blocked_delay2_)
        {
            max_blocked_delay2_ = delay2;
        }

        if (delay3 > max_blocked_delay3_)
        {
            max_blocked_delay3_ = delay3;
        }

        if (delay4 > max_blocked_delay4_)
        {
            max_blocked_delay4_ = delay4;
        }

        if (delay5 > max_blocked_delay5_)
        {
            max_blocked_delay5_ = delay5;
        }

        if (delay6 > max_blocked_delay6_)
        {
            max_blocked_delay6_ = delay6;
        }

        // double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        // double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        // double topic3_timestamp = (double)msg3->header.stamp.sec + 1e-9*(double)msg3->header.stamp.nanosec;
        // double topic4_timestamp = (double)msg4->header.stamp.sec + 1e-9*(double)msg4->header.stamp.nanosec;
        // double topic5_timestamp = (double)msg5->header.stamp.sec + 1e-9*(double)msg5->header.stamp.nanosec;
        // double topic6_timestamp = (double)msg6->header.stamp.sec + 1e-9*(double)msg6->header.stamp.nanosec;

        // outfile_mdl_ << topic1_timestamp << " " << topic2_timestamp << " " << topic3_timestamp << " " << topic4_timestamp << " " << topic5_timestamp << " " << topic6_timestamp << endl;
        // double time_disparity = cal_time_disparity(6, topic1_timestamp, topic2_timestamp, topic3_timestamp, topic4_timestamp, topic5_timestamp, topic6_timestamp);
        // if(time_disparity > mdl_observed_wctd_)
        // {
        //     mdl_observed_wctd_ = time_disparity;
        // }
    }

    void callback1(sensor_msgs::msg::JointState::SharedPtr msg)
    {       
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time1_ = msg->timing.arrival_time;
            wait_earliest_selected_msg1_ = true;
            waiting_event1_ = msg->timing.event_id;
            cout << "Channel 1: Msg Event ID: " << msg->timing.event_id << endl;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous1_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<0>(msg);
        mdl_sync_.add<0>(msg);

        if (delay_previous1_ > max_trans_delay1_)
        {
            max_trans_delay1_ = delay_previous1_;
        }

        if (delay_previous1_ < min_trans_delay1_)
        {
            min_trans_delay1_ = delay_previous1_;
        }

        double topic1_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp1_ > 0)
            {
                double observed_now = topic1_timestamp - previous_timestamp1_;
                if(observed_now > observed_wcp1_)
                {
                    observed_wcp1_ = observed_now;
                }

                if(observed_now < observed_bcp1_)
                {
                    observed_bcp1_ = observed_now;
                }
            }
            previous_timestamp1_ = topic1_timestamp;
        }
    }

    void callback2(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time2_ = msg->timing.arrival_time;
            wait_earliest_selected_msg2_ = true;
            waiting_event2_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous2_ = absolute_delay.seconds() * Mstos;   

        // // alg_sync_.add<1>(msg);
        mdl_sync_.add<1>(msg);

        if (delay_previous2_ > max_trans_delay2_)
        {
            max_trans_delay2_ = delay_previous2_;
        }

        if (delay_previous2_ < min_trans_delay2_)
        {
            min_trans_delay2_ = delay_previous2_;
        }

        double topic2_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp2_ > 0)
            {
                double observed_now = topic2_timestamp - previous_timestamp2_;
                if(observed_now > observed_wcp2_)
                {
                    observed_wcp2_ = observed_now;
                }

                if(observed_now < observed_bcp2_)
                {
                    observed_bcp2_ = observed_now;
                }
            }
            previous_timestamp2_ = topic2_timestamp;
        }
    }

    void callback3(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time3_ = msg->timing.arrival_time;
            wait_earliest_selected_msg3_ = true;
            waiting_event3_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous3_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<2>(msg);
        mdl_sync_.add<2>(msg);

        if (delay_previous3_ > max_trans_delay3_)
        {
            max_trans_delay3_ = delay_previous3_;
        }

        if (delay_previous3_ < min_trans_delay3_)
        {
            min_trans_delay3_ = delay_previous3_;
        }

        double topic3_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp3_ > 0)
            {
                double observed_now = topic3_timestamp - previous_timestamp3_;
                if(observed_now > observed_wcp3_)
                {
                    observed_wcp3_ = observed_now;
                }

                if(observed_now < observed_bcp3_)
                {
                    observed_bcp3_ = observed_now;
                }
            }
            previous_timestamp3_ = topic3_timestamp;
        }
    }

    void callback4(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time4_ = msg->timing.arrival_time;
            wait_earliest_selected_msg4_ = true;
            waiting_event4_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous4_ = absolute_delay.seconds() * Mstos; 

        // // alg_sync_.add<3>(msg);
        mdl_sync_.add<3>(msg);

        if (delay_previous4_ > max_trans_delay4_)
        {
            max_trans_delay4_ = delay_previous4_;
        }

        if (delay_previous4_ < min_trans_delay4_)
        {
            min_trans_delay4_ = delay_previous4_;
        }

        double topic4_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp4_ > 0)
            {
                double observed_now = topic4_timestamp - previous_timestamp4_;
                if(observed_now > observed_wcp4_)
                {
                    observed_wcp4_ = observed_now;
                }

                if(observed_now < observed_bcp4_)
                {
                    observed_bcp4_ = observed_now;
                }
            }
            previous_timestamp4_ = topic4_timestamp;
        }
    }

    void callback5(sensor_msgs::msg::JointState::SharedPtr msg)
    {       
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time5_ = msg->timing.arrival_time;
            wait_earliest_selected_msg5_ = true;
            waiting_event5_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous5_ = absolute_delay.seconds() * Mstos;
            
        // // alg_sync_.add<4>(msg);
        mdl_sync_.add<4>(msg);

        if (delay_previous5_ > max_trans_delay5_)
        {
            max_trans_delay5_ = delay_previous5_;
        }

        if (delay_previous5_ < min_trans_delay5_)
        {
            min_trans_delay5_ = delay_previous5_;
        }

        double topic5_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp5_ > 0)
            {
                double observed_now = topic5_timestamp - previous_timestamp5_;
                if(observed_now > observed_wcp5_)
                {
                    observed_wcp5_ = observed_now;
                }

                if(observed_now < observed_bcp5_)
                {
                    observed_bcp5_ = observed_now;
                }
            }
            previous_timestamp5_ = topic5_timestamp;
        }
    }

    void callback6(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time6_ = msg->timing.arrival_time;
            wait_earliest_selected_msg6_ = true;
            waiting_event6_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous6_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<5>(msg);
        mdl_sync_.add<5>(msg);

        if (delay_previous6_ > max_trans_delay6_)
        {
            max_trans_delay6_ = delay_previous6_;
        }

        if (delay_previous6_ < min_trans_delay6_)
        {
            min_trans_delay6_ = delay_previous6_;
        }

        double topic6_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp6_ > 0)
            {
                double observed_now = topic6_timestamp - previous_timestamp6_;
                if(observed_now > observed_wcp6_)
                {
                    observed_wcp6_ = observed_now;
                }

                if(observed_now < observed_bcp6_)
                {
                    observed_bcp6_ = observed_now;
                }
            }
            previous_timestamp6_ = topic6_timestamp;
        }
    }

    rclcpp::CallbackGroup::SharedPtr callback_group_subscriber_;
    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr topic1_sub_, topic2_sub_, topic3_sub_, topic4_sub_, topic5_sub_, topic6_sub_;

    typedef Synchronizer<sync_policies::ApproximateTime<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > AlgSync;
    typedef Synchronizer<sync_policies::ApproximateTimeModel<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > MdlSync;

    AlgSync alg_sync_;
    MdlSync mdl_sync_;

    int period1_, period2_, period3_, period4_, period5_, period6_;

    int msg_delay_opt_; // Message delay options. 0: no delay, 2: delay 0-upper;
    int delay_upper_;   // Message delay upper limit (0, 10, 20, 30, 40)
    int num_published_set_; // Required number of published sets
    
    int count_alg_, count_mdl_;
    double delay_previous1_, delay_previous2_, delay_previous3_, delay_previous4_, delay_previous5_, delay_previous6_;

    double alg_observerd_wctd_, mdl_observed_wctd_;
    double previous_timestamp1_, previous_timestamp2_, previous_timestamp3_, previous_timestamp4_, previous_timestamp5_, previous_timestamp6_;
    double observed_wcp1_, observed_wcp2_, observed_wcp3_, observed_wcp4_, observed_wcp5_, observed_wcp6_;

    double observed_bcp1_ = FLT_MAX, observed_bcp2_ = FLT_MAX, observed_bcp3_ = FLT_MAX, observed_bcp4_ = FLT_MAX, observed_bcp5_ = FLT_MAX, observed_bcp6_ = FLT_MAX;

    double max_trans_delay1_ = 0., max_trans_delay2_ = 0., max_trans_delay3_ = 0., max_trans_delay4_ = 0., max_trans_delay5_ = 0., max_trans_delay6_ = 0.;
    double min_trans_delay1_ = FLT_MAX, min_trans_delay2_ = FLT_MAX, min_trans_delay3_ = FLT_MAX, min_trans_delay4_ = FLT_MAX, min_trans_delay5_ = FLT_MAX, min_trans_delay6_ = FLT_MAX;
    double max_blocked_delay1_ = 0., max_blocked_delay2_ = 0., max_blocked_delay3_ = 0., max_blocked_delay4_ = 0., max_blocked_delay5_ = 0., max_blocked_delay6_ = 0.;

    double max_queuing_delay1_ = 0., max_queuing_delay2_ = 0., max_queuing_delay3_ = 0., max_queuing_delay4_ = 0., max_queuing_delay5_ = 0., max_queuing_delay6_ = 0.;
    double max_sync_latency1_ = 0., max_sync_latency2_ = 0., max_sync_latency3_ = 0., max_sync_latency4_ = 0., max_sync_latency5_ = 0., max_sync_latency6_ = 0.;
    rclcpp::Time earliest_arrival_time1_, earliest_arrival_time2_, earliest_arrival_time3_, earliest_arrival_time4_, earliest_arrival_time5_, earliest_arrival_time6_;
    bool wait_earliest_selected_msg1_ = false, wait_earliest_selected_msg2_ = false, wait_earliest_selected_msg3_ = false, wait_earliest_selected_msg4_ = false, wait_earliest_selected_msg5_ = false, wait_earliest_selected_msg6_ = false;
    unsigned int waiting_event1_, waiting_event2_, waiting_event3_, waiting_event4_, waiting_event5_, waiting_event6_;
    int count_event1_ = 0, count_event2_ = 0, count_event3_ = 0, count_event4_ = 0, count_event5_ = 0, count_event6_ = 0;
    int num_counted_events_;

    rclcpp::Time timestamp_previous1_, timestamp_previous2_, timestamp_previous3_, timestamp_previous4_, timestamp_previous5_, timestamp_previous6_;
    double max_timestamp_differ1_ = 0., max_timestamp_differ2_ = 0., max_timestamp_differ3_ = 0., max_timestamp_differ4_ = 0., max_timestamp_differ5_ = 0., max_timestamp_differ6_ = 0.;
};

class SubscriberTopic7
    : public rclcpp::Node
{
    ofstream& outfile_alg_;
    ofstream& outfile_mdl_;

    public:
    SubscriberTopic7(int period1, int period2, int period3, int period4, int period5, int period6, int period7, std::ofstream& outfile_alg, std::ofstream& outfile_mdl) :
        Node("subscriber_topic7"), outfile_alg_(outfile_alg), outfile_mdl_(outfile_mdl),
        alg_sync_(buffer_size), mdl_sync_(), 
        period1_(period1), period2_(period2), period3_(period3), period4_(period4), period5_(period5), period6_(period6), period7_(period7),
        count_alg_(0), count_mdl_(0), 
        delay_previous1_(0), delay_previous2_(0), delay_previous3_(0), delay_previous4_(0), delay_previous5_(0), delay_previous6_(0), delay_previous7_(0),
        alg_observerd_wctd_(0), mdl_observed_wctd_(0), 
        previous_timestamp1_(0), previous_timestamp2_(0), previous_timestamp3_(0), previous_timestamp4_(0), previous_timestamp5_(0), previous_timestamp6_(0), previous_timestamp7_(0),
        observed_wcp1_(0), observed_wcp2_(0), observed_wcp3_(0), observed_wcp4_(0), observed_wcp5_(0), observed_wcp6_(0), observed_wcp7_(0)
    {
        this->declare_parameter("num_counted_events", 100);
        this->declare_parameter("num_published_set", 1000);

        this->get_parameter("num_counted_events", num_counted_events_);
        this->get_parameter("num_published_set", num_published_set_);

        RCLCPP_INFO(this->get_logger(), "required number of published sets: %d.", num_published_set_);
        RCLCPP_INFO(this->get_logger(), "required number of counted events: %d.", num_counted_events_);

        callback_group_subscriber_ = this->create_callback_group(
        rclcpp::CallbackGroupType::Reentrant);

        // Each of these callback groups is basically a thread
        // Everything assigned to one of them gets bundled into the same thread
        auto sub_opt = rclcpp::SubscriptionOptions();
        sub_opt.callback_group = callback_group_subscriber_;

        topic1_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic1", 300, 
                                                                        std::bind(&SubscriberTopic7::callback1, this, _1));

        topic2_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic2", 300, 
                                                                        std::bind(&SubscriberTopic7::callback2, this, _1));

        topic3_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic3", 300, 
                                                                        std::bind(&SubscriberTopic7::callback3, this, _1));

        topic4_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic4", 300, 
                                                                        std::bind(&SubscriberTopic7::callback4, this, _1));

        topic5_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic5", 300, 
                                                                        std::bind(&SubscriberTopic7::callback5, this, _1));

        topic6_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic6", 300, 
                                                                        std::bind(&SubscriberTopic7::callback6, this, _1));

        topic7_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic7", 300, 
                                                                        std::bind(&SubscriberTopic7::callback7, this, _1));

        // alg_sync_.registerCallback(std::bind(&SubscriberTopic7::alg_callback, this, _1, _2, _3, _4, _5, _6, _7));
        mdl_sync_.registerCallback(std::bind(&SubscriberTopic7::mdl_callback, this, _1, _2, _3, _4, _5, _6, _7));

        RCLCPP_INFO(this->get_logger(), "Period 1: %d, Period 2: %d, Period 3: %d, Period 4: %d, Period 5: %d, Period 6: %d, Period 7: %d",
                    period1, period2, period3, period4, period5, period6, period7);

        // alg_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        // alg_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);
        // alg_sync_.setInterMessageLowerBound(2, PeriodBase * 0.001 * period3);
        // alg_sync_.setInterMessageLowerBound(3, PeriodBase * 0.001 * period4);
        // alg_sync_.setInterMessageLowerBound(4, PeriodBase * 0.001 * period5);
        // alg_sync_.setInterMessageLowerBound(5, PeriodBase * 0.001 * period6);
        // alg_sync_.setInterMessageLowerBound(6, PeriodBase * 0.001 * period7);

        mdl_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        mdl_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);
        mdl_sync_.setInterMessageLowerBound(2, PeriodBase * 0.001 * period3);
        mdl_sync_.setInterMessageLowerBound(3, PeriodBase * 0.001 * period4);
        mdl_sync_.setInterMessageLowerBound(4, PeriodBase * 0.001 * period5);
        mdl_sync_.setInterMessageLowerBound(5, PeriodBase * 0.001 * period6);
        mdl_sync_.setInterMessageLowerBound(6, PeriodBase * 0.001 * period7);
        
        // alg_sync_.setAgePenalty(0);
        mdl_sync_.setAgePenalty(0);
    }

    ~SubscriberTopic7()
    {
    }

    private:
    void alg_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg3, const sensor_msgs::msg::JointState::ConstSharedPtr& msg4, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg5, const sensor_msgs::msg::JointState::ConstSharedPtr& msg6, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg7)
    {
        if(count_alg_ > num_published_set_)
        {
            return;
        }

        if(count_alg_ == num_published_set_)
        {
            cout << "Approximate Time Algorithm has got enough published sets !" << endl;
            if(count_alg_ == num_published_set_ && count_mdl_ == num_published_set_)
            {
                count_alg_++;
                kill(getpid(),SIGINT);
            }
            return;
        }        
        count_alg_++;

        double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        double topic3_timestamp = (double)msg3->header.stamp.sec + 1e-9*(double)msg3->header.stamp.nanosec;
        double topic4_timestamp = (double)msg4->header.stamp.sec + 1e-9*(double)msg4->header.stamp.nanosec;
        double topic5_timestamp = (double)msg5->header.stamp.sec + 1e-9*(double)msg5->header.stamp.nanosec;
        double topic6_timestamp = (double)msg6->header.stamp.sec + 1e-9*(double)msg6->header.stamp.nanosec;
        double topic7_timestamp = (double)msg7->header.stamp.sec + 1e-9*(double)msg7->header.stamp.nanosec;

        outfile_alg_ << topic1_timestamp << " " << topic2_timestamp << " " << topic3_timestamp << " " << topic4_timestamp << " " << topic5_timestamp << " " << topic6_timestamp << " " << topic7_timestamp << endl;
        double time_disparity = cal_time_disparity(7, topic1_timestamp, topic2_timestamp, topic3_timestamp, topic4_timestamp, topic5_timestamp, topic6_timestamp, topic7_timestamp);
        if(time_disparity > alg_observerd_wctd_)
        {
            alg_observerd_wctd_ = time_disparity;
        }
    }

    void mdl_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg3, const sensor_msgs::msg::JointState::ConstSharedPtr& msg4,
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg5, const sensor_msgs::msg::JointState::ConstSharedPtr& msg6, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg7)
    {
        rclcpp::Time output_time = this->now();

        if(count_mdl_ >= num_published_set_ && count_event1_ >= num_counted_events_ && 
           count_event2_ >= num_counted_events_ && count_event3_ >= num_counted_events_ && 
           count_event4_ >= num_counted_events_ && count_event5_ >= num_counted_events_ &&
           count_event6_ >= num_counted_events_ && count_event7_ >= num_counted_events_)
        {
            cout << "Approximate Time Model has got enough published sets !" << endl;

            outfile_mdl_ << observed_bcp1_ << " " << observed_bcp2_ << " " << observed_bcp3_ << " " << observed_bcp4_ << " " << observed_bcp5_ << " " << observed_bcp6_ << " " << observed_bcp7_ << " ";
            outfile_mdl_ << observed_wcp1_ << " " << observed_wcp2_ << " " << observed_wcp3_ << " " << observed_wcp4_ << " " << observed_wcp5_ << " " << observed_wcp6_ << " " << observed_wcp7_ << " ";
            outfile_mdl_ << min_trans_delay1_ /  Mstos << " " << min_trans_delay2_ /  Mstos << " " << min_trans_delay3_ /  Mstos << " " << min_trans_delay4_ /  Mstos << " " << min_trans_delay5_ /  Mstos << " " << min_trans_delay6_ /  Mstos << " " << min_trans_delay7_ /  Mstos << " ";
            outfile_mdl_ << max_trans_delay1_ /  Mstos << " " << max_trans_delay2_ /  Mstos << " " << max_trans_delay3_ /  Mstos << " " << max_trans_delay4_ /  Mstos << " " << max_trans_delay5_ /  Mstos << " " << max_trans_delay6_ /  Mstos << " " << max_trans_delay7_ /  Mstos << " ";
            outfile_mdl_ << max_queuing_delay1_ << " " << max_queuing_delay2_ << " " << max_queuing_delay3_<< " " << max_queuing_delay4_<< " " << max_queuing_delay5_ << " " << max_queuing_delay6_ << " " << max_queuing_delay7_ << " ";
            outfile_mdl_ << max_blocked_delay1_ << " " << max_blocked_delay2_ << " " << max_blocked_delay3_ << " " << max_blocked_delay4_ << " " << max_blocked_delay5_ << " " << max_blocked_delay6_ << " " << max_blocked_delay7_ << " ";
            outfile_mdl_ << max_sync_latency1_ << " " << max_sync_latency2_ << " " << max_sync_latency3_ << " " << max_sync_latency4_ << " " << max_sync_latency5_ << " " << max_sync_latency6_ << " " << max_sync_latency7_ << endl;

            kill(getpid(),SIGINT);
            return;    
        }
        count_mdl_++;

        if (wait_earliest_selected_msg1_ && waiting_event1_ == msg1->timing.event_id)
        {
            if (msg1->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time1 = msg1->timing.arrival_time;
                double queuing_latency1 = (arrival_time1 - earliest_arrival_time1_).seconds();
                
                if (queuing_latency1 > max_queuing_delay1_)
                {
                    max_queuing_delay1_ = queuing_latency1;
                }
                
                // Sync latency logging
                double sync_latency1 = (output_time - earliest_arrival_time1_).seconds();

                if (sync_latency1 > max_sync_latency1_)
                {
                    max_sync_latency1_ = sync_latency1;
                }

                count_event1_++;
            }

            wait_earliest_selected_msg1_ = false;
        }

        if (wait_earliest_selected_msg2_ && waiting_event2_ == msg2->timing.event_id)
        {   
            if (msg2->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time2 = msg2->timing.arrival_time;
                double queuing_latency2 = (arrival_time2 - earliest_arrival_time2_).seconds();
                
                if (queuing_latency2 > max_queuing_delay2_)
                {
                    max_queuing_delay2_ = queuing_latency2;
                }
                
                // Sync latency logging
                double sync_latency2 = (output_time - earliest_arrival_time2_).seconds();

                if (sync_latency2 > max_sync_latency2_)
                {
                    max_sync_latency2_ = sync_latency2;
                }

                count_event2_++;
            }

            wait_earliest_selected_msg2_ = false;
        }

        if (wait_earliest_selected_msg3_ && waiting_event3_ == msg3->timing.event_id)
        {   
            if (msg3->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time3 = msg3->timing.arrival_time;
                double queuing_latency3 = (arrival_time3 - earliest_arrival_time3_).seconds();
                
                if (queuing_latency3 > max_queuing_delay3_)
                {
                    max_queuing_delay3_ = queuing_latency3;
                }
                
                // Sync latency logging
                double sync_latency3 = (output_time - earliest_arrival_time3_).seconds();

                if (sync_latency3 > max_sync_latency3_)
                {
                    max_sync_latency3_ = sync_latency3;
                }

                count_event3_++;
            }

            wait_earliest_selected_msg3_ = false;
        }

        if (wait_earliest_selected_msg4_ && waiting_event4_ == msg4->timing.event_id)
        {   
            if (msg4->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time4 = msg4->timing.arrival_time;
                double queuing_latency4 = (arrival_time4 - earliest_arrival_time4_).seconds();
                
                if (queuing_latency4 > max_queuing_delay4_)
                {
                    max_queuing_delay4_ = queuing_latency4;
                }
                
                // Sync latency logging
                double sync_latency4 = (output_time - earliest_arrival_time4_).seconds();

                if (sync_latency4 > max_sync_latency4_)
                {
                    max_sync_latency4_ = sync_latency4;
                }

                count_event4_++;
            }

            wait_earliest_selected_msg4_ = false;
        }

        if (wait_earliest_selected_msg5_ && waiting_event5_ == msg5->timing.event_id)
        {   
            if (msg5->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time5 = msg5->timing.arrival_time;
                double queuing_latency5 = (arrival_time5 - earliest_arrival_time5_).seconds();
                
                if (queuing_latency5 > max_queuing_delay5_)
                {
                    max_queuing_delay5_ = queuing_latency5;
                }
                
                // Sync latency logging
                double sync_latency5 = (output_time - earliest_arrival_time5_).seconds();

                if (sync_latency5 > max_sync_latency5_)
                {
                    max_sync_latency5_ = sync_latency5;
                }

                count_event5_++;
            }

            wait_earliest_selected_msg5_ = false;
        }

        if (wait_earliest_selected_msg6_ && waiting_event6_ == msg6->timing.event_id)
        {   
            if (msg6->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time6 = msg6->timing.arrival_time;
                double queuing_latency6 = (arrival_time6 - earliest_arrival_time6_).seconds();
                
                if (queuing_latency6 > max_queuing_delay6_)
                {
                    max_queuing_delay6_ = queuing_latency6;
                }
                
                // Sync latency logging
                double sync_latency6 = (output_time - earliest_arrival_time6_).seconds();

                if (sync_latency6 > max_sync_latency6_)
                {
                    max_sync_latency6_ = sync_latency6;
                }

                count_event6_++;
            }

            wait_earliest_selected_msg6_ = false;
        }

        if (wait_earliest_selected_msg7_ && waiting_event7_ == msg7->timing.event_id)
        {   
            if (msg7->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time7 = msg7->timing.arrival_time;
                double queuing_latency7 = (arrival_time7 - earliest_arrival_time7_).seconds();
                
                if (queuing_latency7 > max_queuing_delay7_)
                {
                    max_queuing_delay7_ = queuing_latency7;
                }
                
                // Sync latency logging
                double sync_latency7 = (output_time - earliest_arrival_time7_).seconds();

                if (sync_latency7 > max_sync_latency7_)
                {
                    max_sync_latency7_ = sync_latency7;
                }

                count_event7_++;
            }

            wait_earliest_selected_msg7_ = false;
        }

        // Blocked delay logging
        rclcpp::Time start_time1 = msg1->header.temp_stamp;
        rclcpp::Time start_time2 = msg2->header.temp_stamp;
        rclcpp::Time start_time3 = msg3->header.temp_stamp;
        rclcpp::Time start_time4 = msg4->header.temp_stamp;
        rclcpp::Time start_time5 = msg5->header.temp_stamp;
        rclcpp::Time start_time6 = msg6->header.temp_stamp;
        rclcpp::Time start_time7 = msg7->header.temp_stamp;

        double delay1 = (output_time - start_time1).seconds();
        double delay2 = (output_time - start_time2).seconds();
        double delay3 = (output_time - start_time3).seconds();
        double delay4 = (output_time - start_time4).seconds();
        double delay5 = (output_time - start_time5).seconds();
        double delay6 = (output_time - start_time6).seconds();
        double delay7 = (output_time - start_time7).seconds();

        if (delay1 > max_blocked_delay1_)
        {
            max_blocked_delay1_ = delay1;
        }

        if (delay2 > max_blocked_delay2_)
        {
            max_blocked_delay2_ = delay2;
        }

        if (delay3 > max_blocked_delay3_)
        {
            max_blocked_delay3_ = delay3;
        }

        if (delay4 > max_blocked_delay4_)
        {
            max_blocked_delay4_ = delay4;
        }

        if (delay5 > max_blocked_delay5_)
        {
            max_blocked_delay5_ = delay5;
        }

        if (delay6 > max_blocked_delay6_)
        {
            max_blocked_delay6_ = delay6;
        }

        if (delay7 > max_blocked_delay7_)
        {
            max_blocked_delay7_ = delay7;
        }

        // double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        // double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        // double topic3_timestamp = (double)msg3->header.stamp.sec + 1e-9*(double)msg3->header.stamp.nanosec;
        // double topic4_timestamp = (double)msg4->header.stamp.sec + 1e-9*(double)msg4->header.stamp.nanosec;
        // double topic5_timestamp = (double)msg5->header.stamp.sec + 1e-9*(double)msg5->header.stamp.nanosec;
        // double topic6_timestamp = (double)msg6->header.stamp.sec + 1e-9*(double)msg6->header.stamp.nanosec;
        // double topic7_timestamp = (double)msg7->header.stamp.sec + 1e-9*(double)msg7->header.stamp.nanosec;

        // outfile_mdl_ << topic1_timestamp << " " << topic2_timestamp << " " << topic3_timestamp << " " << topic4_timestamp << " " << topic5_timestamp << " " << topic6_timestamp << " " << topic7_timestamp << endl;
        // double time_disparity = cal_time_disparity(7, topic1_timestamp, topic2_timestamp, topic3_timestamp, topic4_timestamp, topic5_timestamp, topic6_timestamp, topic7_timestamp);
        // if(time_disparity > mdl_observed_wctd_)
        // {
        //     mdl_observed_wctd_ = time_disparity;
        // }
    }

    void callback1(sensor_msgs::msg::JointState::SharedPtr msg)
    {       
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time1_ = msg->timing.arrival_time;
            wait_earliest_selected_msg1_ = true;
            waiting_event1_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous1_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<0>(msg);
        mdl_sync_.add<0>(msg);

        if (delay_previous1_ > max_trans_delay1_)
        {
            max_trans_delay1_ = delay_previous1_;
        }

        if (delay_previous1_ < min_trans_delay1_)
        {
            min_trans_delay1_ = delay_previous1_;
        }

        double topic1_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp1_ > 0)
            {
                double observed_now = topic1_timestamp - previous_timestamp1_;
                if(observed_now > observed_wcp1_)
                {
                    observed_wcp1_ = observed_now;
                }

                if(observed_now < observed_bcp1_)
                {
                    observed_bcp1_ = observed_now;
                }
            }
            previous_timestamp1_ = topic1_timestamp;
        }
    }

    void callback2(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time2_ = msg->timing.arrival_time;
            wait_earliest_selected_msg2_ = true;
            waiting_event2_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous2_ = absolute_delay.seconds() * Mstos;   

        // // alg_sync_.add<1>(msg);
        mdl_sync_.add<1>(msg);

        if (delay_previous2_ > max_trans_delay2_)
        {
            max_trans_delay2_ = delay_previous2_;
        }

        if (delay_previous2_ < min_trans_delay2_)
        {
            min_trans_delay2_ = delay_previous2_;
        }

        double topic2_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp2_ > 0)
            {
                double observed_now = topic2_timestamp - previous_timestamp2_;
                if(observed_now > observed_wcp2_)
                {
                    observed_wcp2_ = observed_now;
                }

                if(observed_now < observed_bcp2_)
                {
                    observed_bcp2_ = observed_now;
                }
            }
            previous_timestamp2_ = topic2_timestamp;
        }
    }

    void callback3(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time3_ = msg->timing.arrival_time;
            wait_earliest_selected_msg3_ = true;
            waiting_event3_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous3_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<2>(msg);
        mdl_sync_.add<2>(msg);

        if (delay_previous3_ > max_trans_delay3_)
        {
            max_trans_delay3_ = delay_previous3_;
        }

        if (delay_previous3_ < min_trans_delay3_)
        {
            min_trans_delay3_ = delay_previous3_;
        }

        double topic3_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp3_ > 0)
            {
                double observed_now = topic3_timestamp - previous_timestamp3_;
                if(observed_now > observed_wcp3_)
                {
                    observed_wcp3_ = observed_now;
                }

                if(observed_now < observed_bcp3_)
                {
                    observed_bcp3_ = observed_now;
                }
            }
            previous_timestamp3_ = topic3_timestamp;
        }
    }

    void callback4(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time4_ = msg->timing.arrival_time;
            wait_earliest_selected_msg4_ = true;
            waiting_event4_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous4_ = absolute_delay.seconds() * Mstos; 

        // // alg_sync_.add<3>(msg);
        mdl_sync_.add<3>(msg);

        if (delay_previous4_ > max_trans_delay4_)
        {
            max_trans_delay4_ = delay_previous4_;
        }

        if (delay_previous4_ < min_trans_delay4_)
        {
            min_trans_delay4_ = delay_previous4_;
        }

        double topic4_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp4_ > 0)
            {
                double observed_now = topic4_timestamp - previous_timestamp4_;
                if(observed_now > observed_wcp4_)
                {
                    observed_wcp4_ = observed_now;
                }

                if(observed_now < observed_bcp4_)
                {
                    observed_bcp4_ = observed_now;
                }
            }
            previous_timestamp4_ = topic4_timestamp;
        }
    }

    void callback5(sensor_msgs::msg::JointState::SharedPtr msg)
    {       
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time5_ = msg->timing.arrival_time;
            wait_earliest_selected_msg5_ = true;
            waiting_event5_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous5_ = absolute_delay.seconds() * Mstos;
            
        // // alg_sync_.add<4>(msg);
        mdl_sync_.add<4>(msg);

        if (delay_previous5_ > max_trans_delay5_)
        {
            max_trans_delay5_ = delay_previous5_;
        }

        if (delay_previous5_ < min_trans_delay5_)
        {
            min_trans_delay5_ = delay_previous5_;
        }

        double topic5_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp5_ > 0)
            {
                double observed_now = topic5_timestamp - previous_timestamp5_;
                if(observed_now > observed_wcp5_)
                {
                    observed_wcp5_ = observed_now;
                }

                if(observed_now < observed_bcp5_)
                {
                    observed_bcp5_ = observed_now;
                }
            }
            previous_timestamp5_ = topic5_timestamp;
        }
    }

    void callback6(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time6_ = msg->timing.arrival_time;
            wait_earliest_selected_msg6_ = true;
            waiting_event6_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous6_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<5>(msg);
        mdl_sync_.add<5>(msg);

        if (delay_previous6_ > max_trans_delay6_)
        {
            max_trans_delay6_ = delay_previous6_;
        }

        if (delay_previous6_ < min_trans_delay6_)
        {
            min_trans_delay6_ = delay_previous6_;
        }

        double topic6_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp6_ > 0)
            {
                double observed_now = topic6_timestamp - previous_timestamp6_;
                if(observed_now > observed_wcp6_)
                {
                    observed_wcp6_ = observed_now;
                }

                if(observed_now < observed_bcp6_)
                {
                    observed_bcp6_ = observed_now;
                }
            }
            previous_timestamp6_ = topic6_timestamp;
        }
    }

    void callback7(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time7_ = msg->timing.arrival_time;
            wait_earliest_selected_msg7_ = true;
            waiting_event7_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous7_ = absolute_delay.seconds() * Mstos;  

        // // alg_sync_.add<6>(msg);
        mdl_sync_.add<6>(msg);

        if (delay_previous7_ > max_trans_delay7_)
        {
            max_trans_delay7_ = delay_previous7_;
        }

        if (delay_previous7_ < min_trans_delay7_)
        {
            min_trans_delay7_ = delay_previous7_;
        }

        double topic7_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp7_ > 0)
            {
                double observed_now = topic7_timestamp - previous_timestamp7_;
                if(observed_now > observed_wcp7_)
                {
                    observed_wcp7_ = observed_now;
                }

                if(observed_now < observed_bcp7_)
                {
                    observed_bcp7_ = observed_now;
                }
            }
            previous_timestamp7_ = topic7_timestamp;
        }
    }

    rclcpp::CallbackGroup::SharedPtr callback_group_subscriber_;
    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr topic1_sub_, topic2_sub_, topic3_sub_, topic4_sub_, topic5_sub_, topic6_sub_, topic7_sub_;

    typedef Synchronizer<sync_policies::ApproximateTime<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > AlgSync;
    typedef Synchronizer<sync_policies::ApproximateTimeModel<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > MdlSync;

    AlgSync alg_sync_;
    MdlSync mdl_sync_;

    int period1_, period2_, period3_, period4_, period5_, period6_, period7_;

    int msg_delay_opt_; // Message delay options. 0: no delay, 2: delay 0-upper;
    int delay_upper_;   // Message delay upper limit (0, 10, 20, 30, 40)
    int num_published_set_; // Required number of published sets
    
    int count_alg_, count_mdl_;
    double delay_previous1_, delay_previous2_, delay_previous3_, delay_previous4_, delay_previous5_, delay_previous6_, delay_previous7_;

    double alg_observerd_wctd_, mdl_observed_wctd_;
    double previous_timestamp1_, previous_timestamp2_, previous_timestamp3_, previous_timestamp4_, previous_timestamp5_, previous_timestamp6_, previous_timestamp7_;
    double observed_wcp1_, observed_wcp2_, observed_wcp3_, observed_wcp4_, observed_wcp5_, observed_wcp6_, observed_wcp7_;

    double observed_bcp1_ = FLT_MAX, observed_bcp2_ = FLT_MAX, observed_bcp3_ = FLT_MAX, observed_bcp4_ = FLT_MAX, observed_bcp5_ = FLT_MAX, observed_bcp6_ = FLT_MAX, observed_bcp7_ = FLT_MAX;

    double max_trans_delay1_ = 0., max_trans_delay2_ = 0., max_trans_delay3_ = 0., max_trans_delay4_ = 0., max_trans_delay5_ = 0., max_trans_delay6_ = 0., max_trans_delay7_ = 0.;
    double min_trans_delay1_ = FLT_MAX, min_trans_delay2_ = FLT_MAX, min_trans_delay3_ = FLT_MAX, min_trans_delay4_ = FLT_MAX, min_trans_delay5_ = FLT_MAX, min_trans_delay6_ = FLT_MAX, min_trans_delay7_ = FLT_MAX;
    double max_blocked_delay1_ = 0., max_blocked_delay2_ = 0., max_blocked_delay3_ = 0., max_blocked_delay4_ = 0., max_blocked_delay5_ = 0., max_blocked_delay6_ = 0., max_blocked_delay7_ = 0.;

    double max_queuing_delay1_ = 0., max_queuing_delay2_ = 0., max_queuing_delay3_ = 0., max_queuing_delay4_ = 0., max_queuing_delay5_ = 0., max_queuing_delay6_ = 0., max_queuing_delay7_ = 0.;
    double max_sync_latency1_ = 0., max_sync_latency2_ = 0., max_sync_latency3_ = 0., max_sync_latency4_ = 0., max_sync_latency5_ = 0., max_sync_latency6_ = 0., max_sync_latency7_ = 0.;
    rclcpp::Time earliest_arrival_time1_, earliest_arrival_time2_, earliest_arrival_time3_, earliest_arrival_time4_, earliest_arrival_time5_, earliest_arrival_time6_, earliest_arrival_time7_;
    bool wait_earliest_selected_msg1_ = false, wait_earliest_selected_msg2_ = false, wait_earliest_selected_msg3_ = false, wait_earliest_selected_msg4_ = false, wait_earliest_selected_msg5_ = false, wait_earliest_selected_msg6_ = false, wait_earliest_selected_msg7_ = false;
    unsigned int waiting_event1_, waiting_event2_, waiting_event3_, waiting_event4_, waiting_event5_, waiting_event6_, waiting_event7_;

    int count_event1_ = 0, count_event2_ = 0, count_event3_ = 0, count_event4_ = 0, count_event5_ = 0, count_event6_ = 0, count_event7_ = 0;
    int num_counted_events_;
};

class SubscriberTopic8
    : public rclcpp::Node
{
    ofstream& outfile_alg_;
    ofstream& outfile_mdl_;

    public:
    SubscriberTopic8(int period1, int period2, int period3, int period4, int period5, int period6, int period7, int period8, std::ofstream& outfile_alg, std::ofstream& outfile_mdl) :
        Node("subscriber_topic8"), outfile_alg_(outfile_alg), outfile_mdl_(outfile_mdl),
        alg_sync_(buffer_size), mdl_sync_(), 
        period1_(period1), period2_(period2), period3_(period3), period4_(period4), period5_(period5), period6_(period6), period7_(period7), period8_(period8),
        count_alg_(0), count_mdl_(0), 
        delay_previous1_(0), delay_previous2_(0), delay_previous3_(0), delay_previous4_(0), delay_previous5_(0), delay_previous6_(0), delay_previous7_(0), delay_previous8_(0),
        alg_observerd_wctd_(0), mdl_observed_wctd_(0), 
        previous_timestamp1_(0), previous_timestamp2_(0), previous_timestamp3_(0), previous_timestamp4_(0), previous_timestamp5_(0), previous_timestamp6_(0), previous_timestamp7_(0), previous_timestamp8_(0),
        observed_wcp1_(0), observed_wcp2_(0), observed_wcp3_(0), observed_wcp4_(0), observed_wcp5_(0), observed_wcp6_(0), observed_wcp7_(0), observed_wcp8_(0)
    {
        this->declare_parameter("num_counted_events", 100);
        this->declare_parameter("num_published_set", 1000);

        this->get_parameter("num_counted_events", num_counted_events_);
        this->get_parameter("num_published_set", num_published_set_);

        RCLCPP_INFO(this->get_logger(), "required number of published sets: %d.", num_published_set_);
        RCLCPP_INFO(this->get_logger(), "required number of counted events: %d.", num_counted_events_);

        callback_group_subscriber_ = this->create_callback_group(
        rclcpp::CallbackGroupType::Reentrant);

        // Each of these callback groups is basically a thread
        // Everything assigned to one of them gets bundled into the same thread
        auto sub_opt = rclcpp::SubscriptionOptions();
        sub_opt.callback_group = callback_group_subscriber_;

        topic1_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic1", 300, 
                                                                        std::bind(&SubscriberTopic8::callback1, this, _1));

        topic2_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic2", 300, 
                                                                        std::bind(&SubscriberTopic8::callback2, this, _1));

        topic3_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic3", 300, 
                                                                        std::bind(&SubscriberTopic8::callback3, this, _1));

        topic4_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic4", 300, 
                                                                        std::bind(&SubscriberTopic8::callback4, this, _1));

        topic5_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic5", 300, 
                                                                        std::bind(&SubscriberTopic8::callback5, this, _1));

        topic6_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic6", 300, 
                                                                        std::bind(&SubscriberTopic8::callback6, this, _1));

        topic7_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic7", 300, 
                                                                        std::bind(&SubscriberTopic8::callback7, this, _1));

        topic8_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic8", 300, 
                                                                        std::bind(&SubscriberTopic8::callback8, this, _1));

        // alg_sync_.registerCallback(std::bind(&SubscriberTopic8::alg_callback, this, _1, _2, _3, _4, _5, _6, _7, _8));
        mdl_sync_.registerCallback(std::bind(&SubscriberTopic8::mdl_callback, this, _1, _2, _3, _4, _5, _6, _7, _8));

        RCLCPP_INFO(this->get_logger(), "Period 1: %d, Period 2: %d, Period 3: %d, Period 4: %d, Period 5: %d, Period 6: %d, Period 7: %d, Period 8: %d",
                    period1, period2, period3, period4, period5, period6, period7, period8);

        // alg_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        // alg_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);
        // alg_sync_.setInterMessageLowerBound(2, PeriodBase * 0.001 * period3);
        // alg_sync_.setInterMessageLowerBound(3, PeriodBase * 0.001 * period4);
        // alg_sync_.setInterMessageLowerBound(4, PeriodBase * 0.001 * period5);
        // alg_sync_.setInterMessageLowerBound(5, PeriodBase * 0.001 * period6);
        // alg_sync_.setInterMessageLowerBound(6, PeriodBase * 0.001 * period7);
        // alg_sync_.setInterMessageLowerBound(7, PeriodBase * 0.001 * period8);

        mdl_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        mdl_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);
        mdl_sync_.setInterMessageLowerBound(2, PeriodBase * 0.001 * period3);
        mdl_sync_.setInterMessageLowerBound(3, PeriodBase * 0.001 * period4);
        mdl_sync_.setInterMessageLowerBound(4, PeriodBase * 0.001 * period5);
        mdl_sync_.setInterMessageLowerBound(5, PeriodBase * 0.001 * period6);
        mdl_sync_.setInterMessageLowerBound(6, PeriodBase * 0.001 * period7);
        mdl_sync_.setInterMessageLowerBound(7, PeriodBase * 0.001 * period8);
        
        // alg_sync_.setAgePenalty(0);
        mdl_sync_.setAgePenalty(0);
    }

    ~SubscriberTopic8()
    {
    }

    private:
    void alg_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg3, const sensor_msgs::msg::JointState::ConstSharedPtr& msg4, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg5, const sensor_msgs::msg::JointState::ConstSharedPtr& msg6, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg7, const sensor_msgs::msg::JointState::ConstSharedPtr& msg8)
    {
        if(count_alg_ > num_published_set_)
        {
            return;
        }

        if(count_alg_ == num_published_set_)
        {
            cout << "Approximate Time Algorithm has got enough published sets !" << endl;
            if(count_alg_ == num_published_set_ && count_mdl_ == num_published_set_)
            {
                count_alg_++;
                kill(getpid(),SIGINT);
            }
            return;
        }        
        count_alg_++;

        double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        double topic3_timestamp = (double)msg3->header.stamp.sec + 1e-9*(double)msg3->header.stamp.nanosec;
        double topic4_timestamp = (double)msg4->header.stamp.sec + 1e-9*(double)msg4->header.stamp.nanosec;
        double topic5_timestamp = (double)msg5->header.stamp.sec + 1e-9*(double)msg5->header.stamp.nanosec;
        double topic6_timestamp = (double)msg6->header.stamp.sec + 1e-9*(double)msg6->header.stamp.nanosec;
        double topic7_timestamp = (double)msg7->header.stamp.sec + 1e-9*(double)msg7->header.stamp.nanosec;
        double topic8_timestamp = (double)msg8->header.stamp.sec + 1e-9*(double)msg8->header.stamp.nanosec;

        outfile_alg_ << topic1_timestamp << " " << topic2_timestamp << " " << topic3_timestamp << " " << topic4_timestamp << " " << topic5_timestamp << " " << topic6_timestamp << " " << topic7_timestamp << " " << topic8_timestamp << endl;
        double time_disparity = cal_time_disparity(8, topic1_timestamp, topic2_timestamp, topic3_timestamp, topic4_timestamp, topic5_timestamp, topic6_timestamp, topic7_timestamp, topic8_timestamp);
        if(time_disparity > alg_observerd_wctd_)
        {
            alg_observerd_wctd_ = time_disparity;
        }
    }

    void mdl_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg3, const sensor_msgs::msg::JointState::ConstSharedPtr& msg4,
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg5, const sensor_msgs::msg::JointState::ConstSharedPtr& msg6, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg7, const sensor_msgs::msg::JointState::ConstSharedPtr& msg8)
    {
        rclcpp::Time output_time = this->now();

        if(count_mdl_ >= num_published_set_ && count_event1_ >= num_counted_events_ && 
           count_event2_ >= num_counted_events_ && count_event3_ >= num_counted_events_ && 
           count_event4_ >= num_counted_events_ && count_event5_ >= num_counted_events_ &&
           count_event6_ >= num_counted_events_ && count_event7_ >= num_counted_events_ &&
           count_event8_ >= num_counted_events_)
        {
            cout << "Approximate Time Model has got enough published sets !" << endl;

            outfile_mdl_ << observed_bcp1_ << " " << observed_bcp2_ << " " << observed_bcp3_ << " " << observed_bcp4_ << " " << observed_bcp5_ << " " << observed_bcp6_ << " " << observed_bcp7_ << " " << observed_bcp8_ << " ";
            outfile_mdl_ << observed_wcp1_ << " " << observed_wcp2_ << " " << observed_wcp3_ << " " << observed_wcp4_ << " " << observed_wcp5_ << " " << observed_wcp6_ << " " << observed_wcp7_ << " " << observed_wcp8_ << " ";
            outfile_mdl_ << min_trans_delay1_ /  Mstos << " " << min_trans_delay2_ /  Mstos << " " << min_trans_delay3_ /  Mstos << " " << min_trans_delay4_ /  Mstos << " " << min_trans_delay5_ /  Mstos << " " << min_trans_delay6_ /  Mstos << " " << min_trans_delay7_ /  Mstos << " " << min_trans_delay8_ /  Mstos << " ";
            outfile_mdl_ << max_trans_delay1_ /  Mstos << " " << max_trans_delay2_ /  Mstos << " " << max_trans_delay3_ /  Mstos << " " << max_trans_delay4_ /  Mstos << " " << max_trans_delay5_ /  Mstos << " " << max_trans_delay6_ /  Mstos << " " << max_trans_delay7_ /  Mstos << " " << max_trans_delay8_ /  Mstos << " ";
            outfile_mdl_ << max_queuing_delay1_ << " " << max_queuing_delay2_ << " " << max_queuing_delay3_<< " " << max_queuing_delay4_<< " " << max_queuing_delay5_ << " " << max_queuing_delay6_ << " " << max_queuing_delay7_ << " " << max_queuing_delay8_ << " ";
            outfile_mdl_ << max_blocked_delay1_ << " " << max_blocked_delay2_ << " " << max_blocked_delay3_ << " " << max_blocked_delay4_ << " " << max_blocked_delay5_ << " " << max_blocked_delay6_ << " " << max_blocked_delay7_ << " " << max_blocked_delay8_ << " ";
            outfile_mdl_ << max_sync_latency1_ << " " << max_sync_latency2_ << " " << max_sync_latency3_ << " " << max_sync_latency4_ << " " << max_sync_latency5_ << " " << max_sync_latency6_ << " " << max_sync_latency7_ << " " << max_sync_latency8_ << endl;

            kill(getpid(),SIGINT);
            return;    
        }
        count_mdl_++;

        if (wait_earliest_selected_msg1_ && waiting_event1_ == msg1->timing.event_id)
        {
            if (msg1->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time1 = msg1->timing.arrival_time;
                double queuing_latency1 = (arrival_time1 - earliest_arrival_time1_).seconds();
                
                if (queuing_latency1 > max_queuing_delay1_)
                {
                    max_queuing_delay1_ = queuing_latency1;
                }
                
                // Sync latency logging
                double sync_latency1 = (output_time - earliest_arrival_time1_).seconds();

                if (sync_latency1 > max_sync_latency1_)
                {
                    max_sync_latency1_ = sync_latency1;
                }

                count_event1_++;
            }
            
            wait_earliest_selected_msg1_ = false;
        }

        if (wait_earliest_selected_msg2_ && waiting_event2_ == msg2->timing.event_id)
        {   
            if (msg2->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time2 = msg2->timing.arrival_time;
                double queuing_latency2 = (arrival_time2 - earliest_arrival_time2_).seconds();
                
                if (queuing_latency2 > max_queuing_delay2_)
                {
                    max_queuing_delay2_ = queuing_latency2;
                }
                
                // Sync latency logging
                double sync_latency2 = (output_time - earliest_arrival_time2_).seconds();

                if (sync_latency2 > max_sync_latency2_)
                {
                    max_sync_latency2_ = sync_latency2;
                }

                count_event2_++;
            }

            wait_earliest_selected_msg2_ = false;
        }

        if (wait_earliest_selected_msg3_ && waiting_event3_ == msg3->timing.event_id)
        {   
            if (msg3->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time3 = msg3->timing.arrival_time;
                double queuing_latency3 = (arrival_time3 - earliest_arrival_time3_).seconds();
                
                if (queuing_latency3 > max_queuing_delay3_)
                {
                    max_queuing_delay3_ = queuing_latency3;
                }
                
                // Sync latency logging
                double sync_latency3 = (output_time - earliest_arrival_time3_).seconds();

                if (sync_latency3 > max_sync_latency3_)
                {
                    max_sync_latency3_ = sync_latency3;
                }

                count_event3_++;
            }

            wait_earliest_selected_msg3_ = false;
        }

        if (wait_earliest_selected_msg4_ && waiting_event4_ == msg4->timing.event_id)
        {   
            if (msg4->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time4 = msg4->timing.arrival_time;
                double queuing_latency4 = (arrival_time4 - earliest_arrival_time4_).seconds();
                
                if (queuing_latency4 > max_queuing_delay4_)
                {
                    max_queuing_delay4_ = queuing_latency4;
                }
                
                // Sync latency logging
                double sync_latency4 = (output_time - earliest_arrival_time4_).seconds();

                if (sync_latency4 > max_sync_latency4_)
                {
                    max_sync_latency4_ = sync_latency4;
                }

                count_event4_++;
            }

            wait_earliest_selected_msg4_ = false;
        }

        if (wait_earliest_selected_msg5_ && waiting_event5_ == msg5->timing.event_id)
        {   
            if (msg5->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time5 = msg5->timing.arrival_time;
                double queuing_latency5 = (arrival_time5 - earliest_arrival_time5_).seconds();
                
                if (queuing_latency5 > max_queuing_delay5_)
                {
                    max_queuing_delay5_ = queuing_latency5;
                }
                
                // Sync latency logging
                double sync_latency5 = (output_time - earliest_arrival_time5_).seconds();

                if (sync_latency5 > max_sync_latency5_)
                {
                    max_sync_latency5_ = sync_latency5;
                }

                count_event5_++;
            }

            wait_earliest_selected_msg5_ = false;
        }

        if (wait_earliest_selected_msg6_ && waiting_event6_ == msg6->timing.event_id)
        {   
            if (msg6->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time6 = msg6->timing.arrival_time;
                double queuing_latency6 = (arrival_time6 - earliest_arrival_time6_).seconds();
                
                if (queuing_latency6 > max_queuing_delay6_)
                {
                    max_queuing_delay6_ = queuing_latency6;
                }
                
                // Sync latency logging
                double sync_latency6 = (output_time - earliest_arrival_time6_).seconds();

                if (sync_latency6 > max_sync_latency6_)
                {
                    max_sync_latency6_ = sync_latency6;
                }

                count_event6_++;
            }

            wait_earliest_selected_msg6_ = false;
        }

        if (wait_earliest_selected_msg7_ && waiting_event7_ == msg7->timing.event_id)
        {   
            if (msg7->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time7 = msg7->timing.arrival_time;
                double queuing_latency7 = (arrival_time7 - earliest_arrival_time7_).seconds();
                
                if (queuing_latency7 > max_queuing_delay7_)
                {
                    max_queuing_delay7_ = queuing_latency7;
                }
                
                // Sync latency logging
                double sync_latency7 = (output_time - earliest_arrival_time7_).seconds();

                if (sync_latency7 > max_sync_latency7_)
                {
                    max_sync_latency7_ = sync_latency7;
                }

                count_event7_++;
            }

            wait_earliest_selected_msg7_ = false;
        }

        if (wait_earliest_selected_msg8_ && waiting_event8_ == msg8->timing.event_id)
        {   
            if (msg8->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time8 = msg8->timing.arrival_time;
                double queuing_latency8 = (arrival_time8 - earliest_arrival_time8_).seconds();
                
                if (queuing_latency8 > max_queuing_delay8_)
                {
                    max_queuing_delay8_ = queuing_latency8;
                }
                
                // Sync latency logging
                double sync_latency8 = (output_time - earliest_arrival_time8_).seconds();

                if (sync_latency8 > max_sync_latency8_)
                {
                    max_sync_latency8_ = sync_latency8;
                }

                count_event8_++;
            }

            wait_earliest_selected_msg8_ = false;
        }

        // Blocked delay logging
        rclcpp::Time start_time1 = msg1->header.temp_stamp;
        rclcpp::Time start_time2 = msg2->header.temp_stamp;
        rclcpp::Time start_time3 = msg3->header.temp_stamp;
        rclcpp::Time start_time4 = msg4->header.temp_stamp;
        rclcpp::Time start_time5 = msg5->header.temp_stamp;
        rclcpp::Time start_time6 = msg6->header.temp_stamp;
        rclcpp::Time start_time7 = msg7->header.temp_stamp;
        rclcpp::Time start_time8 = msg8->header.temp_stamp;

        double delay1 = (output_time - start_time1).seconds();
        double delay2 = (output_time - start_time2).seconds();
        double delay3 = (output_time - start_time3).seconds();
        double delay4 = (output_time - start_time4).seconds();
        double delay5 = (output_time - start_time5).seconds();
        double delay6 = (output_time - start_time6).seconds();
        double delay7 = (output_time - start_time7).seconds();
        double delay8 = (output_time - start_time8).seconds();

        if (delay1 > max_blocked_delay1_)
        {
            max_blocked_delay1_ = delay1;
        }

        if (delay2 > max_blocked_delay2_)
        {
            max_blocked_delay2_ = delay2;
        }

        if (delay3 > max_blocked_delay3_)
        {
            max_blocked_delay3_ = delay3;
        }

        if (delay4 > max_blocked_delay4_)
        {
            max_blocked_delay4_ = delay4;
        }

        if (delay5 > max_blocked_delay5_)
        {
            max_blocked_delay5_ = delay5;
        }

        if (delay6 > max_blocked_delay6_)
        {
            max_blocked_delay6_ = delay6;
        }

        if (delay7 > max_blocked_delay7_)
        {
            max_blocked_delay7_ = delay7;
        }

        if (delay8 > max_blocked_delay8_)
        {
            max_blocked_delay8_ = delay8;
        }

        // double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        // double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        // double topic3_timestamp = (double)msg3->header.stamp.sec + 1e-9*(double)msg3->header.stamp.nanosec;
        // double topic4_timestamp = (double)msg4->header.stamp.sec + 1e-9*(double)msg4->header.stamp.nanosec;
        // double topic5_timestamp = (double)msg5->header.stamp.sec + 1e-9*(double)msg5->header.stamp.nanosec;
        // double topic6_timestamp = (double)msg6->header.stamp.sec + 1e-9*(double)msg6->header.stamp.nanosec;
        // double topic7_timestamp = (double)msg7->header.stamp.sec + 1e-9*(double)msg7->header.stamp.nanosec;
        // double topic8_timestamp = (double)msg8->header.stamp.sec + 1e-9*(double)msg8->header.stamp.nanosec;

        // outfile_mdl_ << topic1_timestamp << " " << topic2_timestamp << " " << topic3_timestamp << " " << topic4_timestamp << " " << topic5_timestamp << " " << topic6_timestamp << " " << topic7_timestamp << " " << topic8_timestamp << endl;
        // double time_disparity = cal_time_disparity(8, topic1_timestamp, topic2_timestamp, topic3_timestamp, topic4_timestamp, topic5_timestamp, topic6_timestamp, topic7_timestamp, topic8_timestamp);
        // if(time_disparity > mdl_observed_wctd_)
        // {
        //     mdl_observed_wctd_ = time_disparity;
        // }
    }

    void callback1(sensor_msgs::msg::JointState::SharedPtr msg)
    {       
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time1_ = msg->timing.arrival_time;
            wait_earliest_selected_msg1_ = true;
            waiting_event1_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous1_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<0>(msg);
        mdl_sync_.add<0>(msg);

        if (delay_previous1_ > max_trans_delay1_)
        {
            max_trans_delay1_ = delay_previous1_;
        }

        if (delay_previous1_ < min_trans_delay1_)
        {
            min_trans_delay1_ = delay_previous1_;
        }

        double topic1_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp1_ > 0)
            {
                double observed_now = topic1_timestamp - previous_timestamp1_;
                if(observed_now > observed_wcp1_)
                {
                    observed_wcp1_ = observed_now;
                }

                if(observed_now < observed_bcp1_)
                {
                    observed_bcp1_ = observed_now;
                }
            }
            previous_timestamp1_ = topic1_timestamp;
        }
    }

    void callback2(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time2_ = msg->timing.arrival_time;
            wait_earliest_selected_msg2_ = true;
            waiting_event2_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous2_ = absolute_delay.seconds() * Mstos;   

        // // alg_sync_.add<1>(msg);
        mdl_sync_.add<1>(msg);

        if (delay_previous2_ > max_trans_delay2_)
        {
            max_trans_delay2_ = delay_previous2_;
        }

        if (delay_previous2_ < min_trans_delay2_)
        {
            min_trans_delay2_ = delay_previous2_;
        }

        double topic2_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp2_ > 0)
            {
                double observed_now = topic2_timestamp - previous_timestamp2_;
                if(observed_now > observed_wcp2_)
                {
                    observed_wcp2_ = observed_now;
                }

                if(observed_now < observed_bcp2_)
                {
                    observed_bcp2_ = observed_now;
                }
            }
            previous_timestamp2_ = topic2_timestamp;
        }
    }

    void callback3(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time3_ = msg->timing.arrival_time;
            wait_earliest_selected_msg3_ = true;
            waiting_event3_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous3_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<2>(msg);
        mdl_sync_.add<2>(msg);

        if (delay_previous3_ > max_trans_delay3_)
        {
            max_trans_delay3_ = delay_previous3_;
        }

        if (delay_previous3_ < min_trans_delay3_)
        {
            min_trans_delay3_ = delay_previous3_;
        }

        double topic3_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp3_ > 0)
            {
                double observed_now = topic3_timestamp - previous_timestamp3_;
                if(observed_now > observed_wcp3_)
                {
                    observed_wcp3_ = observed_now;
                }

                if(observed_now < observed_bcp3_)
                {
                    observed_bcp3_ = observed_now;
                }
            }
            previous_timestamp3_ = topic3_timestamp;
        }  
    }

    void callback4(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time4_ = msg->timing.arrival_time;
            wait_earliest_selected_msg4_ = true;
            waiting_event4_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous4_ = absolute_delay.seconds() * Mstos; 

        // // alg_sync_.add<3>(msg);
        mdl_sync_.add<3>(msg);

        if (delay_previous4_ > max_trans_delay4_)
        {
            max_trans_delay4_ = delay_previous4_;
        }

        if (delay_previous4_ < min_trans_delay4_)
        {
            min_trans_delay4_ = delay_previous4_;
        }

        double topic4_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp4_ > 0)
            {
                double observed_now = topic4_timestamp - previous_timestamp4_;
                if(observed_now > observed_wcp4_)
                {
                    observed_wcp4_ = observed_now;
                }

                if(observed_now < observed_bcp4_)
                {
                    observed_bcp4_ = observed_now;
                }
            }
            previous_timestamp4_ = topic4_timestamp;
        }
    }

    void callback5(sensor_msgs::msg::JointState::SharedPtr msg)
    {    
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time5_ = msg->timing.arrival_time;
            wait_earliest_selected_msg5_ = true;
            waiting_event5_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous5_ = absolute_delay.seconds() * Mstos;
            
        // // alg_sync_.add<4>(msg);
        mdl_sync_.add<4>(msg);

        if (delay_previous5_ > max_trans_delay5_)
        {
            max_trans_delay5_ = delay_previous5_;
        }

        if (delay_previous5_ < min_trans_delay5_)
        {
            min_trans_delay5_ = delay_previous5_;
        }

        double topic5_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp5_ > 0)
            {
                double observed_now = topic5_timestamp - previous_timestamp5_;
                if(observed_now > observed_wcp5_)
                {
                    observed_wcp5_ = observed_now;
                }

                if(observed_now < observed_bcp5_)
                {
                    observed_bcp5_ = observed_now;
                }
            }
            previous_timestamp5_ = topic5_timestamp;
        }
    }

    void callback6(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time6_ = msg->timing.arrival_time;
            wait_earliest_selected_msg6_ = true;
            waiting_event6_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous6_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<5>(msg);
        mdl_sync_.add<5>(msg);

        if (delay_previous6_ > max_trans_delay6_)
        {
            max_trans_delay6_ = delay_previous6_;
        }

        if (delay_previous6_ < min_trans_delay6_)
        {
            min_trans_delay6_ = delay_previous6_;
        }

        double topic6_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp6_ > 0)
            {
                double observed_now = topic6_timestamp - previous_timestamp6_;
                if(observed_now > observed_wcp6_)
                {
                    observed_wcp6_ = observed_now;
                }

                if(observed_now < observed_bcp6_)
                {
                    observed_bcp6_ = observed_now;
                }
            }
            previous_timestamp6_ = topic6_timestamp;
        }  
    }

    void callback7(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time7_ = msg->timing.arrival_time;
            wait_earliest_selected_msg7_ = true;
            waiting_event7_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous7_ = absolute_delay.seconds() * Mstos;  

        // // alg_sync_.add<6>(msg);
        mdl_sync_.add<6>(msg);

        if (delay_previous7_ > max_trans_delay7_)
        {
            max_trans_delay7_ = delay_previous7_;
        }

        if (delay_previous7_ < min_trans_delay7_)
        {
            min_trans_delay7_ = delay_previous7_;
        }

        double topic7_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp7_ > 0)
            {
                double observed_now = topic7_timestamp - previous_timestamp7_;
                if(observed_now > observed_wcp7_)
                {
                    observed_wcp7_ = observed_now;
                }

                if(observed_now < observed_bcp7_)
                {
                    observed_bcp7_ = observed_now;
                }
            }
            previous_timestamp7_ = topic7_timestamp;
        }
    }

    void callback8(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time8_ = msg->timing.arrival_time;
            wait_earliest_selected_msg8_ = true;
            waiting_event8_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous8_ = absolute_delay.seconds() * Mstos;      

        // // alg_sync_.add<7>(msg);      
        mdl_sync_.add<7>(msg);

        if (delay_previous8_ > max_trans_delay8_)
        {
            max_trans_delay8_ = delay_previous8_;
        }

        if (delay_previous8_ < min_trans_delay8_)
        {
            min_trans_delay8_ = delay_previous8_;
        }

        double topic8_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp8_ > 0)
            {
                double observed_now = topic8_timestamp - previous_timestamp8_;
                if(observed_now > observed_wcp8_)
                {
                    observed_wcp8_ = observed_now;
                }

                if(observed_now < observed_bcp8_)
                {
                    observed_bcp8_ = observed_now;
                }
            }
            previous_timestamp8_ = topic8_timestamp;
        }
    }

    rclcpp::CallbackGroup::SharedPtr callback_group_subscriber_;
    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr topic1_sub_, topic2_sub_, topic3_sub_, topic4_sub_, topic5_sub_, topic6_sub_, topic7_sub_, topic8_sub_;

    typedef Synchronizer<sync_policies::ApproximateTime<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > AlgSync;
    typedef Synchronizer<sync_policies::ApproximateTimeModel<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > MdlSync;

    AlgSync alg_sync_;
    MdlSync mdl_sync_;

    int period1_, period2_, period3_, period4_, period5_, period6_, period7_, period8_;

    int msg_delay_opt_; // Message delay options. 0: no delay, 2: delay 0-upper;
    int delay_upper_;   // Message delay upper limit (0, 10, 20, 30, 40)
    int num_published_set_; // Required number of published sets
    
    int count_alg_, count_mdl_;
    double delay_previous1_, delay_previous2_, delay_previous3_, delay_previous4_, delay_previous5_, delay_previous6_, delay_previous7_, delay_previous8_;

    double alg_observerd_wctd_, mdl_observed_wctd_;
    double previous_timestamp1_, previous_timestamp2_, previous_timestamp3_, previous_timestamp4_, previous_timestamp5_, previous_timestamp6_, previous_timestamp7_, previous_timestamp8_;
    double observed_wcp1_, observed_wcp2_, observed_wcp3_, observed_wcp4_, observed_wcp5_, observed_wcp6_, observed_wcp7_, observed_wcp8_;

    double observed_bcp1_ = FLT_MAX, observed_bcp2_ = FLT_MAX, observed_bcp3_ = FLT_MAX, observed_bcp4_ = FLT_MAX, observed_bcp5_ = FLT_MAX, observed_bcp6_ = FLT_MAX, observed_bcp7_ = FLT_MAX, observed_bcp8_ = FLT_MAX;

    double max_trans_delay1_ = 0., max_trans_delay2_ = 0., max_trans_delay3_ = 0., max_trans_delay4_ = 0., max_trans_delay5_ = 0., max_trans_delay6_ = 0., max_trans_delay7_ = 0., max_trans_delay8_ = 0.;
    double min_trans_delay1_ = FLT_MAX, min_trans_delay2_ = FLT_MAX, min_trans_delay3_ = FLT_MAX, min_trans_delay4_ = FLT_MAX, min_trans_delay5_ = FLT_MAX, min_trans_delay6_ = FLT_MAX, min_trans_delay7_ = FLT_MAX, min_trans_delay8_ = FLT_MAX;
    double max_blocked_delay1_ = 0., max_blocked_delay2_ = 0., max_blocked_delay3_ = 0., max_blocked_delay4_ = 0., max_blocked_delay5_ = 0., max_blocked_delay6_ = 0., max_blocked_delay7_ = 0., max_blocked_delay8_ = 0.;

    double max_queuing_delay1_ = 0., max_queuing_delay2_ = 0., max_queuing_delay3_ = 0., max_queuing_delay4_ = 0., max_queuing_delay5_ = 0., max_queuing_delay6_ = 0., max_queuing_delay7_ = 0., max_queuing_delay8_ = 0.;
    double max_sync_latency1_ = 0., max_sync_latency2_ = 0., max_sync_latency3_ = 0., max_sync_latency4_ = 0., max_sync_latency5_ = 0., max_sync_latency6_ = 0., max_sync_latency7_ = 0., max_sync_latency8_ = 0.;
    rclcpp::Time earliest_arrival_time1_, earliest_arrival_time2_, earliest_arrival_time3_, earliest_arrival_time4_, earliest_arrival_time5_, earliest_arrival_time6_, earliest_arrival_time7_, earliest_arrival_time8_;
    bool wait_earliest_selected_msg1_ = false, wait_earliest_selected_msg2_ = false, wait_earliest_selected_msg3_ = false, wait_earliest_selected_msg4_ = false, wait_earliest_selected_msg5_ = false, wait_earliest_selected_msg6_ = false, wait_earliest_selected_msg7_ = false, wait_earliest_selected_msg8_ = false;
    unsigned int waiting_event1_, waiting_event2_, waiting_event3_, waiting_event4_, waiting_event5_, waiting_event6_, waiting_event7_, waiting_event8_;

    int count_event1_ = 0, count_event2_ = 0, count_event3_ = 0, count_event4_ = 0, count_event5_ = 0, count_event6_ = 0, count_event7_ = 0, count_event8_ = 0;
    int num_counted_events_;
};

class SubscriberTopic9
    : public rclcpp::Node
{
    ofstream& outfile_alg_;
    ofstream& outfile_mdl_;

    public:
    SubscriberTopic9(int period1, int period2, int period3, int period4, int period5, int period6, int period7, int period8, int period9, std::ofstream& outfile_alg, std::ofstream& outfile_mdl) :
        Node("subscriber_topic9"), outfile_alg_(outfile_alg), outfile_mdl_(outfile_mdl), 
        alg_sync_(buffer_size), mdl_sync_(), 
        period1_(period1), period2_(period2), period3_(period3), period4_(period4), period5_(period5), period6_(period6), period7_(period7), period8_(period8), period9_(period9),
        count_alg_(0), count_mdl_(0), 
        delay_previous1_(0), delay_previous2_(0), delay_previous3_(0), delay_previous4_(0), delay_previous5_(0), delay_previous6_(0), delay_previous7_(0), delay_previous8_(0), delay_previous9_(0),
        alg_observerd_wctd_(0), mdl_observed_wctd_(0), 
        previous_timestamp1_(0), previous_timestamp2_(0), previous_timestamp3_(0), previous_timestamp4_(0), previous_timestamp5_(0), previous_timestamp6_(0), previous_timestamp7_(0), previous_timestamp8_(0), previous_timestamp9_(0), 
        observed_wcp1_(0), observed_wcp2_(0), observed_wcp3_(0), observed_wcp4_(0), observed_wcp5_(0), observed_wcp6_(0), observed_wcp7_(0), observed_wcp8_(0), observed_wcp9_(0)
    {
        this->declare_parameter("num_counted_events", 100);
        this->declare_parameter("num_published_set", 1000);

        this->get_parameter("num_counted_events", num_counted_events_);
        this->get_parameter("num_published_set", num_published_set_);

        RCLCPP_INFO(this->get_logger(), "required number of published sets: %d.", num_published_set_);
        RCLCPP_INFO(this->get_logger(), "required number of counted events: %d.", num_counted_events_);

        callback_group_subscriber_ = this->create_callback_group(
        rclcpp::CallbackGroupType::Reentrant);

        // Each of these callback groups is basically a thread
        // Everything assigned to one of them gets bundled into the same thread
        auto sub_opt = rclcpp::SubscriptionOptions();
        sub_opt.callback_group = callback_group_subscriber_;

        topic1_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic1", 300, 
                                                                        std::bind(&SubscriberTopic9::callback1, this, _1));

        topic2_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic2", 300, 
                                                                        std::bind(&SubscriberTopic9::callback2, this, _1));

        topic3_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic3", 300, 
                                                                        std::bind(&SubscriberTopic9::callback3, this, _1));

        topic4_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic4", 300, 
                                                                        std::bind(&SubscriberTopic9::callback4, this, _1));

        topic5_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic5", 300, 
                                                                        std::bind(&SubscriberTopic9::callback5, this, _1));

        topic6_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic6", 300, 
                                                                        std::bind(&SubscriberTopic9::callback6, this, _1));

        topic7_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic7", 300, 
                                                                        std::bind(&SubscriberTopic9::callback7, this, _1));

        topic8_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic8", 300, 
                                                                        std::bind(&SubscriberTopic9::callback8, this, _1));

        topic9_sub_ = this->create_subscription<sensor_msgs::msg::JointState>("topic8", 300, 
                                                                        std::bind(&SubscriberTopic9::callback9, this, _1));

        // alg_sync_.registerCallback(std::bind(&SubscriberTopic9::alg_callback, this, _1, _2, _3, _4, _5, _6, _7, _8, _9));
        mdl_sync_.registerCallback(std::bind(&SubscriberTopic9::mdl_callback, this, _1, _2, _3, _4, _5, _6, _7, _8, _9));

        RCLCPP_INFO(this->get_logger(), "Period 1: %d, Period 2: %d, Period 3: %d, Period 4: %d, Period 5: %d, Period 6: %d, Period 7: %d, Period 8: %d, Period 9: %d",
                    period1, period2, period3, period4, period5, period6, period7, period8, period9);

        // alg_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        // alg_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);
        // alg_sync_.setInterMessageLowerBound(2, PeriodBase * 0.001 * period3);
        // alg_sync_.setInterMessageLowerBound(3, PeriodBase * 0.001 * period4);
        // alg_sync_.setInterMessageLowerBound(4, PeriodBase * 0.001 * period5);
        // alg_sync_.setInterMessageLowerBound(5, PeriodBase * 0.001 * period6);
        // alg_sync_.setInterMessageLowerBound(6, PeriodBase * 0.001 * period7);
        // alg_sync_.setInterMessageLowerBound(7, PeriodBase * 0.001 * period8);
        // alg_sync_.setInterMessageLowerBound(8, PeriodBase * 0.001 * period9);

        mdl_sync_.setInterMessageLowerBound(0, PeriodBase * 0.001 * period1);
        mdl_sync_.setInterMessageLowerBound(1, PeriodBase * 0.001 * period2);
        mdl_sync_.setInterMessageLowerBound(2, PeriodBase * 0.001 * period3);
        mdl_sync_.setInterMessageLowerBound(3, PeriodBase * 0.001 * period4);
        mdl_sync_.setInterMessageLowerBound(4, PeriodBase * 0.001 * period5);
        mdl_sync_.setInterMessageLowerBound(5, PeriodBase * 0.001 * period6);
        mdl_sync_.setInterMessageLowerBound(6, PeriodBase * 0.001 * period7);
        mdl_sync_.setInterMessageLowerBound(7, PeriodBase * 0.001 * period8);
        mdl_sync_.setInterMessageLowerBound(8, PeriodBase * 0.001 * period9);
        
        // alg_sync_.setAgePenalty(0);
        mdl_sync_.setAgePenalty(0);
    }

    ~SubscriberTopic9()
    {
    }

    private:
    void alg_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg3, const sensor_msgs::msg::JointState::ConstSharedPtr& msg4, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg5, const sensor_msgs::msg::JointState::ConstSharedPtr& msg6, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg7, const sensor_msgs::msg::JointState::ConstSharedPtr& msg8, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg9)
    {
        if(count_alg_ > num_published_set_)
        {
            return;
        }

        if(count_alg_ == num_published_set_)
        {
            cout << "Approximate Time Algorithm has got enough published sets !" << endl;
            if(count_alg_ == num_published_set_ && count_mdl_ == num_published_set_)
            {
                count_alg_++;
                kill(getpid(),SIGINT);
            }
            return;
        }        
        count_alg_++;

        double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        double topic3_timestamp = (double)msg3->header.stamp.sec + 1e-9*(double)msg3->header.stamp.nanosec;
        double topic4_timestamp = (double)msg4->header.stamp.sec + 1e-9*(double)msg4->header.stamp.nanosec;
        double topic5_timestamp = (double)msg5->header.stamp.sec + 1e-9*(double)msg5->header.stamp.nanosec;
        double topic6_timestamp = (double)msg6->header.stamp.sec + 1e-9*(double)msg6->header.stamp.nanosec;
        double topic7_timestamp = (double)msg7->header.stamp.sec + 1e-9*(double)msg7->header.stamp.nanosec;
        double topic8_timestamp = (double)msg8->header.stamp.sec + 1e-9*(double)msg8->header.stamp.nanosec;
        double topic9_timestamp = (double)msg9->header.stamp.sec + 1e-9*(double)msg9->header.stamp.nanosec;

        outfile_alg_ << topic1_timestamp << " " << topic2_timestamp << " " << topic3_timestamp << " " << topic4_timestamp << " " << topic5_timestamp << " " << topic6_timestamp << " " << topic7_timestamp << " " << topic8_timestamp << " " << topic9_timestamp << endl;
        double time_disparity = cal_time_disparity(9, topic1_timestamp, topic2_timestamp, topic3_timestamp, topic4_timestamp, topic5_timestamp, topic6_timestamp, topic7_timestamp, topic8_timestamp, topic9_timestamp);
        if(time_disparity > alg_observerd_wctd_)
        {
            alg_observerd_wctd_ = time_disparity;
        }
    }

    void mdl_callback(const sensor_msgs::msg::JointState::ConstSharedPtr& msg1, const sensor_msgs::msg::JointState::ConstSharedPtr& msg2, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg3, const sensor_msgs::msg::JointState::ConstSharedPtr& msg4,
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg5, const sensor_msgs::msg::JointState::ConstSharedPtr& msg6, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg7, const sensor_msgs::msg::JointState::ConstSharedPtr& msg8, 
                            const sensor_msgs::msg::JointState::ConstSharedPtr& msg9)
    {
        rclcpp::Time output_time = this->now();

        if(count_mdl_ >= num_published_set_ && count_event1_ >= num_counted_events_ && 
           count_event2_ >= num_counted_events_ && count_event3_ >= num_counted_events_ && 
           count_event4_ >= num_counted_events_ && count_event5_ >= num_counted_events_ &&
           count_event6_ >= num_counted_events_ && count_event7_ >= num_counted_events_ &&
           count_event8_ >= num_counted_events_ && count_event9_ >= num_counted_events_)
        {
            cout << "Approximate Time Model has got enough published sets !" << endl;

            outfile_mdl_ << observed_bcp1_ << " " << observed_bcp2_ << " " << observed_bcp3_ << " " << observed_bcp4_ << " " << observed_bcp5_ << " " << observed_bcp6_ << " " << observed_bcp7_ << " " << observed_bcp8_ << " " << observed_bcp9_ << " ";
            outfile_mdl_ << observed_wcp1_ << " " << observed_wcp2_ << " " << observed_wcp3_ << " " << observed_wcp4_ << " " << observed_wcp5_ << " " << observed_wcp6_ << " " << observed_wcp7_ << " " << observed_wcp8_ << " " << observed_wcp9_ << " ";
            outfile_mdl_ << min_trans_delay1_ /  Mstos << " " << min_trans_delay2_ /  Mstos << " " << min_trans_delay3_ /  Mstos << " " << min_trans_delay4_ /  Mstos << " " << min_trans_delay5_ /  Mstos << " " << min_trans_delay6_ /  Mstos << " " << min_trans_delay7_ /  Mstos << " " << min_trans_delay8_ /  Mstos << " " << min_trans_delay9_ /  Mstos << " ";
            outfile_mdl_ << max_trans_delay1_ /  Mstos << " " << max_trans_delay2_ /  Mstos << " " << max_trans_delay3_ /  Mstos << " " << max_trans_delay4_ /  Mstos << " " << max_trans_delay5_ /  Mstos << " " << max_trans_delay6_ /  Mstos << " " << max_trans_delay7_ /  Mstos << " " << max_trans_delay8_ /  Mstos << " " << max_trans_delay9_ /  Mstos << " ";
            outfile_mdl_ << max_queuing_delay1_ << " " << max_queuing_delay2_ << " " << max_queuing_delay3_<< " " << max_queuing_delay4_<< " " << max_queuing_delay5_ << " " << max_queuing_delay6_ << " " << max_queuing_delay7_ << " " << max_queuing_delay8_ << " " << max_queuing_delay9_ << " ";
            outfile_mdl_ << max_blocked_delay1_ << " " << max_blocked_delay2_ << " " << max_blocked_delay3_ << " " << max_blocked_delay4_ << " " << max_blocked_delay5_ << " " << max_blocked_delay6_ << " " << max_blocked_delay7_ << " " << max_blocked_delay8_ << " " << max_blocked_delay9_ << " ";
            outfile_mdl_ << max_sync_latency1_ << " " << max_sync_latency2_ << " " << max_sync_latency3_ << " " << max_sync_latency4_ << " " << max_sync_latency5_ << " " << max_sync_latency6_ << " " << max_sync_latency7_ << " " << max_sync_latency8_ << " " << max_sync_latency9_ << endl;

            kill(getpid(),SIGINT);
            return;    
        }
        count_mdl_++;

        if (wait_earliest_selected_msg1_ && waiting_event1_ == msg1->timing.event_id)
        {
            if (msg1->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time1 = msg1->timing.arrival_time;
                double queuing_latency1 = (arrival_time1 - earliest_arrival_time1_).seconds();
                
                if (queuing_latency1 > max_queuing_delay1_)
                {
                    max_queuing_delay1_ = queuing_latency1;
                }
                
                // Sync latency logging
                double sync_latency1 = (output_time - earliest_arrival_time1_).seconds();

                if (sync_latency1 > max_sync_latency1_)
                {
                    max_sync_latency1_ = sync_latency1;
                }

                count_event1_++;
            }

            wait_earliest_selected_msg1_ = false;
        }

        if (wait_earliest_selected_msg2_ && waiting_event2_ == msg2->timing.event_id)
        {   
            if (msg2->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time2 = msg2->timing.arrival_time;
                double queuing_latency2 = (arrival_time2 - earliest_arrival_time2_).seconds();
                
                if (queuing_latency2 > max_queuing_delay2_)
                {
                    max_queuing_delay2_ = queuing_latency2;
                }
                
                // Sync latency logging
                double sync_latency2 = (output_time - earliest_arrival_time2_).seconds();

                if (sync_latency2 > max_sync_latency2_)
                {
                    max_sync_latency2_ = sync_latency2;
                }

                count_event2_++;
            }

            wait_earliest_selected_msg2_ = false;
        }

        if (wait_earliest_selected_msg3_ && waiting_event3_ == msg3->timing.event_id)
        {   
            if (msg3->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time3 = msg3->timing.arrival_time;
                double queuing_latency3 = (arrival_time3 - earliest_arrival_time3_).seconds();
                
                if (queuing_latency3 > max_queuing_delay3_)
                {
                    max_queuing_delay3_ = queuing_latency3;
                }
                
                // Sync latency logging
                double sync_latency3 = (output_time - earliest_arrival_time3_).seconds();

                if (sync_latency3 > max_sync_latency3_)
                {
                    max_sync_latency3_ = sync_latency3;
                }

                count_event3_++;
            }

            wait_earliest_selected_msg3_ = false;
        }

        if (wait_earliest_selected_msg4_ && waiting_event4_ == msg4->timing.event_id)
        {   
            if (msg4->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time4 = msg4->timing.arrival_time;
                double queuing_latency4 = (arrival_time4 - earliest_arrival_time4_).seconds();
                
                if (queuing_latency4 > max_queuing_delay4_)
                {
                    max_queuing_delay4_ = queuing_latency4;
                }
                
                // Sync latency logging
                double sync_latency4 = (output_time - earliest_arrival_time4_).seconds();

                if (sync_latency4 > max_sync_latency4_)
                {
                    max_sync_latency4_ = sync_latency4;
                }

                count_event4_++;
            }

            wait_earliest_selected_msg4_ = false;
        }

        if (wait_earliest_selected_msg5_ && waiting_event5_ == msg5->timing.event_id)
        {   
            if (msg5->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time5 = msg5->timing.arrival_time;
                double queuing_latency5 = (arrival_time5 - earliest_arrival_time5_).seconds();
                
                if (queuing_latency5 > max_queuing_delay5_)
                {
                    max_queuing_delay5_ = queuing_latency5;
                }
                
                // Sync latency logging
                double sync_latency5 = (output_time - earliest_arrival_time5_).seconds();

                if (sync_latency5 > max_sync_latency5_)
                {
                    max_sync_latency5_ = sync_latency5;
                }

                count_event5_++;
            }

            wait_earliest_selected_msg5_ = false;
        }

        if (wait_earliest_selected_msg6_ && waiting_event6_ == msg6->timing.event_id)
        {   
            if (msg6->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time6 = msg6->timing.arrival_time;
                double queuing_latency6 = (arrival_time6 - earliest_arrival_time6_).seconds();
                
                if (queuing_latency6 > max_queuing_delay6_)
                {
                    max_queuing_delay6_ = queuing_latency6;
                }
                
                // Sync latency logging
                double sync_latency6 = (output_time - earliest_arrival_time6_).seconds();

                if (sync_latency6 > max_sync_latency6_)
                {
                    max_sync_latency6_ = sync_latency6;
                }

                count_event6_++;
            }

            wait_earliest_selected_msg6_ = false;
        }

        if (wait_earliest_selected_msg7_ && waiting_event7_ == msg7->timing.event_id)
        {   
            if (msg7->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time7 = msg7->timing.arrival_time;
                double queuing_latency7 = (arrival_time7 - earliest_arrival_time7_).seconds();
                
                if (queuing_latency7 > max_queuing_delay7_)
                {
                    max_queuing_delay7_ = queuing_latency7;
                }
                
                // Sync latency logging
                double sync_latency7 = (output_time - earliest_arrival_time7_).seconds();

                if (sync_latency7 > max_sync_latency7_)
                {
                    max_sync_latency7_ = sync_latency7;
                }

                count_event7_++;
            }

            wait_earliest_selected_msg7_ = false;
        }

        if (wait_earliest_selected_msg8_ && waiting_event8_ == msg8->timing.event_id)
        {   
            if (msg8->timing.event_id >  0)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time8 = msg8->timing.arrival_time;
                double queuing_latency8 = (arrival_time8 - earliest_arrival_time8_).seconds();
                
                if (queuing_latency8 > max_queuing_delay8_)
                {
                    max_queuing_delay8_ = queuing_latency8;
                }
                
                // Sync latency logging
                double sync_latency8 = (output_time - earliest_arrival_time8_).seconds();

                if (sync_latency8 > max_sync_latency8_)
                {
                    max_sync_latency8_ = sync_latency8;
                }

                count_event8_++;
            }

            wait_earliest_selected_msg8_ = false;
        }

        if (wait_earliest_selected_msg9_ && waiting_event9_ == msg9->timing.event_id)
        {   
            if (msg9->timing.event_id > 4)
            {
                // Queuing delay logging
                rclcpp::Time arrival_time9 = msg9->timing.arrival_time;
                double queuing_latency9 = (arrival_time9 - earliest_arrival_time9_).seconds();
                
                if (queuing_latency9 > max_queuing_delay9_)
                {
                    max_queuing_delay9_ = queuing_latency9;
                }
                
                // Sync latency logging
                double sync_latency9 = (output_time - earliest_arrival_time9_).seconds();

                if (sync_latency9 > max_sync_latency9_)
                {
                    max_sync_latency9_ = sync_latency9;
                }

                count_event9_++;
            }

            wait_earliest_selected_msg9_ = false;
        }

        // Blocked delay logging
        rclcpp::Time start_time1 = msg1->header.temp_stamp;
        rclcpp::Time start_time2 = msg2->header.temp_stamp;
        rclcpp::Time start_time3 = msg3->header.temp_stamp;
        rclcpp::Time start_time4 = msg4->header.temp_stamp;
        rclcpp::Time start_time5 = msg5->header.temp_stamp;
        rclcpp::Time start_time6 = msg6->header.temp_stamp;
        rclcpp::Time start_time7 = msg7->header.temp_stamp;
        rclcpp::Time start_time8 = msg8->header.temp_stamp;
        rclcpp::Time start_time9 = msg9->header.temp_stamp;

        double delay1 = (output_time - start_time1).seconds();
        double delay2 = (output_time - start_time2).seconds();
        double delay3 = (output_time - start_time3).seconds();
        double delay4 = (output_time - start_time4).seconds();
        double delay5 = (output_time - start_time5).seconds();
        double delay6 = (output_time - start_time6).seconds();
        double delay7 = (output_time - start_time7).seconds();
        double delay8 = (output_time - start_time8).seconds();
        double delay9 = (output_time - start_time9).seconds();

        if (delay1 > max_blocked_delay1_)
        {
            max_blocked_delay1_ = delay1;
        }

        if (delay2 > max_blocked_delay2_)
        {
            max_blocked_delay2_ = delay2;
        }

        if (delay3 > max_blocked_delay3_)
        {
            max_blocked_delay3_ = delay3;
        }

        if (delay4 > max_blocked_delay4_)
        {
            max_blocked_delay4_ = delay4;
        }

        if (delay5 > max_blocked_delay5_)
        {
            max_blocked_delay5_ = delay5;
        }

        if (delay6 > max_blocked_delay6_)
        {
            max_blocked_delay6_ = delay6;
        }

        if (delay7 > max_blocked_delay7_)
        {
            max_blocked_delay7_ = delay7;
        }

        if (delay8 > max_blocked_delay8_)
        {
            max_blocked_delay8_ = delay8;
        }

        if (delay9 > max_blocked_delay9_)
        {
            max_blocked_delay9_ = delay9;
        }
        // double topic1_timestamp = (double)msg1->header.stamp.sec + 1e-9*(double)msg1->header.stamp.nanosec;
        // double topic2_timestamp = (double)msg2->header.stamp.sec + 1e-9*(double)msg2->header.stamp.nanosec;
        // double topic3_timestamp = (double)msg3->header.stamp.sec + 1e-9*(double)msg3->header.stamp.nanosec;
        // double topic4_timestamp = (double)msg4->header.stamp.sec + 1e-9*(double)msg4->header.stamp.nanosec;
        // double topic5_timestamp = (double)msg5->header.stamp.sec + 1e-9*(double)msg5->header.stamp.nanosec;
        // double topic6_timestamp = (double)msg6->header.stamp.sec + 1e-9*(double)msg6->header.stamp.nanosec;
        // double topic7_timestamp = (double)msg7->header.stamp.sec + 1e-9*(double)msg7->header.stamp.nanosec;
        // double topic8_timestamp = (double)msg8->header.stamp.sec + 1e-9*(double)msg8->header.stamp.nanosec;
        // double topic9_timestamp = (double)msg9->header.stamp.sec + 1e-9*(double)msg9->header.stamp.nanosec;
        
        // outfile_mdl_ << topic1_timestamp << " " << topic2_timestamp << " " << topic3_timestamp << " " << topic4_timestamp << " " << topic5_timestamp << " " << topic6_timestamp << " " << topic7_timestamp << " " << topic8_timestamp << " " << topic9_timestamp << endl;
        // double time_disparity = cal_time_disparity(9, topic1_timestamp, topic2_timestamp, topic3_timestamp, topic4_timestamp, topic5_timestamp, topic6_timestamp, topic7_timestamp, topic8_timestamp, topic9_timestamp);
        // if(time_disparity > mdl_observed_wctd_)
        // {
        //     mdl_observed_wctd_ = time_disparity;
        // }
    }

    void callback1(sensor_msgs::msg::JointState::SharedPtr msg)
    {       
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time1_ = msg->timing.arrival_time;
            wait_earliest_selected_msg1_ = true;
            waiting_event1_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous1_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<0>(msg);
        mdl_sync_.add<0>(msg);

        if (delay_previous1_ > max_trans_delay1_)
        {
            max_trans_delay1_ = delay_previous1_;
        }

        if (delay_previous1_ < min_trans_delay1_)
        {
            min_trans_delay1_ = delay_previous1_;
        }

        double topic1_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp1_ > 0)
            {
                double observed_now = topic1_timestamp - previous_timestamp1_;
                if(observed_now > observed_wcp1_)
                {
                    observed_wcp1_ = observed_now;
                }

                if(observed_now < observed_bcp1_)
                {
                    observed_bcp1_ = observed_now;
                }
            }
            previous_timestamp1_ = topic1_timestamp;
        }
    }

    void callback2(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time2_ = msg->timing.arrival_time;
            wait_earliest_selected_msg2_ = true;
            waiting_event2_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous2_ = absolute_delay.seconds() * Mstos;   

        // // alg_sync_.add<1>(msg);
        mdl_sync_.add<1>(msg);

        if (delay_previous2_ > max_trans_delay2_)
        {
            max_trans_delay2_ = delay_previous2_;
        }

        if (delay_previous2_ < min_trans_delay2_)
        {
            min_trans_delay2_ = delay_previous2_;
        }

        double topic2_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp2_ > 0)
            {
                double observed_now = topic2_timestamp - previous_timestamp2_;
                if(observed_now > observed_wcp2_)
                {
                    observed_wcp2_ = observed_now;
                }

                if(observed_now < observed_bcp2_)
                {
                    observed_bcp2_ = observed_now;
                }
            }
            previous_timestamp2_ = topic2_timestamp;
        }
    }

    void callback3(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time3_ = msg->timing.arrival_time;
            wait_earliest_selected_msg3_ = true;
            waiting_event3_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous3_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<2>(msg);
        mdl_sync_.add<2>(msg);

        if (delay_previous3_ > max_trans_delay3_)
        {
            max_trans_delay3_ = delay_previous3_;
        }

        if (delay_previous3_ < min_trans_delay3_)
        {
            min_trans_delay3_ = delay_previous3_;
        }

        double topic3_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp3_ > 0)
            {
                double observed_now = topic3_timestamp - previous_timestamp3_;
                if(observed_now > observed_wcp3_)
                {
                    observed_wcp3_ = observed_now;
                }

                if(observed_now < observed_bcp3_)
                {
                    observed_bcp3_ = observed_now;
                }
            }
            previous_timestamp3_ = topic3_timestamp;
        }
    }

    void callback4(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time4_ = msg->timing.arrival_time;
            wait_earliest_selected_msg4_ = true;
            waiting_event4_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous4_ = absolute_delay.seconds() * Mstos; 

        // // alg_sync_.add<3>(msg);
        mdl_sync_.add<3>(msg);

        if (delay_previous4_ > max_trans_delay4_)
        {
            max_trans_delay4_ = delay_previous4_;
        }

        if (delay_previous4_ < min_trans_delay4_)
        {
            min_trans_delay4_ = delay_previous4_;
        }

        double topic4_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp4_ > 0)
            {
                double observed_now = topic4_timestamp - previous_timestamp4_;
                if(observed_now > observed_wcp4_)
                {
                    observed_wcp4_ = observed_now;
                }

                if(observed_now < observed_bcp4_)
                {
                    observed_bcp4_ = observed_now;
                }
            }
            previous_timestamp4_ = topic4_timestamp;
        }
    }

    void callback5(sensor_msgs::msg::JointState::SharedPtr msg)
    {      
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time5_ = msg->timing.arrival_time;
            wait_earliest_selected_msg5_ = true;
            waiting_event5_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous5_ = absolute_delay.seconds() * Mstos;
            
        // // alg_sync_.add<4>(msg);
        mdl_sync_.add<4>(msg);

        if (delay_previous5_ > max_trans_delay5_)
        {
            max_trans_delay5_ = delay_previous5_;
        }

        if (delay_previous5_ < min_trans_delay5_)
        {
            min_trans_delay5_ = delay_previous5_;
        }

        double topic5_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp5_ > 0)
            {
                double observed_now = topic5_timestamp - previous_timestamp5_;
                if(observed_now > observed_wcp5_)
                {
                    observed_wcp5_ = observed_now;
                }

                if(observed_now < observed_bcp5_)
                {
                    observed_bcp5_ = observed_now;
                }
            }
            previous_timestamp5_ = topic5_timestamp;
        }
    }

    void callback6(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time6_ = msg->timing.arrival_time;
            wait_earliest_selected_msg6_ = true;
            waiting_event6_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous6_ = absolute_delay.seconds() * Mstos;

        // // alg_sync_.add<5>(msg);
        mdl_sync_.add<5>(msg);

        if (delay_previous6_ > max_trans_delay6_)
        {
            max_trans_delay6_ = delay_previous6_;
        }

        if (delay_previous6_ < min_trans_delay6_)
        {
            min_trans_delay6_ = delay_previous6_;
        }

        double topic6_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp6_ > 0)
            {
                double observed_now = topic6_timestamp - previous_timestamp6_;
                if(observed_now > observed_wcp6_)
                {
                    observed_wcp6_ = observed_now;
                }

                if(observed_now < observed_bcp6_)
                {
                    observed_bcp6_ = observed_now;
                }
            }
            previous_timestamp6_ = topic6_timestamp;
        }
    }

    void callback7(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time7_ = msg->timing.arrival_time;
            wait_earliest_selected_msg7_ = true;
            waiting_event7_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous7_ = absolute_delay.seconds() * Mstos;  

        // // alg_sync_.add<6>(msg);
        mdl_sync_.add<6>(msg);

        if (delay_previous7_ > max_trans_delay7_)
        {
            max_trans_delay7_ = delay_previous7_;
        }

        if (delay_previous7_ < min_trans_delay7_)
        {
            min_trans_delay7_ = delay_previous7_;
        }

        double topic7_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp7_ > 0)
            {
                double observed_now = topic7_timestamp - previous_timestamp7_;
                if(observed_now > observed_wcp7_)
                {
                    observed_wcp7_ = observed_now;
                }

                if(observed_now < observed_bcp7_)
                {
                    observed_bcp7_ = observed_now;
                }
            }
            previous_timestamp7_ = topic7_timestamp;
        }
    }

    void callback8(sensor_msgs::msg::JointState::SharedPtr msg)
    {
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time8_ = msg->timing.arrival_time;
            wait_earliest_selected_msg8_ = true;
            waiting_event8_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous8_ = absolute_delay.seconds() * Mstos;      

        // // alg_sync_.add<7>(msg);
        mdl_sync_.add<7>(msg);

        if (delay_previous8_ > max_trans_delay8_)
        {
            max_trans_delay8_ = delay_previous8_;
        }

        if (delay_previous8_ < min_trans_delay8_)
        {
            min_trans_delay8_ = delay_previous8_;
        }

        double topic8_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp8_ > 0)
            {
                double observed_now = topic8_timestamp - previous_timestamp8_;
                if(observed_now > observed_wcp8_)
                {
                    observed_wcp8_ = observed_now;
                }

                if(observed_now < observed_bcp8_)
                {
                    observed_bcp8_ = observed_now;
                }
            }
            previous_timestamp8_ = topic8_timestamp;
        }
    }

    void callback9(sensor_msgs::msg::JointState::SharedPtr msg)
    {      
        msg->header.temp_stamp = this->now();
        msg->timing.arrival_time = this->now();

        if (msg->timing.earliest_sample) // The earliset arrived msg arrives
        {
            earliest_arrival_time9_ = msg->timing.arrival_time;
            wait_earliest_selected_msg9_ = true;
            waiting_event9_ = msg->timing.event_id;
        }

        rclcpp::Time now = this->now();
        rclcpp::Duration absolute_delay = now - msg->header.stamp;
        delay_previous9_ = absolute_delay.seconds() * Mstos;      

        // // alg_sync_.add<8>(msg);       
        mdl_sync_.add<8>(msg);

        if (delay_previous9_ > max_trans_delay9_)
        {
            max_trans_delay9_ = delay_previous9_;
        }

        if (delay_previous9_ < min_trans_delay9_)
        {
            min_trans_delay9_ = delay_previous9_;
        }

        double topic9_timestamp = (double)msg->header.stamp.sec + 1e-9*(double)msg->header.stamp.nanosec;

        if(count_mdl_ < num_published_set_)
        {
            if(previous_timestamp9_ > 0)
            {
                double observed_now = topic9_timestamp - previous_timestamp9_;
                if(observed_now > observed_wcp9_)
                {
                    observed_wcp9_ = observed_now;
                }

                if(observed_now < observed_bcp9_)
                {
                    observed_bcp9_ = observed_now;
                }
            }
            previous_timestamp9_ = topic9_timestamp;
        }
    }

    rclcpp::CallbackGroup::SharedPtr callback_group_subscriber_;
    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr topic1_sub_, topic2_sub_, topic3_sub_, topic4_sub_, topic5_sub_, topic6_sub_, topic7_sub_, topic8_sub_, topic9_sub_;

    typedef Synchronizer<sync_policies::ApproximateTime<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > AlgSync;
    typedef Synchronizer<sync_policies::ApproximateTimeModel<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState> > MdlSync;

    AlgSync alg_sync_;
    MdlSync mdl_sync_;

    int period1_, period2_, period3_, period4_, period5_, period6_, period7_, period8_, period9_;

    int msg_delay_opt_; // Message delay options. 0: no delay, 2: delay 0-upper;
    int delay_upper_;   // Message delay upper limit (0, 10, 20, 30, 40)
    int num_published_set_; // Required number of published sets
        
    int count_alg_, count_mdl_;
    double delay_previous1_, delay_previous2_, delay_previous3_, delay_previous4_, delay_previous5_, delay_previous6_, delay_previous7_, delay_previous8_, delay_previous9_;

    double alg_observerd_wctd_, mdl_observed_wctd_;
    double previous_timestamp1_, previous_timestamp2_, previous_timestamp3_, previous_timestamp4_, previous_timestamp5_, previous_timestamp6_, previous_timestamp7_, previous_timestamp8_, previous_timestamp9_;
    double observed_wcp1_, observed_wcp2_, observed_wcp3_, observed_wcp4_, observed_wcp5_, observed_wcp6_, observed_wcp7_, observed_wcp8_, observed_wcp9_;

    double observed_bcp1_ = FLT_MAX, observed_bcp2_ = FLT_MAX, observed_bcp3_ = FLT_MAX, observed_bcp4_ = FLT_MAX, observed_bcp5_ = FLT_MAX, observed_bcp6_ = FLT_MAX, observed_bcp7_ = FLT_MAX, observed_bcp8_ = FLT_MAX, observed_bcp9_ = FLT_MAX;

    double max_trans_delay1_ = 0., max_trans_delay2_ = 0., max_trans_delay3_ = 0., max_trans_delay4_ = 0., max_trans_delay5_ = 0., max_trans_delay6_ = 0., max_trans_delay7_ = 0., max_trans_delay8_ = 0., max_trans_delay9_ = 0.;
    double min_trans_delay1_ = FLT_MAX, min_trans_delay2_ = FLT_MAX, min_trans_delay3_ = FLT_MAX, min_trans_delay4_ = FLT_MAX, min_trans_delay5_ = FLT_MAX, min_trans_delay6_ = FLT_MAX, min_trans_delay7_ = FLT_MAX, min_trans_delay8_ = FLT_MAX, min_trans_delay9_ = FLT_MAX;
    double max_blocked_delay1_ = 0., max_blocked_delay2_ = 0., max_blocked_delay3_ = 0., max_blocked_delay4_ = 0., max_blocked_delay5_ = 0., max_blocked_delay6_ = 0., max_blocked_delay7_ = 0., max_blocked_delay8_ = 0., max_blocked_delay9_ = 0.;

    double max_queuing_delay1_ = 0., max_queuing_delay2_ = 0., max_queuing_delay3_ = 0., max_queuing_delay4_ = 0., max_queuing_delay5_ = 0., max_queuing_delay6_ = 0., max_queuing_delay7_ = 0., max_queuing_delay8_ = 0., max_queuing_delay9_ = 0.;
    double max_sync_latency1_ = 0., max_sync_latency2_ = 0., max_sync_latency3_ = 0., max_sync_latency4_ = 0., max_sync_latency5_ = 0., max_sync_latency6_ = 0., max_sync_latency7_ = 0., max_sync_latency8_ = 0., max_sync_latency9_ = 0.;
    rclcpp::Time earliest_arrival_time1_, earliest_arrival_time2_, earliest_arrival_time3_, earliest_arrival_time4_, earliest_arrival_time5_, earliest_arrival_time6_, earliest_arrival_time7_, earliest_arrival_time8_, earliest_arrival_time9_;
    bool wait_earliest_selected_msg1_ = false, wait_earliest_selected_msg2_ = false, wait_earliest_selected_msg3_ = false, wait_earliest_selected_msg4_ = false, wait_earliest_selected_msg5_ = false, wait_earliest_selected_msg6_ = false, wait_earliest_selected_msg7_ = false, wait_earliest_selected_msg8_ = false, wait_earliest_selected_msg9_ = false;
    unsigned int waiting_event1_, waiting_event2_, waiting_event3_, waiting_event4_, waiting_event5_, waiting_event6_, waiting_event7_, waiting_event8_, waiting_event9_;

    int count_event1_ = 0, count_event2_ = 0, count_event3_ = 0, count_event4_ = 0, count_event5_ = 0, count_event6_ = 0, count_event7_ = 0, count_event8_ = 0, count_event9_ = 0;
    int num_counted_events_;
};
}
