#include <chrono>
#include <memory>
#include <fstream>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include <sensor_msgs/msg/joint_state.hpp>

#include <random>
using namespace std;

using namespace std::chrono_literals;

namespace test_synchronizer
{

class TestPublisher : public rclcpp::Node
{
public:
  TestPublisher(string topic_name, int real_period, int publisher_index = 0)
  : Node(topic_name+std::to_string(publisher_index).c_str()), count_(0), id_(publisher_index), real_period_(real_period)
  {
    RCLCPP_INFO(this->get_logger(), "Publisher to the topic %s, with period of %ld", topic_name.c_str(), real_period_);

    publisher_ = this->create_publisher<sensor_msgs::msg::JointState>(topic_name.c_str(), 10);

    period_ = 1ms * real_period_;

    timer_ = this->create_wall_timer(period_, std::bind(&TestPublisher::timer_callback, this));
  }

private:
  void timer_callback()
  { 
    rclcpp::Time now = this->now();

    std::string symbol = std::to_string(count_);
    count_++;
    
    if(count_ > 10)
        publisher(" "+std::to_string(id_)+": "+symbol, now);
  }

  void publisher(std::string symbol,rclcpp::Time now)
  {
    auto message = sensor_msgs::msg::JointState();
    message.name.push_back("Publisher"+symbol);
    message.header.stamp = now;
    message.header.temp_stamp = now;

    publisher_->publish(message);
  }

private:
  rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr publisher_;
  rclcpp::TimerBase::SharedPtr timer_;

  size_t count_;
  int id_;

  int real_period_;
  std::chrono::milliseconds period_;
};

}