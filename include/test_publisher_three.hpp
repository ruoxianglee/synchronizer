#include <chrono>
#include <memory>
#include <fstream>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include <sensor_msgs/msg/joint_state.hpp>

#include <random>
using namespace std;

using namespace std::chrono_literals;

namespace test_synchronizer
{

class TestPublisherThree : public rclcpp::Node
{
public:
  TestPublisherThree(int real_period1, int real_period2, int real_period3, int publisher_index = 0)
  : Node("publisher"), count_(0), id_(publisher_index),
        real_period1_(real_period1), real_period2_(real_period2), real_period3_(real_period3)
  {
    RCLCPP_INFO(this->get_logger(), "Publisher 1 to the topic 1, with period of %ld", real_period1_);

    RCLCPP_INFO(this->get_logger(), "Publisher 2 to the topic 2, with period of %ld", real_period2_);

    RCLCPP_INFO(this->get_logger(), "Publisher 3 to the topic 3, with period of %ld", real_period3_);

    publisher1_ = this->create_publisher<sensor_msgs::msg::JointState>("topic1", 10);
    publisher2_ = this->create_publisher<sensor_msgs::msg::JointState>("topic2", 10);
    publisher3_ = this->create_publisher<sensor_msgs::msg::JointState>("topic3", 10);

    period1_ = 1ms * real_period1_;
    period2_ = 1ms * real_period2_;
    period3_ = 1ms * real_period3_;

    timer1_ = this->create_wall_timer(period1_, std::bind(&TestPublisherThree::timer_callback1, this));
    timer2_ = this->create_wall_timer(period2_, std::bind(&TestPublisherThree::timer_callback2, this));
    timer3_ = this->create_wall_timer(period3_, std::bind(&TestPublisherThree::timer_callback3, this));
  }

private:
  void timer_callback1()
  { 
    rclcpp::Time now = this->now();

    std::string symbol = std::to_string(count_);
    count_++;
    
    // if(count_ > 10)
        // publisher(" "+std::to_string(id_)+": "+symbol, now);

    auto message = sensor_msgs::msg::JointState();
    message.name.push_back("Publisher1"+symbol);
    message.header.stamp = now;
    message.header.temp_stamp = now;

    publisher1_->publish(message);
  }

  void timer_callback2()
  { 
    rclcpp::Time now = this->now();

    std::string symbol = std::to_string(count_);
    count_++;
    
    // if(count_ > 10)
        // publisher(" "+std::to_string(id_)+": "+symbol, now);

    auto message = sensor_msgs::msg::JointState();
    message.name.push_back("Publisher2"+symbol);
    message.header.stamp = now;
    message.header.temp_stamp = now;

    publisher2_->publish(message);
  }

  void timer_callback3()
  { 
    rclcpp::Time now = this->now();

    std::string symbol = std::to_string(count_);
    count_++;
    
    // if(count_ > 10)
        // publisher(" "+std::to_string(id_)+": "+symbol, now);

    auto message = sensor_msgs::msg::JointState();
    message.name.push_back("Publisher3"+symbol);
    message.header.stamp = now;
    message.header.temp_stamp = now;

    publisher3_->publish(message);
  }

//   void publisher(std::string symbol,rclcpp::Time now)
//   {
//     auto message = sensor_msgs::msg::JointState();
//     message.name.push_back("Publisher"+symbol);
//     message.header.stamp = now;
//     message.header.temp_stamp = now;

//     publisher1_->publish(message);
//   }

private:
  rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr publisher1_, publisher2_, publisher3_;
  rclcpp::TimerBase::SharedPtr timer1_, timer2_, timer3_;

  size_t count_;
  int id_;

  int real_period1_, real_period2_, real_period3_;
  std::chrono::milliseconds period1_, period2_, period3_;
};

}