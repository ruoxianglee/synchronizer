import numpy as np
import matplotlib.pyplot as plt
import bound_with_delay as bound_lib
from matplotlib.ticker import MaxNLocator
import time
import latency_calculation as blc

def line_chart_plot_different_channels(results):
    ## Line Chart
    x = np.array(['3', '4', '5', '6', '7', '8', '9'])

    # Create a figure and axis object
    fig, ax = plt.subplots(figsize=(7, 4))

    # Plot the time disparity line
    upper_bound_array=[]
    observed_array=[]
    for k in range(7):
        upper_bound_array.append(results[k][0])
        observed_array.append(results[k][1])
    
    print(upper_bound_array)
    print(observed_array)

    # Plot the upper bound line
    ax.plot(x, upper_bound_array, marker="o", markersize=12, linewidth=2, label='Upper Bound')
    # ax.scatter(x, upper_bound2_array, color="red", s=100)  # Add dots to represent data points

    # Plot the observed line
    ax.plot(x, observed_array, marker="d", markersize=12, linewidth=2, label='Observed')
    # ax.scatter(x, observed_array, color="red", s=100)  # Add dots to represent data points
    
    # Set the axis labels and title
    ax.set_xlabel('Number of Channels', fontsize=18)
    ax.set_ylabel('Blocking Latency (x 10^2)', fontsize=18)
    # ax.set_title('Delay Analysis', fontsize=16, fontweight='bold')

    # Add a grid to the chart
    ax.grid(axis="y", linestyle='--', alpha=0.7)

    # Customize the tick font sizes
    ax.tick_params(axis='both', which='major', labelsize=12)

    # Set a custom y-axis range
    # ax.set_ylim(0, 25)

    # Add a legend and customize its font size
    ax.legend(fontsize=14)
    
    # Show only integer values on the y-axis
    # ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    # ax.yaxis.set_major_formatter('{:.0f}'.format)

    # Save the chart as a PNG file
    # plt.savefig('line_chart.png', dpi=300, bbox_inches='tight')

    # Show the plot
    plt.show()


def line_chart_plot_six_channels(results):
    ## Line Chart
    x = np.array(['1', '2', '3', '4', '5', '6'])

    # Create a figure and axis object
    fig, ax = plt.subplots(figsize=(7, 4))

    # Plot the time disparity line
    upper_bound_array=[]
    observed_array=[]
    for k in range(6):
        upper_bound_array.append(results[k][0])
        observed_array.append(results[k][1])
    
    print(upper_bound_array)
    print(observed_array)

    # Plot the upper bound line
    ax.plot(x, upper_bound_array, marker="o", markersize=12, linewidth=2, label='Upper Bound')
    # ax.scatter(x, upper_bound2_array, color="red", s=100)  # Add dots to represent data points

    # Plot the observed line
    ax.plot(x, observed_array, marker="d", markersize=12, linewidth=2, label='Observed')
    # ax.scatter(x, observed_array, color="red", s=100)  # Add dots to represent data points
    
    # Set the axis labels and title
    ax.set_xlabel('Index of Channel', fontsize=18)
    ax.set_ylabel('Blocking Latency (x 10^2)', fontsize=18)
    # ax.set_title('Delay Analysis', fontsize=16, fontweight='bold')

    # Add a grid to the chart
    ax.grid(axis="y", linestyle='--', alpha=0.7)

    # Customize the tick font sizes
    ax.tick_params(axis='both', which='major', labelsize=12)

    # Set a custom y-axis range
    # ax.set_ylim(0, 25)

    # Add a legend and customize its font size
    ax.legend(fontsize=14)
    
    # Show only integer values on the y-axis
    # ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    # ax.yaxis.set_major_formatter('{:.0f}'.format)

    # Save the chart as a PNG file
    # plt.savefig('line_chart.png', dpi=300, bbox_inches='tight')

    # Show the plot
    plt.show()

def line_chart_plot_varied_delay(results):
    ## Line Chart
    x = np.array(['0', '[0,10]', '[0,20]', '[0,30]', '[0,40]'])

    # Create a figure and axis object
    fig, ax = plt.subplots(figsize=(7, 4))

    # Plot the time disparity line
    upper_bound_array=[]
    observed_array=[]
    for k in range(5):
        upper_bound_array.append(results[k][0])
        observed_array.append(results[k][1])
    
    print(upper_bound_array)
    print(observed_array)

    # Plot the upper bound line
    ax.plot(x, upper_bound_array, marker="o", markersize=12, linewidth=2, label='Upper Bound')
    # ax.scatter(x, upper_bound2_array, color="red", s=100)  # Add dots to represent data points

    # Plot the observed line
    ax.plot(x, observed_array, marker="d", markersize=12, linewidth=2, label='Observed')
    # ax.scatter(x, observed_array, color="red", s=100)  # Add dots to represent data points
    
    # Set the axis labels and title
    ax.set_xlabel('Random Delay', fontsize=18)
    ax.set_ylabel('Blocking Latency (x 10^2)', fontsize=18)
    # ax.set_title('Delay Analysis', fontsize=16, fontweight='bold')

    # Add a grid to the chart
    ax.grid(axis="y", linestyle='--', alpha=0.7)

    # Customize the tick font sizes
    ax.tick_params(axis='both', which='major', labelsize=12)

    # Set a custom y-axis range
    # ax.set_ylim(0, 25)

    # Add a legend and customize its font size
    ax.legend(fontsize=14)
    
    # Show only integer values on the y-axis
    # ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    # ax.yaxis.set_major_formatter('{:.0f}'.format)

    # Save the chart as a PNG file
    # plt.savefig('line_chart.png', dpi=300, bbox_inches='tight')

    # Show the plot
    plt.show()    

def line_chart_plot_period_factor(results):
    ## Line Chart
    x = np.array(['1.0', '1.2', '1.4', '1.6', '1.8'])

    # Create a figure and axis object
    fig, ax = plt.subplots(figsize=(7, 4))

    # Plot the time disparity line
    upper_bound_array=[]
    observed_array=[]
    for k in range(5):
        upper_bound_array.append(results[k][0])
        observed_array.append(results[k][1])
    
    print(upper_bound_array)
    print(observed_array)

    # Plot the upper bound line
    ax.plot(x, upper_bound_array, marker="o", markersize=12, linewidth=2, label='Upper Bound')
    # ax.scatter(x, upper_bound2_array, color="red", s=100)  # Add dots to represent data points

    # Plot the observed line
    ax.plot(x, observed_array, marker="d", markersize=12, linewidth=2, label='Observed')
    # ax.scatter(x, observed_array, color="red", s=100)  # Add dots to represent data points
    
    # Set the axis labels and title
    ax.set_xlabel('Period Ratio', fontsize=18)
    ax.set_ylabel('Blocking Latency (x 10^2)', fontsize=18)
    # ax.set_title('Delay Analysis', fontsize=16, fontweight='bold')

    # Add a grid to the chart
    ax.grid(axis="y", linestyle='--', alpha=0.7)

    # Customize the tick font sizes
    ax.tick_params(axis='both', which='major', labelsize=12)

    # Set a custom y-axis range
    # ax.set_ylim(0, 25)

    # Add a legend and customize its font size
    ax.legend(fontsize=14)
    
    # Show only integer values on the y-axis
    # ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    # ax.yaxis.set_major_formatter('{:.0f}'.format)

    # Save the chart as a PNG file
    # plt.savefig('line_chart.png', dpi=300, bbox_inches='tight')

    # Show the plot
    plt.show() 