import numpy as np
import matplotlib.pyplot as plt
import bound_with_delay as bound_lib
from matplotlib.ticker import MaxNLocator
import time
import latency_calculation as blc
import queuing_latency_line_chart_plot as lcp

results = []
factors = [0, 2, 4, 6, 8]
for factor in factors:
    path="../results/evaluation_delay/varied_period_factor/varied_period_factor_1."+str(factor)

    avg_upper_bound, avg_observed = blc.calculate_average_timestamp_difference(path, 6, 0)
    results.append([avg_upper_bound * 10, avg_observed * 10])

lcp.line_chart_plot_period_factor(results)