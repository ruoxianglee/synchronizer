import numpy as np
import matplotlib.pyplot as plt
import bound_with_delay as bound_lib
from matplotlib.ticker import MaxNLocator
import time
import latency_calculation as blc
import blocking_latency_line_chart_plot as lcp

results = []
factors = [0, 2, 4, 6, 8]
for factor in factors:
    path="../results/evaluation_delay/varied_period_factor/varied_period_factor_1."+str(factor)

    avg_upper_bound1, avg_upper_bound2, avg_observed = blc.calculate_average_blocking_latency(path, 6, 0)
    results.append([avg_upper_bound1 * 10, avg_upper_bound2 * 10, avg_observed * 10])

# results:
# number of channels  value1            value2             value3
# 3                   avg_upper_bound1  avg_upper_bound2   avg_observed
# 4                   avg_upper_bound1  avg_upper_bound2   avg_observed
# 5                   avg_upper_bound1  avg_upper_bound2   avg_observed
# 6                   ...
# 7
# 8
# 9
# print(results)
lcp.line_chart_plot_period_factor(results)