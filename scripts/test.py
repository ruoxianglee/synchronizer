import matplotlib.pyplot as plt
import numpy as np

array1 = [1, 2, 3, 4, 5]
array2 = [4, 5]

result = list(set(array1) ^ set(array2))

print(result) # Output: [1, 2, 3, 6, 7, 8]

exit

# Define the data points
time_disparity = [3, 4, 5]
observed = [10, 15, 20]
upper_bound = [18, 20, 22]
x = np.array(['Channel 1', 'Channel 2', 'Channel 3'])

# Create a figure and axis object
fig, ax = plt.subplots(figsize=(6, 4))

# Plot the time disparity line
ax.plot(x, time_disparity, marker="o", markersize=10, linewidth=2, label='Time Disparity')
ax.scatter(x, time_disparity, color="red", s=100)  # Add dots to represent data points

# Plot the observed line
ax.plot(x, observed, marker="o", markersize=10, linewidth=2, label='Observed')
ax.scatter(x, observed, color="red", s=100)  # Add dots to represent data points

# Plot the upper bound line
ax.plot(x, upper_bound, marker="o", markersize=10, linewidth=2, label='Upper Bound')
ax.scatter(x, upper_bound, color="red", s=100)  # Add dots to represent data points

# Set the axis labels and title
ax.set_xlabel('Channels', fontsize=14)
ax.set_ylabel('Time (ms)', fontsize=14)
ax.set_title('Line Chart', fontsize=16, fontweight='bold')

# Add a grid to the chart
ax.grid(axis="y", linestyle='--', alpha=0.7)

# Customize the tick font sizes
ax.tick_params(axis='both', which='major', labelsize=12)

# Set a custom y-axis range
ax.set_ylim(0, 25)

# Add a legend and customize its font size
ax.legend(fontsize=12)

# Save the chart as a PNG file
plt.savefig('line_chart.png', dpi=300, bbox_inches='tight')

# Show the plot
plt.show()
