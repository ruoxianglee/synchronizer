import numpy as np
import matplotlib.pyplot as plt
import bound_with_delay as bound_lib
from matplotlib.ticker import MaxNLocator
import time
import latency_calculation as blc
import queuing_latency_line_chart_plot as lcp

results = []
for channel in range(6):
    path="../results/evaluation_delay/channel_num/channel_6"

    avg_upper_bound, avg_observed = blc.calculate_average_queuing_latency(path, 6, channel)
    results.append([avg_upper_bound * 10, avg_observed * 10])

print(results)
lcp.line_chart_plot_six_channels(results)