import numpy as np
import matplotlib.pyplot as plt
import bound_with_delay as bound_lib
from matplotlib.ticker import MaxNLocator
import time
import latency_calculation as blc
import reaction_latency_line_chart_plot as lcp

results = []
channels = [3, 4, 5, 6, 7, 8, 9]
for channel_num in channels:
    path="../results/evaluation_delay/channel_num/channel_" + str(channel_num)

    avg_upper_bound, avg_observed = blc.calculate_average_reaction_latency(path, channel_num, 0)
    results.append([avg_upper_bound * 10, avg_observed * 10])

print(results)
lcp.line_chart_plot_different_channels(results)