import numpy as np
import matplotlib.pyplot as plt
import bound_with_delay as bound_lib
from matplotlib.ticker import MaxNLocator

def line_chart_plot(time_disparity, observed, upper_bound):
    ## Line Chart
    x = np.array(['Channel 1', 'Channel 2', 'Channel 3', 'Channel 4', 'Channel 5', 'Channel 6'])

    # Create a figure and axis object
    fig, ax = plt.subplots(figsize=(6, 4))

    # Plot the time disparity line
    ax.plot(x, time_disparity, marker="o", markersize=10, linewidth=2, label='Upper Bound 1')
    ax.scatter(x, time_disparity, color="red", s=100)  # Add dots to represent data points

    # Plot the observed line
    ax.plot(x, observed, marker="o", markersize=10, linewidth=2, label='Observed')
    ax.scatter(x, observed, color="red", s=100)  # Add dots to represent data points

    # Plot the upper bound line
    ax.plot(x, upper_bound, marker="o", markersize=10, linewidth=2, label='Upper Bound 2')
    ax.scatter(x, upper_bound, color="red", s=100)  # Add dots to represent data points

    # Set the axis labels and title
    ax.set_xlabel('Channels', fontsize=14)
    ax.set_ylabel('Time (x 10^2)', fontsize=14)
    ax.set_title('Delay Analysis', fontsize=16, fontweight='bold')

    # Add a grid to the chart
    ax.grid(axis="y", linestyle='--', alpha=0.7)

    # Customize the tick font sizes
    ax.tick_params(axis='both', which='major', labelsize=12)

    # Set a custom y-axis range
    # ax.set_ylim(0, 25)

    # Add a legend and customize its font size
    ax.legend(fontsize=12)
    
    # Show only integer values on the y-axis
    # ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    # ax.yaxis.set_major_formatter('{:.0f}'.format)

    # Save the chart as a PNG file
    # plt.savefig('line_chart.png', dpi=300, bbox_inches='tight')

    # Show the plot
    plt.show()

    # # Histogram Graph
    # # Plot the four average values
    # labels = ['Time Disp', 'Avg UB 1', 'Avg Delay 1', 'Avg UB 2', 'Avg Delay 2', 'Avg UB 3', 'Avg Delay 3']
    # data = [avg_time_disparity * 1000, avg_delay_upper_bound1 * 1000, avg_num13 * 1000, avg_delay_upper_bound2 * 1000, avg_num14 * 1000, avg_delay_upper_bound3 * 1000, avg_num15 * 1000]

    # fig, ax = plt.subplots()
    # ax.bar(labels, data)

    # # Add labels and title
    # # ax.set_xlabel('Averages')
    # # ax.set_ylabel('Values')
    # ax.set_title('Delay Analysis')

    # # Show the plot
    # plt.show()

def get_zero_one(timedisparity, worse_period):
    if worse_period < timedisparity:
        return 1
    
    return 0

# Open the timestamp file for reading
with open("timestamp_mdl.txt", "r") as f:
    # Initialize variables to store average values
    avg_time_disparity = 0.0
    avg_num13 = 0.0
    avg_num14 = 0.0
    avg_num15 = 0.0
    avg_num0 = 0.0
    avg_num1 = 0.0
    avg_num2 = 0.0
    avg_num3 = 0.0
    avg_num4 = 0.0
    avg_num5 = 0.0    
    count = 0
    avg_delay_upper_bound1 = 0.0
    avg_delay_upper_bound2 = 0.0
    avg_delay_upper_bound3 = 0.0

    # Iterate over lines in the file
    for line in f:
        # Split the line into a list of floats
        values = [float(x) for x in line.split()]

        # Calculate time disparity using the 4th, 5th, and 6th values
        worst_periods = [values[6], values[7], values[8], values[9], values[10], values[11]]
        time_disparity = bound_lib.cal_ros_bound(worst_periods)

        # Add to total average value
        avg_time_disparity += time_disparity

        # Find indexes of values less than the time disparity
        best_periods = [values[0], values[1], values[2], values[3], values[4], values[5]]
        indexes = [i for i, val in enumerate(best_periods) if val < time_disparity]
        print(indexes)

        # Find the maximum sum of the worst period and the corresponding worst delay
        max_sum = 0
        worst_delay = [values[18], values[19], values[20], values[21], values[22], values[23]]
        for idx in indexes:
            # Calculate the sum of group1[idx] and group3[idx]
            val = worst_periods[idx] + worst_delay[idx]
            # print(worst_delay[idx])
            # Update max_sum if val is greater than max_sum
            if val > max_sum:
                max_sum = val

        indexes = []
        for ind, val in enumerate(best_periods):
            if val > time_disparity and val < 2 * time_disparity:
                indexes.append(ind)
        print(indexes)

        for idx in indexes:
            # Calculate the sum of group1[idx] and group3[idx]
            val = worst_periods[idx] + worst_delay[idx] + time_disparity - best_periods[idx]
            # print(worst_delay[idx])
            # Update max_sum if val is greater than max_sum
            if val > max_sum:
                max_sum = val

        # if len(indexes) == 0:
        #     for idx in range(6):
        #         # Calculate the sum of group1[idx] and group3[idx]
        #         val = worst_delay[idx]
        #         # print(worst_delay[idx])
        #         # Update max_sum if val is greater than max_sum
        #         if val > max_sum:
        #             max_sum = val
        
        # max_sum = 0
        # # if len(indexes) == 0:
        # for idx in range(6):
        #     # Calculate the sum of group1[idx] and group3[idx]
        #     val = worst_delay[idx] + worst_periods[idx] - best_periods[idx] + time_disparity - (time_disparity - best_periods[idx]) * get_zero_one(time_disparity, best_periods[idx])
        #     # print(worst_delay[idx])
        #     # Update max_sum if val is greater than max_sum
        #     if val > max_sum:
        #         max_sum = val

        # max_sum1 = 0
        # for idx in range(3):
        #     if idx != 0:
        #         # Calculate the sum of group1[idx] and group3[idx]
        #         val = worst_delay[idx]
        #         # print(worst_delay[idx])
        #         # Update max_sum if val is greater than max_sum
        #         if val > max_sum1:
        #             max_sum1 = val

        # max_sum2 = 0
        # for idx in range(3):
        #     if idx != 1:
        #         # Calculate the sum of group1[idx] and group3[idx]
        #         val = worst_delay[idx]
        #         # print(worst_delay[idx])
        #         # Update max_sum if val is greater than max_sum
        #         if val > max_sum2:
        #             max_sum2 = val

        # max_sum3 = 0
        # for idx in range(3):
        #     if idx != 2:
        #         # Calculate the sum of group1[idx] and group3[idx]
        #         val = worst_delay[idx]
        #         # print(worst_delay[idx])
        #         # Update max_sum if val is greater than max_sum
        #         if val > max_sum3:
        #             max_sum3 = val

        # Calculate the upper bound
        # avg_delay_upper_bound1 += (max_sum1 + time_disparity - values[6])
        # avg_delay_upper_bound2 += (max_sum2 + time_disparity - values[7])
        # avg_delay_upper_bound3 += (max_sum3 + time_disparity - values[8])

        time_disparity_arr = [time_disparity * 10, time_disparity * 10, time_disparity * 10]
        observed_arr = [values[24] * 10, values[25] * 10, values[26] * 10, values[27] * 10, values[28] * 10, values[29] * 10]

        # # Calculate the upper bound
        upper_bound1 = (max_sum + time_disparity - values[12])
        upper_bound2 = (max_sum + time_disparity - values[13])
        upper_bound3 = (max_sum + time_disparity - values[14])
        upper_bound4 = (max_sum + time_disparity - values[15])
        upper_bound5 = (max_sum + time_disparity - values[16])
        upper_bound6 = (max_sum + time_disparity - values[17])

        upper_bound_arr = [upper_bound1 * 10, upper_bound2 * 10, upper_bound3 * 10, upper_bound4 * 10, upper_bound5 * 10, upper_bound6 * 10]

        max_sum = 0
        for idx in range(6):
            # Calculate the sum of group1[idx] and group3[idx]
            val = worst_periods[idx] + worst_delay[idx]
            # print(worst_delay[idx])
            # Update max_sum if val is greater than max_sum
            if val > max_sum:
                max_sum = val

        upper_bound1 = (max_sum + time_disparity - values[12])
        upper_bound2 = (max_sum + time_disparity - values[13])
        upper_bound3 = (max_sum + time_disparity - values[14])
        upper_bound4 = (max_sum + time_disparity - values[15])
        upper_bound5 = (max_sum + time_disparity - values[16])
        upper_bound6 = (max_sum + time_disparity - values[17])

        upper_bound_arr_big = [upper_bound1 * 10, upper_bound2 * 10, upper_bound3 * 10, upper_bound4 * 10, upper_bound5 * 10, upper_bound6 * 10]
        print(upper_bound_arr)
        print(upper_bound_arr_big)
        print(time_disparity)
        print(best_periods)
        # print(worst_periods)
        line_chart_plot(upper_bound_arr_big, observed_arr, upper_bound_arr)

        # break
        continue
    
        # # Calculate the upper bound
        avg_delay_upper_bound1 += (max_sum + time_disparity - values[6])
        avg_delay_upper_bound2 += (max_sum + time_disparity - values[7])
        avg_delay_upper_bound3 += (max_sum + time_disparity - values[8])

        # Add to total average values for columns 13-15
        avg_num13 += values[12]
        avg_num14 += values[13]
        avg_num15 += values[14]

        # Total sum of best periods and worst periods
        avg_num0 += values[0]
        avg_num1 += values[1]
        avg_num2 += values[2]

        avg_num3 += values[3]
        avg_num4 += values[4]
        avg_num5 += values[5]

        # Increment count
        count += 1

    # Calculate averages by dividing by the count
    # avg_time_disparity /= count
    # avg_delay_upper_bound1 /= count
    # avg_delay_upper_bound2 /= count
    # avg_delay_upper_bound3 /= count
    # avg_num13 /= count
    # avg_num14 /= count
    # avg_num15 /= count

    # avg_num0 /= count
    # avg_num1 /= count
    # avg_num2 /= count

    # avg_num3 /= count
    # avg_num4 /= count
    # avg_num5 /= count

    # print(avg_num0)
    # print(avg_num1)
    # print(avg_num2)
    # print(avg_num3)
    # print(avg_num4)
    # print(avg_num5)

    # time_disparity = [avg_time_disparity * 10, avg_time_disparity * 10, avg_time_disparity * 10]
    # observed = [avg_num13 * 10, avg_num14 * 10, avg_num15 * 10]
    # upper_bound = [avg_delay_upper_bound1 * 10, avg_delay_upper_bound2 * 10, avg_delay_upper_bound3 * 10]

    # line_chart_plot(time_disparity, observed, upper_bound)