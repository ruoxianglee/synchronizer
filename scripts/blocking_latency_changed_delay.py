import numpy as np
import matplotlib.pyplot as plt
import bound_with_delay as bound_lib
from matplotlib.ticker import MaxNLocator
import time
import latency_calculation as blc
import blocking_latency_line_chart_plot as lcp

results = []
delay_lower = [0, 10, 20, 30, 40]
for delay_lower in delay_lower:
    if delay_lower == 0:
        path="../results/evaluation_delay/varied_delay/random_nodelay"
    else:
        path="../results/evaluation_delay/varied_delay/random_delay_"+str(delay_lower)

    avg_upper_bound1, avg_upper_bound2, avg_observed = blc.calculate_average_blocking_latency(path, 6, 0)
    results.append([avg_upper_bound1 * 10, avg_upper_bound2 * 10, avg_observed * 10])

# results:
# number of channels  value1            value2             value3
# 3                   avg_upper_bound1  avg_upper_bound2   avg_observed
# 4                   avg_upper_bound1  avg_upper_bound2   avg_observed
# 5                   avg_upper_bound1  avg_upper_bound2   avg_observed
# 6                   ...
# 7
# 8
# 9
# print(results)
lcp.line_chart_plot_varied_delay(results)