#include <memory>

#include "rclcpp/rclcpp.hpp"

#include "evaluation/test_publisher.hpp"
#include "evaluation/test_publisher_three.hpp"
#include "evaluation/test_subscriber.hpp"

int main(int argc, char * argv[])
{
    rclcpp::init(argc, argv);
    
    rclcpp::NodeOptions options;

    auto exec = std::make_shared<rclcpp::executors::SingleThreadedExecutor>();
    // auto exec = std::make_shared<rclcpp::executors::MultiThreadedExecutor>(rclcpp::ExecutorOptions(), 6, false);

    // auto publisher1 = std::make_shared<test_synchronizer::TestPublisher>("topic1", 50); 
    // auto publisher2 = std::make_shared<test_synchronizer::TestPublisher>("topic2", 50); 
    // auto publisher3 = std::make_shared<test_synchronizer::TestPublisher>("topic3", 100);

    // auto publisher = std::make_shared<test_synchronizer::TestPublisherThree>(20, 80, 330); 

    auto publisher = std::make_shared<test_synchronizer::TestPublisherThree>(20, 30, 50);    

    auto subscirber = std::make_shared<test_synchronizer::TestSubscriber>(); 

    // exec->add_node(publisher3);
    // exec->add_node(publisher1);
    // exec->add_node(publisher2);

    exec->add_node(publisher);
    exec->add_node(subscirber);

    exec->spin();
}
