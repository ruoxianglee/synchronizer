#!/bin/bash

source ../../install/setup.bash

# Number of test group
num_group=1

#########################################################
###################  Evaluation  ########################
#########################################################
# Evaluation with different number of channels
ros2 run synchronizer evaluation_delay 3 50 $num_group /home/ros/ros2_humble/src/synchronizer/scripts --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config.yaml

exit

ros2 run synchronizer evaluation_delay 3 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/channel_num/channel_3 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_1.yaml
ros2 run synchronizer evaluation_delay 4 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/channel_num/channel_4 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_1.yaml
ros2 run synchronizer evaluation_delay 5 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/channel_num/channel_5 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_1.yaml
ros2 run synchronizer evaluation_delay 6 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/channel_num/channel_6 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_1.yaml
ros2 run synchronizer evaluation_delay 7 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/channel_num/channel_7 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_1.yaml
ros2 run synchronizer evaluation_delay 8 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/channel_num/channel_8 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_1.yaml
ros2 run synchronizer evaluation_delay 9 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/channel_num/channel_9 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_1.yaml

# Evaluation with changed periods
ros2 run synchronizer evaluation_delay 6 10 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_periods/varied_period_lower_10 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_1.yaml
ros2 run synchronizer evaluation_delay 6 20 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_periods/varied_period_lower_20 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_1.yaml
ros2 run synchronizer evaluation_delay 6 30 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_periods/varied_period_lower_30 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_1.yaml
ros2 run synchronizer evaluation_delay 6 40 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_periods/varied_period_lower_40 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_1.yaml
ros2 run synchronizer evaluation_delay 6 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_periods/varied_period_lower_50 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_1.yaml

# Evaluation with varied timestamp separation 
ros2 run synchronizer evaluation_delay 6 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_period_factor/varied_period_factor_1.0 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_2.yaml
ros2 run synchronizer evaluation_delay 6 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_period_factor/varied_period_factor_1.2 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_3.yaml
ros2 run synchronizer evaluation_delay 6 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_period_factor/varied_period_factor_1.4 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_4.yaml
ros2 run synchronizer evaluation_delay 6 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_period_factor/varied_period_factor_1.6 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_5.yaml
ros2 run synchronizer evaluation_delay 6 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_period_factor/varied_period_factor_1.8 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_6.yaml

# Evaluation with varied delay
ros2 run synchronizer evaluation_delay 6 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_delay/random_delay_10 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_7.yaml
ros2 run synchronizer evaluation_delay 6 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_delay/random_delay_20 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_8.yaml
ros2 run synchronizer evaluation_delay 6 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_delay/random_delay_30 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_9.yaml
ros2 run synchronizer evaluation_delay 6 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_delay/random_delay_40 --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_10.yaml
ros2 run synchronizer evaluation_delay 6 50 $num_group /home/ros/ros2_humble/src/synchronizer/results/evaluation_delay/varied_delay/random_nodelay --ros-args --params-file /home/ros/ros2_humble/src/synchronizer/config/config_11.yaml

# clear

# Generate the evalution illustrations
# python3 /home/ros/ros2_humble/src/synchronizer/scripts/delay_channel_num.py